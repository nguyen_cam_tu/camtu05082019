<?php get_header(); ?>

<main class="p-service">
    <?php the_breadcrumb(); ?>
    <div class="c-headpage">
        <div class="l-container2">
            <h2 class="c-title">成功事例</h2>
            <p>関与した企業さま、個人のお客様に対し、ひかり税理士法人が提供したサービスと、どのような成果が得られたか、その一部をご紹介いたします。</p>
        </div>
    </div>

    <div class="p-cases__content">
        <div class="l-container2">
            <?php include(locate_template('inc/loop-cases.php'));?>
        </div>
    </div>
</main>

<?php get_footer();?>