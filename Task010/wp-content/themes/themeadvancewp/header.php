<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>
        <?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?>
    </title>
    <meta name="description" content="<?= bloginfo('description'); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>">

    <!-- css -->
    <link rel="stylesheet" defer href="<?php echo get_stylesheet_directory_uri()?>/css/style.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
    <!-- css -->

    <!-- js -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- js -->

    <!-- ▼ WP HEAD -->
    <?php wp_head(); ?>
    <!-- ▲ WP HEAD -->
</head>

<body>
    <header class="c-header">
        <div class="l-container">
            <h1 class="c-logo"><a href="<?php echo site_url(); ?>"><img defer src="<?php echo get_stylesheet_directory_uri()?>/images/logo.png" alt="Allgrow Labo"></a></h1>
            <?php wp_nav_menu( array(
				'theme_location' => 'primary-menu', 
				'container' => 'nav', 
				'container_class' => 'c-gnav', 
				'menu_class' => 'menu clearfix' 
			) ); ?>
        </div>
    </header><!-- /header -->