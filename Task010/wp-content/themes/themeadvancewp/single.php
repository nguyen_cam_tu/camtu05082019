
<?php get_header() ;?>

<!-- ▼Content ================================================== -->
<main class="p-news">
    <?php the_breadcrumb(); ?>

    <div class="p-news__content">
        <div class="l-container">

            <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
            <div class="feature_img">
                <?php the_post_thumbnail('full'); ?>
            </div>
            <div class="c-ttlpostpage">
                <h2><?php the_title();?></h2>
                <span><?php the_time("Y年m月d日")?></span>
                <span class="c-dotcat c-dotcat--1"> <?php echo the_category($post->ID) ; ?></span>
            </div>
            <div class="single_content">
                <?php the_content();?>
            </div>
            <div class="l-btn">
                <div class="c-btn c-btn--small2">
                    <a href="<?php echo site_url('/news/'); ?>">ニュース一覧を見る</a>
                </div>
            </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>
    </div>
</main>
<!-- ▲Content ================================================== -->
<?php get_footer() ;?>