<footer class="c-footer">
		<div class="c-footer__logo">
			<div class="l-container">
				<a href="<?php echo site_url(); ?>"><img defer src="<?php echo get_stylesheet_directory_uri()?>/images/logo.png" alt=""></a>
			</div>
		</div>
		
		<div class="c-footer__main">
			<div class="l-container">
				<div class="c-footer__link">
					<h3><a href="">ニュース</a></h3>
					<ul class="c-boxlink">
						<li><a href="#">お知らせ</a></li>
						<li><a href="#">税の最新情報</a></li>
						<li><a href="#">税制改正</a></li>
						<li><a href="#">掲載情報</a></li>
						<li><a href="#">バックナンバー</a></li>
					</ul>
				</div>

				<div class="c-footer__link">
					<h3><a href="">成功事例</a></h3>
					<ul class="c-boxlink">
						<li><a href="#">法人のお客様</a></li>
						<li><a href="#">個人のお客様</a></li>
					</ul>
				</div>

				<div class="c-footer__link">
					<ul class="c-boxlink">
						<li><a href="staff.html">スタッフ</a></li>
						<li><a href="recruit.html">採用情報</a></li>
						<li><a href="privacy.html">プライバシーポリシー</a></li>
						<li><a href="sitemap.html">サイトマップ</a></li>
					</ul>
				</div>
			</div>
		</div>
    </footer>
    <script defer src="<?php echo get_stylesheet_directory_uri()?>/js/themes.js"></script>
</body>
</html>
