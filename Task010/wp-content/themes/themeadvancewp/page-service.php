<?php get_header(); ?>

<main class="p-service">
<?php the_breadcrumb(); ?>
		<div class="c-headpage">
			<div class="l-container2">
				<h2 class="c-title">ご提供サービス</h2>
			</div>
		</div>

		<div class="feature_img">
			<img defer src="<?php echo get_stylesheet_directory_uri()?>/images/img_services01.png" alt="">
		</div>
		<div class="p-service__content">
			<div class="l-container">
				<p class="notice">ひかり税理士法人がご提供するすべてのサービスを目的別に検索していただけます</p>
				<ul class="c-column">
					<li class="c-column__item"><a href="">
						<img defer src="<?php echo get_stylesheet_directory_uri()?>/images/tax.jpg" alt="">
						<p>法人税務顧問</p></a>
					</li>
					<li class="c-column__item"><a href="">
						<img defer src="<?php echo get_stylesheet_directory_uri()?>/images/tax.jpg" alt="">
						<p>法人税務顧問</p></a>
					</li>
					<li class="c-column__item"><a href="">
						<img defer src="<?php echo get_stylesheet_directory_uri()?>/images/tax.jpg" alt="">
						<p>法人税務顧問</p></a>
					</li>
					<li class="c-column__item"><a href="">
						<img defer src="<?php echo get_stylesheet_directory_uri()?>/images/tax.jpg" alt="">
						<p>法人税務顧問</p></a>
					</li>
					<li class="c-column__item"><a href="">
						<img defer src="<?php echo get_stylesheet_directory_uri()?>/images/tax.jpg" alt="">
						<p>法人税務顧問</p></a>
					</li>
					<li class="c-column__item"><a href="">
						<img defer src="<?php echo get_stylesheet_directory_uri()?>/images/tax.jpg" alt="">
						<p>法人税務顧問</p></a>
					</li>
				</ul>

				<div class="endcontent">
					<img defer src="<?php echo get_stylesheet_directory_uri()?>/images/img_more05.png" alt="">
					<img defer src="<?php echo get_stylesheet_directory_uri()?>/images/img_more06.png" alt="">
				</div>
			</div>
		</div>
	</main>

<?php get_footer();?>