<?php get_header() ;?>
<!-- ▼Slider ================================================== -->
<div class="l-mainvisual">
    <?php if( have_rows('slide','option') ): ?>
    <div class="mv">
        <?php while( have_rows('slide','option') ): the_row(); 
                // vars
                $image = get_sub_field('image_slide');
                $link = get_sub_field('link_slide');
                ?>
        <?php if( $link ): ?>
        <a href="<?php echo $link; ?>">
            <?php endif; ?>
            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
            <?php if( $link ): ?>
        </a>
        <?php endif; ?>
        <?php endwhile; ?>
    </div>
    <?php endif; ?>
</div>
<!-- ▲Slider ================================================== -->

<!-- ▼Content ================================================== -->
<main class="p-home">

    <!-- ▼Service ================================================== -->
    <section class="service">
        <div class="l-container">
            <h2 class="c-title"><span>幅広い案件に対応できるひかりのワンストップサービス</span>目的に応じて、最適な方法をご提案できます</h2>
            <div class="service_inner">
                <div class="service_item">
                    <img defer src="<?php echo get_stylesheet_directory_uri()?>/images/img_service01.png" alt="">
                </div>
                <div class="service_item">
                    <img defer src="<?php echo get_stylesheet_directory_uri()?>/images/img_service02.png" alt="">
                </div>
                <div class="service_item">
                    <img defer src="<?php echo get_stylesheet_directory_uri()?>/images/img_service03.png" alt="">
                </div>
                <div class="service_item">
                    <img defer src="<?php echo get_stylesheet_directory_uri()?>/images/img_service04.png" alt="">
                </div>
            </div>

            <div class="l-btn l-btn--2btn">
                <div class="c-btn">
                    <a href="<?php echo site_url('/service/'); ?>">ひかり税理士法人のサービス一覧を見る</a>
                </div>
                <div class="c-btn">
                    <a href="<?php echo site_url('/cases/'); ?>">ひかり税理士法人の成功事例を見る</a>
                </div>
            </div>
        </div>
    </section>
    <!-- ▲Service ================================================== -->

    <!-- ▼News ================================================== -->
    <section class="news">
        <div class="l-container">
            <h2 class="c-title1">
                <span class="ja">ニュース</span>
                <span class="en">News</span>
            </h2>

            <div class="news_inner">
					<ul class="c-tabs">
						<li data-content="all" class="active"><span>すべて</span></li>
						<li data-content="cat_1" class="c-bgcat--1"><span>お知らせ</span></li>
						<li data-content="cat_2" class="c-bgcat--2"><span>税の最新情報</span></li>
						<li data-content="cat_3" class="c-bgcat--3"><span>税制改正</span></li>
						<li data-content="cat_4" class="c-bgcat--4"><span>掲載情報</span></li>
						<li data-content="cat_5" class="c-bgcat--5"><span>バックナンバー</span></li>
					</ul>

            <div class="c-tabs__content">
                <?php
			$categories = get_the_category($post->ID);
			if ($categories)
			{
				$category_ids = array();
				foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
		
				$args=array(
				'category__in' => $category_ids,
				'showposts'=>10,
				'caller_get_posts'=>1
				);
				$my_query = new wp_query($args);
				if( $my_query->have_posts() )
				{
					echo '<ul class="c-listpost active" id="all">';
					while ($my_query->have_posts())
					{
						$my_query->the_post();
						?>
						<li class="c-listpost__item">
							<div class="c-listpost__info">
								<span class="c-listpost__date"><?php the_time("Y年m月d日")?></span>
								<span class="c-listpost__cat c-dotcat c-dotcat--1"> <?php the_category($post->ID); ?></span>
							</div>
							<h3 class="c-listpost__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						</li>
						<?php
					}
					echo '</ul>';
				}
			}
		?>
            </div>
            <div class="l-btn">
                <div class="c-btn c-btn--small">
                    <a href="news.html">ニュース一覧を見る</a>
                </div>
            </div>
        </div>
        </div>
    </section>
    <!-- ▲News ================================================== -->

    <!-- ▼Publish ================================================== -->

    <!-- ▲Publish ================================================== -->
    <div class="l-container">
   <?php include(locate_template('inc/loop-cases.php'));?>
    </div>
</main>
<!-- ▲Content ================================================== -->
<?php get_footer() ;?>