<?php get_header(); ?>
<?php
$tax = get_query_var('taxonomy') ;  // Get current taxonomy was registered
$taxonomy_obj = get_taxonomy($tax);
$post_type = $taxonomy_obj->object_type[0]; //Get Custom Post Type of current taxonomy

$args = array('post_type' => $post_type,'posts_per_page'=>-1,
    'tax_query' => array(
        array(
            'taxonomy' => $tax,
            'field' => 'slug',
            'terms' => get_queried_object()->slug,
        ),
    ),
 );
query_posts($args);
?>

<!-- ▼content ================================================== -->
<main>
	<div class="l-container">
		<?php include(locate_template('inc/loop-'.$post_type.'.php')); ?>
	</div>
</main>
<!-- ▲content ================================================== -->

<?php get_footer();?>