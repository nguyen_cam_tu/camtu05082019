
<?php get_header(); ?>
<?php  /* Template Name: Service */ ?>
<?php  /* Template Name: Cases */ ?>
<?php
	$post_type = get_query_var('post_type');
	$args = array(
		'posts_per_page'=>5, //Show 5 post
		'post_type'=> $post_type,
		'paged'=> get_query_var('paged')
	);
	query_posts($args);
?>

<!-- ▼content ================================================== -->
<main>
	<div class="l-container">
		<?php 
		if($post_type){
			include(locate_template('inc/loop-cases.php')); // use for Custom Post Type
		}else{
			include(locate_template('inc/loop-default.php')); // use for Posts
		}
		?>
	</div>
</main>
<!-- ▲content ================================================== -->

<?php get_footer();?>