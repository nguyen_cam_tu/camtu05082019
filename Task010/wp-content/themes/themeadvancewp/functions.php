<?php
define('THEME_URL', get_stylesheet_directory());
add_filter('show_admin_bar', '__return_false');

/**
 * Setup themes
 * 
 */
if(!function_exists('agl_theme_setup'))
{
	function agl_theme_setup()
	{
		add_theme_support('automatic-feed-links');
		add_theme_support( 'post-thumbnails' );
        add_theme_support( 'post-formats', array(
            'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
        ) );
    /* Add menu */
    add_theme_support( 'menus' );
    register_nav_menus(
      array(
        'primary-menu' => 'Main Menu',
        'footer-menu' => 'Footer Menu'
        )
    );
    add_theme_support(
      'html5',
      array(
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
      ));
	}
	add_action('init','agl_theme_setup');
}


/**
*Create function pagination
**/
function wp_corenavi_table($custom_query = null) {
  global $wp_query;
  if($custom_query) $main_query = $custom_query;
  else $main_query = $wp_query;
  $big = 999999999;
  $total = isset($main_query->max_num_pages)?$main_query->max_num_pages:'';
  if($total > 1) echo '<div class="c-pnav">';
  echo paginate_links( array(
     'base'        => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
     'format'   => '?paged=%#%',
     'current'  => max( 1, get_query_var('paged') ),
     'total'    => $total,
     'mid_size' => '10',
     'prev_text'    => __(' ','camtu'),
     'next_text'    => __(' ','camtu'),
  ) );
  if($total > 1) echo '</div>';}

/**
 * Options page (slider)
 */
if( function_exists('acf_add_options_page') ) {
	
    acf_add_options_page();
   
    acf_add_options_sub_page(
      array(
        'page_title' => 'Slider',
        'menu_title' =>'Slider',
        'menu_slug' => 'slider',
        'capability' =>'edit_posts',
        'parent_slug' => '',
        'position' =>false,
        'icon_url' => false,
        'redirect' =>false
      )
    );
      
  }

  /**
   * Breadcrumb
   */
  function the_breadcrumb() {
    echo '<div class="c-breadcrumb">';
    echo '<div class="l-container">';
if (!is_home()) {
    echo '<a href="';
    echo get_option('home');
    echo '">';
    echo 'Home';
    echo "</a> ";
   
    if (is_category() || is_single()) {
            the_category('""');
            if (is_single()) {
                echo "<span>"; 
                the_title(); echo "</span>";
            }
    } elseif (is_page()) {
        echo "<span>"; 
        the_title(); echo "</span>";
    }
}
elseif (is_tag()) {single_tag_title();}
elseif (is_day()) {echo"<div>Archive for "; the_time('F jS, Y'); echo'</div>';}
elseif (is_month()) {echo"<div>Archive for "; the_time('F, Y'); echo'</div>';}
elseif (is_year()) {echo"<div>Archive for "; the_time('Y'); echo'</div>';}
elseif (is_author()) {echo"<div>Author Archive"; echo'</div>';}
elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<div>Blog Archives"; echo'</div>';}
elseif (is_search()) {echo"<div>Search Results"; echo'</div>';}
echo '</div>';
echo '</div>';
}

/**
 * Register Custom Post Type Case
 * 
 */
register_post_type('case', array(
  'labels'        => array(
      'name'               => __('Case'),
      'singular_name'      => __('Case'),
      'menu_name'          => __('Case'),
      'all_items'          => __('All Post'),
      'view_item'          => __( 'View Post'),
      'add_new_item'       => __( 'Add New'),
      'add_new'            => __( 'Add New'),
      'edit_item'          => __( 'Edit'),
      'update_item'        => __( 'Update'),
      'search_items'       => __( 'Search'),
      'not_found'          =>  __('No case Found'),
      'not_found_in_trash' => __('No case Found In Trash'), 
      'parent_item_colon'  => ''
  ),
  'public'        => true,
  'rewrite'       => array('slug' => 'case'),
  'has_archive'   => true,
  'hierarchical'  => false,
  'show_ui'       => true,
  'show_in_menu'  => true,
  'menu_position' => 5,
  'menu_icon'     => 'dashicons-admin-post',
  'supports'      => array(
      'title',
      'editor',
      'excerpt',
      'thumbnail',
      'comments'
  ),
));

/**
* Register Taxonomy of Custom Post Type Case
* 
*/
function create_case_taxonomies() {
  $labels = array(
      'name'          => __( 'Taxonomy'),
      'singular_name' => __( 'Taxonomy'),
      'search_items'  => __( 'Search Taxonomy'),
      'all_items'     => __( 'All Taxonomy'),
      'edit_item'     => __( 'Edit Taxonomy'),
      'update_item'   => __( 'Update Taxonomy'),
      'add_new_item'  => __( 'Add New Taxonomy'),
      'new_item_name' => __( 'New Taxonomy Name'),
      'menu_name'     => __( 'Taxonomy'),
  );

  $args = array(
      'hierarchical'      => true,
      'labels'            => $labels,
      'show_ui'           => true,
      'show_admin_column' => true,
      'query_var'         => true,
      'rewrite'           => array( 'slug' => 'case_cat' ),
  );

  register_taxonomy( 'case_cat', array('case'), $args );
}
add_action( 'init', 'create_case_taxonomies', 0 );
?>