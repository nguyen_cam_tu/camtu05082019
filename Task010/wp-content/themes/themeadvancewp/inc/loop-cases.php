<?php 
     $loop = new WP_Query(array('post_type' => 'case', 'posts_per_page' => 10, 'order'=> 'ASC')); 
    ?>
<?php while ( $loop->have_posts() ) : $loop->the_post(); ?>
<div class="c-listpost2">
    <div class="c-listpost2__thumb">
        <?php the_post_thumbnail('full');?>

    </div>
    <div class="c-listpost2__info">
        <a class="cat"><?php the_taxonomies(); ?></a>
        <h3 class="titlepost"><a href="<?php the_permalink() ;?>"><?php the_title() ;?></a></h3>
        <p class="desc"><?php the_excerpt(); ?></p>
        <p class="name">京都市 S社様（印刷業）</p>
    </div>

</div>
<?php endwhile;?>