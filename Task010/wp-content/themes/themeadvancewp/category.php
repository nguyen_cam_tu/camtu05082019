<?php get_header(); ?>

<!-- ▼content ================================================== -->
<main class="p-news">
    <?php the_breadcrumb(); ?>

    <div class="c-headpage">
        <h2 class="c-title">ニュース・お知らせ</h2>
    </div>

    <div class="p-news__content">
        <div class="l-container">
            <?php if (have_posts()): ?>
            <ul class="c-listpost">
                <?php while(have_posts() ) : the_post();?>
                <li class="c-listpost__item">
                <div class="c-listpost__info">
                    <span class="c-listpost__date"><?php the_time("Y年m月d日")?></span>
                    <span class="c-listpost__cat c-dotcat c-dotcat--1"> <?php echo the_category($post->ID) ; ?></span>
                </div>
                <h3 class="c-listpost__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                </li>
                <?php endwhile;?>
            </ul>
            <?php endif;?>
        </div>
</main>
<!-- ▲content ================================================== -->

<?php get_footer();?>