-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 03, 2019 at 12:36 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `advance_wp`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-09-30 03:35:31', '2019-09-30 03:35:31', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost:8080/Project/Task010', 'yes'),
(2, 'home', 'http://localhost:8080/Project/Task010', 'yes'),
(3, 'blogname', 'Advance WordPress', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'camtun80@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:89:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'themeadvancewp', 'yes'),
(41, 'stylesheet', 'themeadvancewp', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'cron', 'a:6:{i:1570098933;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1570116933;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1570160132;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1570160156;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1570160159;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'recovery_keys', 'a:0:{}', 'yes'),
(114, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:2:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.3-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2.3-partial-2.zip\";s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.3\";s:7:\"version\";s:5:\"5.2.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:5:\"5.2.2\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.3-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2.3-partial-2.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.3-rollback-2.zip\";}s:7:\"current\";s:5:\"5.2.3\";s:7:\"version\";s:5:\"5.2.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:5:\"5.2.2\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1570091820;s:15:\"version_checked\";s:5:\"5.2.2\";s:12:\"translations\";a:0:{}}', 'no'),
(119, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1570091824;s:7:\"checked\";a:1:{s:14:\"themeadvancewp\";s:3:\"1.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(120, '_site_transient_timeout_browser_40d2af28a4c309bbb824dc957af59b11', '1570419357', 'no'),
(121, '_site_transient_browser_40d2af28a4c309bbb824dc957af59b11', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"77.0.3865.90\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(122, '_site_transient_timeout_php_check_a5b4d2808570efd012607394df5c6fa9', '1570419358', 'no'),
(123, '_site_transient_php_check_a5b4d2808570efd012607394df5c6fa9', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(126, 'can_compress_scripts', '1', 'no'),
(139, 'theme_mods_twentynineteen', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1569814587;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(140, 'current_theme', 'Advance wordpress', 'yes'),
(141, 'theme_mods_themeadvancewp', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:12:\"primary-menu\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(142, 'theme_switched', '', 'yes'),
(143, 'recovery_mode_email_last_sent', '1569815266', 'yes'),
(150, 'recently_activated', 'a:0:{}', 'yes'),
(151, 'acf_version', '5.8.2', 'yes'),
(154, 'options_slide_0_image_slide', '11', 'no'),
(155, '_options_slide_0_image_slide', 'field_5d9182886598c', 'no'),
(156, 'options_slide_0_link_slide', '', 'no'),
(157, '_options_slide_0_link_slide', 'field_5d9182b96598d', 'no'),
(158, 'options_slide_1_image_slide', '12', 'no'),
(159, '_options_slide_1_image_slide', 'field_5d9182886598c', 'no'),
(160, 'options_slide_1_link_slide', '', 'no'),
(161, '_options_slide_1_link_slide', 'field_5d9182b96598d', 'no'),
(162, 'options_slide_2_image_slide', '11', 'no'),
(163, '_options_slide_2_image_slide', 'field_5d9182886598c', 'no'),
(164, 'options_slide_2_link_slide', '', 'no'),
(165, '_options_slide_2_link_slide', 'field_5d9182b96598d', 'no'),
(166, 'options_slide', '3', 'no'),
(167, '_options_slide', 'field_5d9182656598b', 'no'),
(169, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(209, 'category_children', 'a:0:{}', 'yes'),
(228, '_transient_timeout_acf_plugin_updates', '1570264622', 'no'),
(229, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"5.8.4\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:3:\"5.2\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.8.2\";}}', 'no'),
(230, '_site_transient_timeout_theme_roots', '1570093623', 'no'),
(231, '_site_transient_theme_roots', 'a:1:{s:14:\"themeadvancewp\";s:7:\"/themes\";}', 'no'),
(232, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1570091825;s:7:\"checked\";a:3:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.8.2\";s:19:\"akismet/akismet.php\";s:5:\"4.1.2\";s:9:\"hello.php\";s:5:\"1.7.2\";}s:8:\"response\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"5.8.4\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:3:\"5.2\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:2:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(234, 'case_cat_children', 'a:0:{}', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(5, 6, '_edit_last', '1'),
(6, 6, '_edit_lock', '1569817246:1'),
(7, 11, '_wp_attached_file', '2019/09/mv0.png'),
(8, 11, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:869;s:4:\"file\";s:15:\"2019/09/mv0.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"mv0-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"mv0-300x163.png\";s:5:\"width\";i:300;s:6:\"height\";i:163;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"mv0-768x417.png\";s:5:\"width\";i:768;s:6:\"height\";i:417;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"mv0-1024x556.png\";s:5:\"width\";i:1024;s:6:\"height\";i:556;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(9, 12, '_wp_attached_file', '2019/09/mv2.png'),
(10, 12, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1600;s:6:\"height\";i:869;s:4:\"file\";s:15:\"2019/09/mv2.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"mv2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"mv2-300x163.png\";s:5:\"width\";i:300;s:6:\"height\";i:163;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:15:\"mv2-768x417.png\";s:5:\"width\";i:768;s:6:\"height\";i:417;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:16:\"mv2-1024x556.png\";s:5:\"width\";i:1024;s:6:\"height\";i:556;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(11, 13, '_edit_lock', '1569818804:1'),
(12, 15, '_edit_lock', '1570098875:1'),
(13, 17, '_edit_lock', '1569818164:1'),
(14, 19, '_edit_lock', '1570098453:1'),
(15, 21, '_menu_item_type', 'post_type'),
(16, 21, '_menu_item_menu_item_parent', '0'),
(17, 21, '_menu_item_object_id', '19'),
(18, 21, '_menu_item_object', 'page'),
(19, 21, '_menu_item_target', ''),
(20, 21, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(21, 21, '_menu_item_xfn', ''),
(22, 21, '_menu_item_url', ''),
(24, 22, '_menu_item_type', 'post_type'),
(25, 22, '_menu_item_menu_item_parent', '0'),
(26, 22, '_menu_item_object_id', '17'),
(27, 22, '_menu_item_object', 'page'),
(28, 22, '_menu_item_target', ''),
(29, 22, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(30, 22, '_menu_item_xfn', ''),
(31, 22, '_menu_item_url', ''),
(33, 23, '_menu_item_type', 'post_type'),
(34, 23, '_menu_item_menu_item_parent', '0'),
(35, 23, '_menu_item_object_id', '15'),
(36, 23, '_menu_item_object', 'page'),
(37, 23, '_menu_item_target', ''),
(38, 23, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(39, 23, '_menu_item_xfn', ''),
(40, 23, '_menu_item_url', ''),
(42, 24, '_menu_item_type', 'post_type'),
(43, 24, '_menu_item_menu_item_parent', '0'),
(44, 24, '_menu_item_object_id', '13'),
(45, 24, '_menu_item_object', 'page'),
(46, 24, '_menu_item_target', ''),
(47, 24, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(48, 24, '_menu_item_xfn', ''),
(49, 24, '_menu_item_url', ''),
(51, 3, '_wp_trash_meta_status', 'draft'),
(52, 3, '_wp_trash_meta_time', '1569818899'),
(53, 3, '_wp_desired_post_slug', 'privacy-policy'),
(54, 2, '_wp_trash_meta_status', 'publish'),
(55, 2, '_wp_trash_meta_time', '1569818900'),
(56, 2, '_wp_desired_post_slug', 'sample-page'),
(57, 27, '_edit_lock', '1569828629:1'),
(58, 28, '_edit_lock', '1569830352:1'),
(59, 29, '_wp_attached_file', '2019/09/img_news.png'),
(60, 29, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1080;s:6:\"height\";i:400;s:4:\"file\";s:20:\"2019/09/img_news.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"img_news-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"img_news-300x111.png\";s:5:\"width\";i:300;s:6:\"height\";i:111;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"img_news-768x284.png\";s:5:\"width\";i:768;s:6:\"height\";i:284;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"img_news-1024x379.png\";s:5:\"width\";i:1024;s:6:\"height\";i:379;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(63, 28, '_thumbnail_id', '29'),
(66, 32, '_edit_lock', '1569830029:1'),
(67, 33, '_edit_lock', '1569830180:1'),
(70, 33, '_thumbnail_id', '29'),
(71, 35, '_edit_lock', '1569830095:1'),
(74, 33, '_wp_old_date', '2019-09-30'),
(77, 33, '_wp_old_date', '2018-10-23'),
(78, 1, '_wp_trash_meta_status', 'publish'),
(79, 1, '_wp_trash_meta_time', '1569830329'),
(80, 1, '_wp_desired_post_slug', 'hello-world'),
(81, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}'),
(82, 28, '_edit_last', '1'),
(85, 28, '_wp_old_date', '2019-09-30'),
(86, 37, '_edit_lock', '1569830342:1'),
(91, 37, '_wp_old_date', '2019-09-30'),
(92, 37, '_thumbnail_id', '29'),
(93, 39, '_edit_lock', '1569830895:1'),
(96, 39, '_thumbnail_id', '29'),
(99, 39, '_wp_old_date', '2019-09-30'),
(100, 41, '_edit_lock', '1569830989:1'),
(103, 41, '_thumbnail_id', '29'),
(106, 41, '_wp_old_date', '2019-09-30'),
(107, 43, '_edit_lock', '1569832887:1'),
(110, 43, '_thumbnail_id', '29'),
(113, 43, '_wp_old_date', '2019-09-30'),
(114, 45, '_edit_lock', '1569831095:1'),
(115, 46, '_edit_lock', '1569831356:1'),
(118, 46, '_thumbnail_id', '29'),
(121, 46, '_wp_old_date', '2019-09-30'),
(122, 48, '_edit_lock', '1569831283:1'),
(125, 48, '_thumbnail_id', '29'),
(128, 48, '_wp_old_date', '2019-09-30'),
(129, 50, '_edit_lock', '1569831342:1'),
(132, 50, '_thumbnail_id', '29'),
(135, 50, '_wp_old_date', '2019-09-30'),
(136, 52, '_edit_lock', '1569831446:1'),
(139, 52, '_thumbnail_id', '29'),
(142, 52, '_wp_old_date', '2019-09-30'),
(143, 54, '_edit_lock', '1569831497:1'),
(146, 54, '_thumbnail_id', '29'),
(149, 54, '_wp_old_date', '2019-09-30'),
(150, 56, '_edit_lock', '1569831643:1'),
(151, 57, '_edit_lock', '1569831549:1'),
(156, 57, '_wp_old_date', '2019-09-30'),
(157, 59, '_edit_lock', '1569832126:1'),
(162, 59, '_wp_old_date', '2019-09-30'),
(163, 61, '_edit_lock', '1569832340:1'),
(166, 61, '_thumbnail_id', '29'),
(169, 61, '_wp_old_date', '2019-09-30'),
(170, 63, '_edit_lock', '1569832406:1'),
(173, 63, '_thumbnail_id', '29'),
(176, 63, '_wp_old_date', '2019-09-30'),
(177, 65, '_edit_lock', '1569832774:1'),
(180, 65, '_thumbnail_id', '29'),
(183, 65, '_wp_old_date', '2019-09-30'),
(184, 68, '_edit_lock', '1569832722:1'),
(187, 68, '_thumbnail_id', '29'),
(190, 68, '_wp_old_date', '2019-09-30'),
(195, 43, '_edit_last', '1'),
(198, 43, '_wp_old_date', '2018-10-22'),
(201, 65, '_wp_old_date', '2018-10-04'),
(202, 70, '_edit_lock', '1569838931:1'),
(205, 70, '_thumbnail_id', '29'),
(208, 70, '_wp_old_date', '2019-09-30'),
(209, 72, '_edit_lock', '1570094825:1'),
(210, 73, '_edit_lock', '1570094906:1'),
(211, 74, '_edit_lock', '1570096919:1'),
(212, 76, '_wp_attached_file', '2019/10/case1.jpg'),
(213, 76, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:800;s:6:\"height\";i:800;s:4:\"file\";s:17:\"2019/10/case1.jpg\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"case1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"case1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"case1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(214, 75, '_edit_last', '1'),
(215, 75, '_edit_lock', '1570097843:1'),
(216, 75, '_thumbnail_id', '76'),
(217, 19, '_wp_page_template', 'archive.php');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-09-30 03:35:31', '2019-09-30 03:35:31', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world__trashed', '', '', '2019-09-30 07:58:49', '2019-09-30 07:58:49', '', 0, 'http://localhost:8080/Project/Task010/?p=1', 0, 'post', '', 1),
(2, 1, '2019-09-30 03:35:31', '2019-09-30 03:35:31', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost:8080/Project/Task010/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'trash', 'closed', 'open', '', 'sample-page__trashed', '', '', '2019-09-30 04:48:20', '2019-09-30 04:48:20', '', 0, 'http://localhost:8080/Project/Task010/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-09-30 03:35:31', '2019-09-30 03:35:31', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost:8080/Project/Task010.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'trash', 'closed', 'open', '', 'privacy-policy__trashed', '', '', '2019-09-30 04:48:19', '2019-09-30 04:48:19', '', 0, 'http://localhost:8080/Project/Task010/?page_id=3', 0, 'page', '', 0),
(4, 1, '2019-09-30 03:35:59', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-30 03:35:59', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/Project/Task010/?p=4', 0, 'post', '', 0),
(6, 1, '2019-09-30 04:22:29', '2019-09-30 04:22:29', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"slider\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Slider', 'slider', 'publish', 'closed', 'closed', '', 'group_5d918254d2087', '', '', '2019-09-30 04:23:05', '2019-09-30 04:23:05', '', 0, 'http://localhost:8080/Project/Task010/?post_type=acf-field-group&#038;p=6', 0, 'acf-field-group', '', 0),
(7, 1, '2019-09-30 04:22:30', '2019-09-30 04:22:30', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Slide', 'slide', 'publish', 'closed', 'closed', '', 'field_5d9182656598b', '', '', '2019-09-30 04:22:30', '2019-09-30 04:22:30', '', 6, 'http://localhost:8080/Project/Task010/?post_type=acf-field&p=7', 0, 'acf-field', '', 0),
(8, 1, '2019-09-30 04:22:30', '2019-09-30 04:22:30', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image slide', 'image_slide', 'publish', 'closed', 'closed', '', 'field_5d9182886598c', '', '', '2019-09-30 04:22:30', '2019-09-30 04:22:30', '', 7, 'http://localhost:8080/Project/Task010/?post_type=acf-field&p=8', 0, 'acf-field', '', 0),
(9, 1, '2019-09-30 04:22:30', '2019-09-30 04:22:30', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Link slide', 'link_slide', 'publish', 'closed', 'closed', '', 'field_5d9182b96598d', '', '', '2019-09-30 04:22:30', '2019-09-30 04:22:30', '', 7, 'http://localhost:8080/Project/Task010/?post_type=acf-field&p=9', 1, 'acf-field', '', 0),
(10, 1, '2019-09-30 04:22:39', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-30 04:22:39', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/Project/Task010/?post_type=acf-field-group&p=10', 0, 'acf-field-group', '', 0),
(11, 1, '2019-09-30 04:28:30', '2019-09-30 04:28:30', '', 'mv0', '', 'inherit', 'open', 'closed', '', 'mv0', '', '', '2019-09-30 04:28:30', '2019-09-30 04:28:30', '', 0, 'http://localhost:8080/Project/Task010/wp-content/uploads/2019/09/mv0.png', 0, 'attachment', 'image/png', 0),
(12, 1, '2019-09-30 04:28:32', '2019-09-30 04:28:32', '', 'mv2', '', 'inherit', 'open', 'closed', '', 'mv2', '', '', '2019-09-30 04:28:32', '2019-09-30 04:28:32', '', 0, 'http://localhost:8080/Project/Task010/wp-content/uploads/2019/09/mv2.png', 0, 'attachment', 'image/png', 0),
(13, 1, '2019-09-30 04:37:42', '2019-09-30 04:37:42', '', 'サービス', '', 'publish', 'closed', 'closed', '', 'service', '', '', '2019-09-30 04:49:01', '2019-09-30 04:49:01', '', 0, 'http://localhost:8080/Project/Task010/?page_id=13', 0, 'page', '', 0),
(14, 1, '2019-09-30 04:37:42', '2019-09-30 04:37:42', '', 'サービス', '', 'inherit', 'closed', 'closed', '', '13-revision-v1', '', '', '2019-09-30 04:37:42', '2019-09-30 04:37:42', '', 13, 'http://localhost:8080/Project/Task010/2019/09/30/13-revision-v1/', 0, 'revision', '', 0),
(15, 1, '2019-09-30 04:38:03', '2019-09-30 04:38:03', '', '成功事例', '', 'publish', 'closed', 'closed', '', 'cases', '', '', '2019-09-30 04:49:38', '2019-09-30 04:49:38', '', 0, 'http://localhost:8080/Project/Task010/?page_id=15', 0, 'page', '', 0),
(16, 1, '2019-09-30 04:38:03', '2019-09-30 04:38:03', '', '成功事例', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2019-09-30 04:38:03', '2019-09-30 04:38:03', '', 15, 'http://localhost:8080/Project/Task010/2019/09/30/15-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2019-09-30 04:38:25', '2019-09-30 04:38:25', '', '出版物', '', 'publish', 'closed', 'closed', '', '%e5%87%ba%e7%89%88%e7%89%a9', '', '', '2019-09-30 04:38:25', '2019-09-30 04:38:25', '', 0, 'http://localhost:8080/Project/Task010/?page_id=17', 0, 'page', '', 0),
(18, 1, '2019-09-30 04:38:25', '2019-09-30 04:38:25', '', '出版物', '', 'inherit', 'closed', 'closed', '', '17-revision-v1', '', '', '2019-09-30 04:38:25', '2019-09-30 04:38:25', '', 17, 'http://localhost:8080/Project/Task010/2019/09/30/17-revision-v1/', 0, 'revision', '', 0),
(19, 1, '2019-09-30 04:38:42', '2019-09-30 04:38:42', '', 'お問い合わせ', '', 'publish', 'closed', 'closed', '', '%e3%81%8a%e5%95%8f%e3%81%84%e5%90%88%e3%82%8f%e3%81%9b', '', '', '2019-10-03 10:20:19', '2019-10-03 10:20:19', '', 0, 'http://localhost:8080/Project/Task010/?page_id=19', 0, 'page', '', 0),
(20, 1, '2019-09-30 04:38:42', '2019-09-30 04:38:42', '', 'お問い合わせ', '', 'inherit', 'closed', 'closed', '', '19-revision-v1', '', '', '2019-09-30 04:38:42', '2019-09-30 04:38:42', '', 19, 'http://localhost:8080/Project/Task010/2019/09/30/19-revision-v1/', 0, 'revision', '', 0),
(21, 1, '2019-09-30 04:39:47', '2019-09-30 04:39:47', ' ', '', '', 'publish', 'closed', 'closed', '', '21', '', '', '2019-09-30 04:39:59', '2019-09-30 04:39:59', '', 0, 'http://localhost:8080/Project/Task010/?p=21', 4, 'nav_menu_item', '', 0),
(22, 1, '2019-09-30 04:39:47', '2019-09-30 04:39:47', ' ', '', '', 'publish', 'closed', 'closed', '', '22', '', '', '2019-09-30 04:39:59', '2019-09-30 04:39:59', '', 0, 'http://localhost:8080/Project/Task010/?p=22', 3, 'nav_menu_item', '', 0),
(23, 1, '2019-09-30 04:39:46', '2019-09-30 04:39:46', ' ', '', '', 'publish', 'closed', 'closed', '', '23', '', '', '2019-09-30 04:39:59', '2019-09-30 04:39:59', '', 0, 'http://localhost:8080/Project/Task010/?p=23', 2, 'nav_menu_item', '', 0),
(24, 1, '2019-09-30 04:39:46', '2019-09-30 04:39:46', ' ', '', '', 'publish', 'closed', 'closed', '', '24', '', '', '2019-09-30 04:39:59', '2019-09-30 04:39:59', '', 0, 'http://localhost:8080/Project/Task010/?p=24', 1, 'nav_menu_item', '', 0),
(25, 1, '2019-09-30 04:48:19', '2019-09-30 04:48:19', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost:8080/Project/Task010.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2019-09-30 04:48:19', '2019-09-30 04:48:19', '', 3, 'http://localhost:8080/Project/Task010/2019/09/30/3-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2019-09-30 04:48:20', '2019-09-30 04:48:20', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost:8080/Project/Task010/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-09-30 04:48:20', '2019-09-30 04:48:20', '', 2, 'http://localhost:8080/Project/Task010/2019/09/30/2-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2019-09-30 07:32:06', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-30 07:32:06', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/Project/Task010/?p=27', 0, 'post', '', 0),
(28, 1, '2018-10-23 07:34:19', '2018-10-23 07:34:19', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '2018年12月12日 就職活動中の方向けに京都事務所で事務所見学会を開催します。', '', 'publish', 'open', 'open', '', '2018%e5%b9%b412%e6%9c%8812%e6%97%a5-%e5%b0%b1%e8%81%b7%e6%b4%bb%e5%8b%95%e4%b8%ad%e3%81%ae%e6%96%b9%e5%90%91%e3%81%91%e3%81%ab%e4%ba%ac%e9%83%bd%e4%ba%8b%e5%8b%99%e6%89%80%e3%81%a7%e4%ba%8b%e5%8b%99', '', '', '2019-09-30 07:59:11', '2019-09-30 07:59:11', '', 0, 'http://localhost:8080/Project/Task010/?p=28', 0, 'post', '', 0),
(29, 1, '2019-09-30 07:34:08', '2019-09-30 07:34:08', '', 'img_news', '', 'inherit', 'open', 'closed', '', 'img_news', '', '', '2019-09-30 07:34:08', '2019-09-30 07:34:08', '', 28, 'http://localhost:8080/Project/Task010/wp-content/uploads/2019/09/img_news.png', 0, 'attachment', 'image/png', 0),
(30, 1, '2019-09-30 07:34:19', '2019-09-30 07:34:19', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '2018年12月12日 就職活動中の方向けに京都事務所で事務所見学会を開催します。', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2019-09-30 07:34:19', '2019-09-30 07:34:19', '', 28, 'http://localhost:8080/Project/Task010/2019/09/30/28-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2019-09-30 07:38:17', '2019-09-30 07:38:17', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '2018年12月12日 就職活動中の方向けに京都事務所で事務所見学会を開催します。', '', 'inherit', 'closed', 'closed', '', '28-autosave-v1', '', '', '2019-09-30 07:38:17', '2019-09-30 07:38:17', '', 28, 'http://localhost:8080/Project/Task010/2019/09/30/28-autosave-v1/', 0, 'revision', '', 0),
(32, 1, '2019-09-30 07:56:07', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-30 07:56:07', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/Project/Task010/?p=32', 0, 'post', '', 0),
(33, 1, '2018-10-10 07:57:05', '2018-10-10 07:57:05', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', 'ライフプラン情報誌「ＡＬＰＳ」10月号に光田CEOの執筆記事が掲載されました！', '', 'publish', 'open', 'open', '', '%e3%83%a9%e3%82%a4%e3%83%95%e3%83%97%e3%83%a9%e3%83%b3%e6%83%85%e5%a0%b1%e8%aa%8c%e3%80%8c%ef%bd%81%ef%bd%8c%ef%bd%90%ef%bd%93%e3%80%8d10%e6%9c%88%e5%8f%b7%e3%81%ab%e5%85%89%e7%94%b0ceo%e3%81%ae', '', '', '2019-09-30 07:58:26', '2019-09-30 07:58:26', '', 0, 'http://localhost:8080/Project/Task010/?p=33', 0, 'post', '', 0),
(34, 1, '2019-09-30 07:57:05', '2019-09-30 07:57:05', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', 'ライフプラン情報誌「ＡＬＰＳ」10月号に光田CEOの執筆記事が掲載されました！', '', 'inherit', 'closed', 'closed', '', '33-revision-v1', '', '', '2019-09-30 07:57:05', '2019-09-30 07:57:05', '', 33, 'http://localhost:8080/Project/Task010/2019/09/30/33-revision-v1/', 0, 'revision', '', 0),
(35, 1, '2019-09-30 07:57:12', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-30 07:57:12', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/Project/Task010/?p=35', 0, 'post', '', 0),
(36, 1, '2019-09-30 07:58:49', '2019-09-30 07:58:49', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-09-30 07:58:49', '2019-09-30 07:58:49', '', 1, 'http://localhost:8080/Project/Task010/2019/09/30/1-revision-v1/', 0, 'revision', '', 0),
(37, 1, '2018-10-05 08:00:48', '2018-10-05 08:00:48', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', 'ひかり税理士法人創立15周年記念講演と感謝の夕べを開催しました。', '', 'publish', 'open', 'open', '', '%e3%81%b2%e3%81%8b%e3%82%8a%e7%a8%8e%e7%90%86%e5%a3%ab%e6%b3%95%e4%ba%ba%e5%89%b5%e7%ab%8b15%e5%91%a8%e5%b9%b4%e8%a8%98%e5%bf%b5%e8%ac%9b%e6%bc%94%e3%81%a8%e6%84%9f%e8%ac%9d%e3%81%ae%e5%a4%95%e3%81%b9', '', '', '2019-09-30 08:01:20', '2019-09-30 08:01:20', '', 0, 'http://localhost:8080/Project/Task010/?p=37', 0, 'post', '', 0),
(38, 1, '2019-09-30 08:00:48', '2019-09-30 08:00:48', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', 'ひかり税理士法人創立15周年記念講演と感謝の夕べを開催しました。', '', 'inherit', 'closed', 'closed', '', '37-revision-v1', '', '', '2019-09-30 08:00:48', '2019-09-30 08:00:48', '', 37, 'http://localhost:8080/Project/Task010/2019/09/30/37-revision-v1/', 0, 'revision', '', 0),
(39, 1, '2018-10-04 08:02:06', '2018-10-04 08:02:06', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '【コラム：決算月で税負担が変わる！？資金繰りにも影響！？】をアップしました', '', 'publish', 'open', 'open', '', '%e3%80%90%e3%82%b3%e3%83%a9%e3%83%a0%ef%bc%9a%e6%b1%ba%e7%ae%97%e6%9c%88%e3%81%a7%e7%a8%8e%e8%b2%a0%e6%8b%85%e3%81%8c%e5%a4%89%e3%82%8f%e3%82%8b%ef%bc%81%ef%bc%9f%e8%b3%87%e9%87%91%e7%b9%b0%e3%82%8a', '', '', '2019-09-30 08:02:23', '2019-09-30 08:02:23', '', 0, 'http://localhost:8080/Project/Task010/?p=39', 0, 'post', '', 0),
(40, 1, '2019-09-30 08:02:06', '2019-09-30 08:02:06', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '【コラム：決算月で税負担が変わる！？資金繰りにも影響！？】をアップしました', '', 'inherit', 'closed', 'closed', '', '39-revision-v1', '', '', '2019-09-30 08:02:06', '2019-09-30 08:02:06', '', 39, 'http://localhost:8080/Project/Task010/2019/09/30/39-revision-v1/', 0, 'revision', '', 0),
(41, 1, '2018-09-02 08:11:47', '2018-09-02 08:11:47', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '2018年9月台風21号及び台風24号により被害を受けられた皆様方へ', '', 'publish', 'open', 'open', '', '2018%e5%b9%b49%e6%9c%88%e5%8f%b0%e9%a2%a821%e5%8f%b7%e5%8f%8a%e3%81%b3%e5%8f%b0%e9%a2%a824%e5%8f%b7%e3%81%ab%e3%82%88%e3%82%8a%e8%a2%ab%e5%ae%b3%e3%82%92%e5%8f%97%e3%81%91%e3%82%89%e3%82%8c%e3%81%9f', '', '', '2019-09-30 08:12:07', '2019-09-30 08:12:07', '', 0, 'http://localhost:8080/Project/Task010/?p=41', 0, 'post', '', 0),
(42, 1, '2019-09-30 08:11:47', '2019-09-30 08:11:47', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '2018年9月台風21号及び台風24号により被害を受けられた皆様方へ', '', 'inherit', 'closed', 'closed', '', '41-revision-v1', '', '', '2019-09-30 08:11:47', '2019-09-30 08:11:47', '', 41, 'http://localhost:8080/Project/Task010/2019/09/30/41-revision-v1/', 0, 'revision', '', 0),
(43, 1, '2018-10-23 08:13:13', '2018-10-23 08:13:13', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '2018年12月12日 就職活動中の方向けに京都事務所で事務所見学会を開催します。', '', 'publish', 'open', 'open', '', '2018%e5%b9%b412%e6%9c%8812%e6%97%a5-%e5%b0%b1%e8%81%b7%e6%b4%bb%e5%8b%95%e4%b8%ad%e3%81%ae%e6%96%b9%e5%90%91%e3%81%91%e3%81%ab%e4%ba%ac%e9%83%bd%e4%ba%8b%e5%8b%99%e6%89%80%e3%81%a7%e4%ba%8b%e5%8b%99-2', '', '', '2019-09-30 08:41:27', '2019-09-30 08:41:27', '', 0, 'http://localhost:8080/Project/Task010/?p=43', 0, 'post', '', 0),
(44, 1, '2019-09-30 08:13:13', '2019-09-30 08:13:13', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '2018年12月12日 就職活動中の方向けに京都事務所で事務所見学会を開催します。', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2019-09-30 08:13:13', '2019-09-30 08:13:13', '', 43, 'http://localhost:8080/Project/Task010/2019/09/30/43-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2019-09-30 08:13:55', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-30 08:13:55', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/Project/Task010/?p=45', 0, 'post', '', 0),
(46, 1, '2018-10-10 08:15:34', '2018-10-10 08:15:34', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', 'ライフプラン情報誌「ＡＬＰＳ」10月号に光田CEOの執筆記事が掲載されました！', '', 'publish', 'open', 'open', '', '%e3%83%a9%e3%82%a4%e3%83%95%e3%83%97%e3%83%a9%e3%83%b3%e6%83%85%e5%a0%b1%e8%aa%8c%e3%80%8c%ef%bd%81%ef%bd%8c%ef%bd%90%ef%bd%93%e3%80%8d10%e6%9c%88%e5%8f%b7%e3%81%ab%e5%85%89%e7%94%b0ceo%e3%81%ae-2', '', '', '2019-09-30 08:15:51', '2019-09-30 08:15:51', '', 0, 'http://localhost:8080/Project/Task010/?p=46', 0, 'post', '', 0),
(47, 1, '2019-09-30 08:15:34', '2019-09-30 08:15:34', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', 'ライフプラン情報誌「ＡＬＰＳ」10月号に光田CEOの執筆記事が掲載されました！', '', 'inherit', 'closed', 'closed', '', '46-revision-v1', '', '', '2019-09-30 08:15:34', '2019-09-30 08:15:34', '', 46, 'http://localhost:8080/Project/Task010/2019/09/30/46-revision-v1/', 0, 'revision', '', 0),
(48, 1, '2018-10-05 08:16:38', '2018-10-05 08:16:38', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', 'ひかり税理士法人創立15周年記念講演と感謝の夕べを開催しました。', '', 'publish', 'open', 'open', '', '%e3%81%b2%e3%81%8b%e3%82%8a%e7%a8%8e%e7%90%86%e5%a3%ab%e6%b3%95%e4%ba%ba%e5%89%b5%e7%ab%8b15%e5%91%a8%e5%b9%b4%e8%a8%98%e5%bf%b5%e8%ac%9b%e6%bc%94%e3%81%a8%e6%84%9f%e8%ac%9d%e3%81%ae%e5%a4%95-2', '', '', '2019-09-30 08:17:03', '2019-09-30 08:17:03', '', 0, 'http://localhost:8080/Project/Task010/?p=48', 0, 'post', '', 0),
(49, 1, '2019-09-30 08:16:38', '2019-09-30 08:16:38', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', 'ひかり税理士法人創立15周年記念講演と感謝の夕べを開催しました。', '', 'inherit', 'closed', 'closed', '', '48-revision-v1', '', '', '2019-09-30 08:16:38', '2019-09-30 08:16:38', '', 48, 'http://localhost:8080/Project/Task010/2019/09/30/48-revision-v1/', 0, 'revision', '', 0),
(50, 1, '2018-10-04 08:17:44', '2018-10-04 08:17:44', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '【コラム：決算月で税負担が変わる！？資金繰りにも影響！？】をアップしました', '', 'publish', 'open', 'open', '', '%e3%80%90%e3%82%b3%e3%83%a9%e3%83%a0%ef%bc%9a%e6%b1%ba%e7%ae%97%e6%9c%88%e3%81%a7%e7%a8%8e%e8%b2%a0%e6%8b%85%e3%81%8c%e5%a4%89%e3%82%8f%e3%82%8b%ef%bc%81%ef%bc%9f%e8%b3%87%e9%87%91%e7%b9%b0%e3%82%8a-2', '', '', '2019-09-30 08:18:00', '2019-09-30 08:18:00', '', 0, 'http://localhost:8080/Project/Task010/?p=50', 0, 'post', '', 0),
(51, 1, '2019-09-30 08:17:44', '2019-09-30 08:17:44', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '【コラム：決算月で税負担が変わる！？資金繰りにも影響！？】をアップしました', '', 'inherit', 'closed', 'closed', '', '50-revision-v1', '', '', '2019-09-30 08:17:44', '2019-09-30 08:17:44', '', 50, 'http://localhost:8080/Project/Task010/2019/09/30/50-revision-v1/', 0, 'revision', '', 0),
(52, 1, '2018-10-02 08:18:45', '2018-10-02 08:18:45', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '2018年9月台風21号及び台風24号により被害を受けられた皆様方へ', '', 'publish', 'open', 'open', '', '2018%e5%b9%b49%e6%9c%88%e5%8f%b0%e9%a2%a821%e5%8f%b7%e5%8f%8a%e3%81%b3%e5%8f%b0%e9%a2%a824%e5%8f%b7%e3%81%ab%e3%82%88%e3%82%8a%e8%a2%ab%e5%ae%b3%e3%82%92%e5%8f%97%e3%81%91%e3%82%89%e3%82%8c%e3%81%9f-2', '', '', '2019-09-30 08:19:01', '2019-09-30 08:19:01', '', 0, 'http://localhost:8080/Project/Task010/?p=52', 0, 'post', '', 0),
(53, 1, '2019-09-30 08:18:45', '2019-09-30 08:18:45', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '2018年9月台風21号及び台風24号により被害を受けられた皆様方へ', '', 'inherit', 'closed', 'closed', '', '52-revision-v1', '', '', '2019-09-30 08:18:45', '2019-09-30 08:18:45', '', 52, 'http://localhost:8080/Project/Task010/2019/09/30/52-revision-v1/', 0, 'revision', '', 0),
(54, 1, '2018-10-04 08:20:16', '2018-10-04 08:20:16', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '【コラム：決算月で税負担が変わる！？資金繰りにも影響！？】をアップしました', '', 'publish', 'open', 'open', '', '%e3%80%90%e3%82%b3%e3%83%a9%e3%83%a0%ef%bc%9a%e6%b1%ba%e7%ae%97%e6%9c%88%e3%81%a7%e7%a8%8e%e8%b2%a0%e6%8b%85%e3%81%8c%e5%a4%89%e3%82%8f%e3%82%8b%ef%bc%81%ef%bc%9f%e8%b3%87%e9%87%91%e7%b9%b0%e3%82%8a-3', '', '', '2019-09-30 08:20:33', '2019-09-30 08:20:33', '', 0, 'http://localhost:8080/Project/Task010/?p=54', 0, 'post', '', 0),
(55, 1, '2019-09-30 08:20:16', '2019-09-30 08:20:16', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '【コラム：決算月で税負担が変わる！？資金繰りにも影響！？】をアップしました', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2019-09-30 08:20:16', '2019-09-30 08:20:16', '', 54, 'http://localhost:8080/Project/Task010/2019/09/30/54-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2019-09-30 08:20:43', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-30 08:20:43', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/Project/Task010/?p=56', 0, 'post', '', 0),
(57, 1, '2018-10-02 08:21:12', '2018-10-02 08:21:12', '', '2018年9月台風21号及び台風24号により被害を受けられた皆様方へ', '', 'publish', 'open', 'open', '', '2018%e5%b9%b49%e6%9c%88%e5%8f%b0%e9%a2%a821%e5%8f%b7%e5%8f%8a%e3%81%b3%e5%8f%b0%e9%a2%a824%e5%8f%b7%e3%81%ab%e3%82%88%e3%82%8a%e8%a2%ab%e5%ae%b3%e3%82%92%e5%8f%97%e3%81%91%e3%82%89%e3%82%8c%e3%81%9f-3', '', '', '2019-09-30 08:21:28', '2019-09-30 08:21:28', '', 0, 'http://localhost:8080/Project/Task010/?p=57', 0, 'post', '', 0),
(58, 1, '2019-09-30 08:21:12', '2019-09-30 08:21:12', '', '2018年9月台風21号及び台風24号により被害を受けられた皆様方へ', '', 'inherit', 'closed', 'closed', '', '57-revision-v1', '', '', '2019-09-30 08:21:12', '2019-09-30 08:21:12', '', 57, 'http://localhost:8080/Project/Task010/2019/09/30/57-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2018-10-02 08:21:43', '2018-10-02 08:21:43', '', '2018年9月台風21号及び台風24号により被害を受けられた皆様方へ', '', 'publish', 'open', 'open', '', '2018%e5%b9%b49%e6%9c%88%e5%8f%b0%e9%a2%a821%e5%8f%b7%e5%8f%8a%e3%81%b3%e5%8f%b0%e9%a2%a824%e5%8f%b7%e3%81%ab%e3%82%88%e3%82%8a%e8%a2%ab%e5%ae%b3%e3%82%92%e5%8f%97%e3%81%91%e3%82%89%e3%82%8c%e3%81%9f-4', '', '', '2019-09-30 08:21:56', '2019-09-30 08:21:56', '', 0, 'http://localhost:8080/Project/Task010/?p=59', 0, 'post', '', 0),
(60, 1, '2019-09-30 08:21:43', '2019-09-30 08:21:43', '', '2018年9月台風21号及び台風24号により被害を受けられた皆様方へ', '', 'inherit', 'closed', 'closed', '', '59-revision-v1', '', '', '2019-09-30 08:21:43', '2019-09-30 08:21:43', '', 59, 'http://localhost:8080/Project/Task010/2019/09/30/59-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2018-10-23 08:34:22', '2018-10-23 08:34:22', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '2018年12月12日 就職活動中の方向けに京都事務所で事務所見学会を開催します。', '', 'publish', 'open', 'open', '', '2018%e5%b9%b412%e6%9c%8812%e6%97%a5-%e5%b0%b1%e8%81%b7%e6%b4%bb%e5%8b%95%e4%b8%ad%e3%81%ae%e6%96%b9%e5%90%91%e3%81%91%e3%81%ab%e4%ba%ac%e9%83%bd%e4%ba%8b%e5%8b%99%e6%89%80%e3%81%a7%e4%ba%8b%e5%8b%99-3', '', '', '2019-09-30 08:34:39', '2019-09-30 08:34:39', '', 0, 'http://localhost:8080/Project/Task010/?p=61', 0, 'post', '', 0),
(62, 1, '2019-09-30 08:34:22', '2019-09-30 08:34:22', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '2018年12月12日 就職活動中の方向けに京都事務所で事務所見学会を開催します。', '', 'inherit', 'closed', 'closed', '', '61-revision-v1', '', '', '2019-09-30 08:34:22', '2019-09-30 08:34:22', '', 61, 'http://localhost:8080/Project/Task010/2019/09/30/61-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2018-10-10 08:35:15', '2018-10-10 08:35:15', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', 'ライフプラン情報誌「ＡＬＰＳ」10月号に光田CEOの執筆記事が掲載されました！', '', 'publish', 'open', 'open', '', '%e3%83%a9%e3%82%a4%e3%83%95%e3%83%97%e3%83%a9%e3%83%b3%e6%83%85%e5%a0%b1%e8%aa%8c%e3%80%8c%ef%bd%81%ef%bd%8c%ef%bd%90%ef%bd%93%e3%80%8d10%e6%9c%88%e5%8f%b7%e3%81%ab%e5%85%89%e7%94%b0ceo%e3%81%ae-3', '', '', '2019-09-30 08:35:46', '2019-09-30 08:35:46', '', 0, 'http://localhost:8080/Project/Task010/?p=63', 0, 'post', '', 0),
(64, 1, '2019-09-30 08:35:15', '2019-09-30 08:35:15', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', 'ライフプラン情報誌「ＡＬＰＳ」10月号に光田CEOの執筆記事が掲載されました！', '', 'inherit', 'closed', 'closed', '', '63-revision-v1', '', '', '2019-09-30 08:35:15', '2019-09-30 08:35:15', '', 63, 'http://localhost:8080/Project/Task010/2019/09/30/63-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2018-10-05 08:36:27', '2018-10-05 08:36:27', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', 'ひかり税理士法人創立15周年記念講演と感謝の夕べを開催しました。', '', 'publish', 'open', 'open', '', '%e3%81%b2%e3%81%8b%e3%82%8a%e7%a8%8e%e7%90%86%e5%a3%ab%e6%b3%95%e4%ba%ba%e5%89%b5%e7%ab%8b15%e5%91%a8%e5%b9%b4%e8%a8%98%e5%bf%b5%e8%ac%9b%e6%bc%94%e3%81%a8%e6%84%9f%e8%ac%9d%e3%81%ae%e5%a4%95-3', '', '', '2019-09-30 08:41:54', '2019-09-30 08:41:54', '', 0, 'http://localhost:8080/Project/Task010/?p=65', 0, 'post', '', 0),
(66, 1, '2019-09-30 08:36:27', '2019-09-30 08:36:27', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', 'ひかり税理士法人創立15周年記念講演と感謝の夕べを開催しました。', '', 'inherit', 'closed', 'closed', '', '65-revision-v1', '', '', '2019-09-30 08:36:27', '2019-09-30 08:36:27', '', 65, 'http://localhost:8080/Project/Task010/2019/09/30/65-revision-v1/', 0, 'revision', '', 0),
(68, 1, '2018-10-04 08:38:19', '2018-10-04 08:38:19', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '【コラム：決算月で税負担が変わる！？資金繰りにも影響！？】をアップしました', '', 'publish', 'open', 'open', '', '%e3%80%90%e3%82%b3%e3%83%a9%e3%83%a0%ef%bc%9a%e6%b1%ba%e7%ae%97%e6%9c%88%e3%81%a7%e7%a8%8e%e8%b2%a0%e6%8b%85%e3%81%8c%e5%a4%89%e3%82%8f%e3%82%8b%ef%bc%81%ef%bc%9f%e8%b3%87%e9%87%91%e7%b9%b0%e3%82%8a-4', '', '', '2019-09-30 08:41:01', '2019-09-30 08:41:01', '', 0, 'http://localhost:8080/Project/Task010/?p=68', 0, 'post', '', 0),
(69, 1, '2019-09-30 08:38:19', '2019-09-30 08:38:19', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '【コラム：決算月で税負担が変わる！？資金繰りにも影響！？】をアップしました', '', 'inherit', 'closed', 'closed', '', '68-revision-v1', '', '', '2019-09-30 08:38:19', '2019-09-30 08:38:19', '', 68, 'http://localhost:8080/Project/Task010/2019/09/30/68-revision-v1/', 0, 'revision', '', 0),
(70, 1, '2018-10-02 08:42:33', '2018-10-02 08:42:33', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '2018年9月台風21号及び台風24号により被害を受けられた皆様方へ', '', 'publish', 'open', 'open', '', '2018%e5%b9%b49%e6%9c%88%e5%8f%b0%e9%a2%a821%e5%8f%b7%e5%8f%8a%e3%81%b3%e5%8f%b0%e9%a2%a824%e5%8f%b7%e3%81%ab%e3%82%88%e3%82%8a%e8%a2%ab%e5%ae%b3%e3%82%92%e5%8f%97%e3%81%91%e3%82%89%e3%82%8c%e3%81%9f-5', '', '', '2019-09-30 08:42:52', '2019-09-30 08:42:52', '', 0, 'http://localhost:8080/Project/Task010/?p=70', 0, 'post', '', 0),
(71, 1, '2019-09-30 08:42:33', '2019-09-30 08:42:33', '<!-- wp:html -->\n<p>実務経営研究会が発行する「月刊実務経営ニュース」2018年2月号に弊所中小企業診断士 西村の記事が掲載されました。</p>\n\n					<p class=\"u-center\">▽▽詳細はこちら▽▽</p>\n\n					<p class=\"u-center u-bborder\">さあ、顧問先育成型会計事務所へ！~業務改善をもたらすMAS指導の実際~</p>\n<!-- /wp:html -->', '2018年9月台風21号及び台風24号により被害を受けられた皆様方へ', '', 'inherit', 'closed', 'closed', '', '70-revision-v1', '', '', '2019-09-30 08:42:33', '2019-09-30 08:42:33', '', 70, 'http://localhost:8080/Project/Task010/2019/09/30/70-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2019-10-03 08:37:25', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-10-03 08:37:25', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/Project/Task010/?p=72', 0, 'post', '', 0),
(73, 1, '2019-10-03 09:29:30', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-10-03 09:29:30', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/Project/Task010/?p=73', 0, 'post', '', 0),
(74, 1, '2019-10-03 09:30:54', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-10-03 09:30:54', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/Project/Task010/?p=74', 0, 'post', '', 0),
(75, 1, '2019-10-03 10:06:59', '2019-10-03 10:06:59', '<p>経営改善計画では主に、財務分析と事業分析を行い、数値計画に落とし込みます。お客様の決算書だけでなく、社内管理資料やヒアリングを通じて課題を抽出し、アクションプランとして落とし込みます。</p>\r\n\r\n					<p>今回は支援させていただいた印刷業Ｓ社様の事例を紹介させていただきます。</p>\r\n\r\n					<p>財務分析の結果、資金繰り状況が非常に悪く、金融機関返済だけでなく、仕入先への支払いも遅延している事が判明。このままでは会社存続の危機と判断。限られた時間の中、以下の支援策を社長とともに検討し、金融機関へ経営改善計画書を提出しました。</p>\r\n\r\n					<p>&nbsp;</p>\r\n\r\n					<p>支援①金融機関へリスケ要請し、長期返済まき直しにより経営危機を回避</p>\r\n\r\n					<p>支援②ビジネスモデル図による経営状態の把握</p>\r\n\r\n					<p>支援③課題設定とアクションプランの構築</p>\r\n\r\n					<p>支援④案件の受注状況の見える化</p>', 'ご提供サービス', '資金繰り悪化による経営危機を、経営改善計画取組により回避', 'publish', 'open', 'closed', '', '%e3%81%94%e6%8f%90%e4%be%9b%e3%82%b5%e3%83%bc%e3%83%93%e3%82%b9', '', '', '2019-10-03 10:06:59', '2019-10-03 10:06:59', '', 0, 'http://localhost:8080/Project/Task010/?post_type=case&#038;p=75', 0, 'case', '', 0),
(76, 1, '2019-10-03 10:05:49', '2019-10-03 10:05:49', '', 'case1', '', 'inherit', 'open', 'closed', '', 'case1', '', '', '2019-10-03 10:05:49', '2019-10-03 10:05:49', '', 75, 'http://localhost:8080/Project/Task010/wp-content/uploads/2019/10/case1.jpg', 0, 'attachment', 'image/jpeg', 0),
(77, 1, '2019-10-03 10:32:00', '2019-10-03 10:32:00', '', '成功事例', '', 'inherit', 'closed', 'closed', '', '15-autosave-v1', '', '', '2019-10-03 10:32:00', '2019-10-03 10:32:00', '', 15, 'http://localhost:8080/Project/Task010/2019/10/03/15-autosave-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'Main menu', 'main-menu', 0),
(4, 'お知らせ', '%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b', 0),
(5, '税の最新情報', '%e7%a8%8e%e3%81%ae%e6%9c%80%e6%96%b0%e6%83%85%e5%a0%b1', 0),
(6, '税制改正', '%e7%a8%8e%e5%88%b6%e6%94%b9%e6%ad%a3', 0),
(7, '掲載情報', '%e6%8e%b2%e8%bc%89%e6%83%85%e5%a0%b1', 0),
(8, 'バックナンバー', '%e3%83%90%e3%83%83%e3%82%af%e3%83%8a%e3%83%b3%e3%83%90%e3%83%bc', 0),
(9, '法人のお客様', '%e6%b3%95%e4%ba%ba%e3%81%ae%e3%81%8a%e5%ae%a2%e6%a7%98', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(21, 2, 0),
(22, 2, 0),
(23, 2, 0),
(24, 2, 0),
(28, 4, 0),
(33, 4, 0),
(37, 4, 0),
(39, 1, 0),
(41, 4, 0),
(43, 4, 0),
(46, 4, 0),
(48, 4, 0),
(50, 4, 0),
(52, 4, 0),
(54, 4, 0),
(57, 4, 0),
(59, 4, 0),
(61, 5, 0),
(63, 5, 0),
(65, 5, 0),
(68, 5, 0),
(70, 5, 0),
(75, 9, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'nav_menu', '', 0, 4),
(4, 4, 'category', '', 0, 12),
(5, 5, 'category', '', 0, 5),
(6, 6, 'category', '', 0, 0),
(7, 7, 'category', '', 0, 0),
(8, 8, 'category', '', 0, 0),
(9, 9, 'case_cat', '京都市 S社様（印刷業）', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"22795c90b1f148c2d462ede3cd3b4aba7655a6465b16ee2f91ff48029e066c6c\";a:4:{s:10:\"expiration\";i:1570264644;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1570091844;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'wp_user-settings', 'libraryContent=browse&post_dfw=off&editor=html'),
(19, 1, 'wp_user-settings-time', '1570097214'),
(20, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(21, 1, 'metaboxhidden_nav-menus', 'a:1:{i:0;s:12:\"add-post_tag\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$B2Od769fA/u0SFh7LTjXrIoCtDovkE.', 'admin', 'camtun80@gmail.com', '', '2019-09-30 03:35:30', '', 0, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=235;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=218;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
