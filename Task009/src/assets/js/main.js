/**
 * Nav Mobile
 */

function openNav() {
    document.getElementById("myNav").style.width = "100%";
    document.getElementById("top").style.transform = "translateX(-40%)";
    document.getElementById("main").style.transform = "translateX(-40%)";
    document.getElementById("footer").style.transform = "translateX(-40%)";


}

function closeNav() {
    document.getElementById("myNav").style.width = "0";
    document.getElementById("top").style.transform = "translateX(0)";
    document.getElementById("main").style.transform = "translateX(0)";
    document.getElementById("footer").style.transform = "translateX(0)";

}

/**
 * Slider full
 */

var images = new Array('assets/img/main_img01.jpg', 'assets/img/main_img02.jpg', 'assets/img/main_img03.jpg', 'assets/img/main_img04.jpg');
var nextimage = 0;

doSlideshow();

function doSlideshow() {

    if (nextimage >= images.length)
        nextimage = 0;
    $('.c-main__slide').css('background-image', 'url("' + images[nextimage++] + '")').fadeIn(3000, function() {
        setTimeout(doSlideshow, 5000);
    });
}
/**
 * Zoom hover
 * 
 */

function zoomImg(zoom_id) {
    document.getElementById(zoom_id).style.transform = "scale(1.05)";
}

function normalImg(zoom_id) {
    document.getElementById(zoom_id).style.transform = "scale(1)";

}
/**
 * Get background img
 */

function init() {
    $(".c-posts1__thumbnail").each(function(index) {
        this.style.backgroundImage = "url('" + this.children[0].src + "')";
    });
}
$(document).ready(init);

/**
 * Slide scroll horizontal
 */
$(window).on('load', function() {
    initSmoothScrolling('.item', 'smoothscroll');
});

function initSmoothScrolling(container, animation) {

    var sliderWidth = 0;
    var animationWidth = 0;
    var sliderHeight = $('>div>div:first-of-type', container).outerHeight(false);

    $('>div>div', container).each(function() {
        animationWidth += $(this).outerWidth(false);
    });

    var slidesVisible = $(container).width() / $('>div>div:first-of-type', container).outerWidth(false);
    slidesVisible = Math.ceil(slidesVisible);

    var slidesNumber = $('>div>div', container).length;
    var speed = slidesNumber;

    $('>div>div', container).slice(0, slidesVisible).clone().appendTo($('>div', container));

    $('>div>div', container).each(function() {
        sliderWidth += $(this).outerWidth(false);
    });

    $('>div', container).css({ 'width': sliderWidth, 'height': sliderHeight });

    $("<style type='text/css'>@keyframes " + animation + " { 0% { margin-left: 0px; } 100% { margin-left: -20%; } } " + $('>div>div:first-of-type', container).selector + " { -webkit-animation: " + animation + " " + speed + "s linear infinite; -moz-animation: " + animation + " " + speed + "s linear infinite; -ms-animation: " + animation + " " + speed + "s linear infinite; -o-animation: " + animation + " " + speed + "s linear infinite; animation: " + animation + " " + speed + "s linear infinite; }</style>").appendTo("head");

    var cl = $(container).attr("class");
    $(container).removeClass(cl).animate({ 'nothing': null }, 2, function() {
        $(this).addClass(cl);
    });
}