@charset "UTF-8";

/*------------------------------------------------------------
	Default
------------------------------------------------------------*/

html,
body,
div,
span,
object,
iframe,
h1,
h2,
h3,
h4,
h5,
h6,
p,
blockquote,
pre,
abbr,
address,
cite,
code,
del,
dfn,
em,
img,
ins,
kbd,
q,
samp,
small,
strong,
sub,
sup,
var,
b,
i,
dl,
dt,
dd,
ol,
ul,
li,
fieldset,
form,
label,
legend,
table,
caption,
tbody,
tfoot,
thead,
tr,
th,
td,
article,
aside,
dialog,
figure,
footer,
header,
button,
hgroup,
menu,
nav,
section,
time,
mark,
audio,
video {
    margin: 0;
    padding: 0;
    border: 0;
    outline: 0;
    font-size: 1em;
}

html {
    font-size: 62.5%;
}

body,
table,
input,
textarea,
select,
option,
button,
h1,
h2,
h3,
h4,
h5,
h6 {
    font-family: "Noto Sans JP";
    line-height: 1.1;
    font-weight: 400;
}

h1,
h2,
h3,
h4,
h5,
h6 {
    font-weight: 400;
}

table,
input,
textarea,
select,
option {
    line-height: 1.1;
}

ol,
ul {
    list-style: none;
}

blockquote,
q {
    quotes: none;
}

:focus {
    outline: 0;
}

ins {
    text-decoration: none;
}

del {
    text-decoration: line-through;
}

img {
    vertical-align: top;
}

.container {
    width: 1000px;
    margin: 0 auto;
}

.sp-only {
    display: none;
}

.pc-only {
    display: block;
}


/*------------------------------------------------------------
      adjustment class ※ no use frequently
  ------------------------------------------------------------*/

.mt0 {
    margin-top: 0 !important;
}

.mb0 {
    margin-bottom: 0 !important;
}


/*------------------------------------------------------------
      clearfix
  ------------------------------------------------------------*/

.clearfix {
    *zoom: 1;
}

.clearfix:after {
    display: block;
    clear: both;
    content: "";
}


/*------------------------------------------------------------
      Header
  ------------------------------------------------------------*/

.header {
    background-color: #0068b7;
    color: #fff;
    font-family: "Noto Sans JP";
    height: 900px;
    padding: 15px 0;
}

.header::after {
    content: "";
    display: block;
    width: 0;
    height: 0;
    position: absolute;
    left: 50%;
    top: 928px;
    transform: translate(-50%, 0%);
    border-left: 50px solid transparent;
    border-right: 50px solid transparent;
    border-top: 40px solid #0068b7;
}

.header-top {
    padding-bottom: -10px;
    border-bottom: #fff 2px solid;
    height: 108px;
}

.header-top .logo-txt {
    font-size: 1.5rem;
    margin-top: 85px;
}

.header-logo {
    float: left;
    display: flex;
}

.header .logo img {
    width: 165px;
    height: 130px;
    margin-left: 80px;
    position: relative;
    z-index: 99;
}

.header-tel img {
    width: 285px;
    height: 30px;
    padding-top: 15px;
}

.header-tel a {
    text-decoration: none;
    color: #fff;
    font-size: 1.4rem;
}

.header-tel {
    float: right;
    margin-right: 25px;
    margin-top: 30px;
}

.header-content p.subcatch {
    font-size: 2.8rem;
    text-align: center;
    line-height: 2;
}

.header-content img {
    margin: 0px auto;
}

.keycatch {
    text-align: center;
}

.keycatch img {
    margin: 50px auto;
}

.header-content {
    text-align: center;
}


/*------------------------------------------------------------
      Point
  ------------------------------------------------------------*/

.point {
    background-color: #fff;
}

.point h2 {
    font-size: 3.2rem;
    color: #000;
    text-align: center;
    margin-top: 75px;
    margin-bottom: 20px;
}

.grid-point {
    display: flex;
    flex-direction: row;
}

.item-point {
    width: 330px;
}

.item2 {
    flex-grow: 1;
}

.item-point2 hr {
    height: 0px;
    width: 100%;
    margin-left: auto;
    margin-right: 0px;
    border-top-style: solid;
    border-right-style: solid;
    border-bottom-style: solid;
    border-left-style: solid;
    border-top-color: rgb(218, 218, 218);
    border-right-color: rgb(218, 218, 218);
    border-bottom-color: rgb(218, 218, 218);
    border-left-color: rgb(218, 218, 218);
    background-color: rgb(218, 218, 218);
}

.point p.pi {
    font-size: 3rem;
    color: #0068b7;
    width: 130px;
    height: 130px;
    line-height: 130px;
    background-color: #fff000;
    text-align: center;
    border-radius: 50%;
}

.point .border {
    border-bottom: #fff000 solid 1rem;
    margin-bottom: 30px;
    color: #0068b7;
    font-weight: 700;
    font-size: 3.6rem;
    text-align: center;
    margin-top: 60px;
}

.item1 img {
    width: 300px;
}

.list-point {
    display: flex;
    flex-direction: column;
}

.list-item {
    display: flex;
    flex-direction: row;
    margin: 15px 0;
}

.item-po {
    width: 130px;
}

.item-po img {
    height: 30px;
}

.item-po1 {
    margin-right: 40px;
}

.item-po2 {
    flex-grow: 1
}

.text-point {
    font-size: 1.4rem;
    color: #000;
    line-height: 1.8;
}

.item-po2 img {
    margin-bottom: 10px;
}

.img-point3 img {
    margin: auto;
}


/*------------------------------------------------------------
      Contact
  ------------------------------------------------------------*/

.contact {
    background-color: #0068b7;
}

.contact .container {
    display: grid;
    grid-template-columns: 500px 2px 500px;
}

.contact img.tel {
    padding: 20px 20px 20px 0;
}

.contact img.mail {
    padding: 20px 0px 20px 30px;
}

.contact .container div {
    color: rgba(255, 255, 255, 0.5) !important;
    font-size: 4rem;
    padding-top: 20px;
}


/*------------------------------------------------------------
      Flow
  ------------------------------------------------------------*/

.flow {
    background-color: #ebebeb;
    background-image: url(../images/grid.png);
    background-size: cover;
    background-repeat: no-repeat;
}

.flow h3 {
    font-size: 2.2rem;
    line-height: 1.6;
    color: #000;
    text-align: center;
    margin-bottom: 80px;
}

.flow .case1 span.h2,
.flow .case2 span.h2 {
    color: #0068b7;
    font-size: 3rem;
}

.title-case {
    text-align: center;
    padding-top: 60px;
    padding-bottom: 30px;
}

.h2 {
    margin-left: 20px;
}


/*------------------------------------------------------------
      Case1
  ------------------------------------------------------------*/

.columns {
    display: flex;
}

.columns .col {
    margin-right: 43px;
    border: #0068b7 dotted 2px;
    background-color: #fff;
}

.columns .col:nth-child(3n) {
    margin-right: 0 !important;
}

.bg-lable {
    background-color: #0068b7;
    height: 50px;
    margin: 10px;
}

.bg-lable img {
    float: right;
    margin-top: -20px;
    margin-right: -25px;
}

.img-case1 {
    width: 175px;
}

.bg-lable img.ex {
    float: left;
    padding-top: 35px;
    padding-left: 10px;
}

table.col-price {
    padding: 0px 20px 20px 20px;
    width: 300px;
}

table.col-price td,
table.col-price th {
    line-height: 36px;
    ;
    font-size: 1.8rem;
}

table.col-price th {
    text-align: left;
}

td.price {
    text-align: right;
}

.case1 .bg-lable span {
    color: #fff;
    font-size: 1.6rem;
    padding-top: 15px;
    padding-left: 10px;
    float: left;
    letter-spacing: 1px;
    font-family: hanaMinA;
}

.ex {
    height: 20px;
}


/*------------------------------------------------------------
      Case2
  ------------------------------------------------------------*/

.container-case2 {
    width: 1136px;
    margin: 0 auto;
}

.block {
    height: 130px;
    margin: 60px 20px 0 35px;
    background-image: url(../images/Polygon.png);
    background-repeat: no-repeat;
    background-size: contain;
    padding-bottom: 90px;
}

.block p.text {
    font-size: 1.6rem;
    text-align: center;
    line-height: 1.7;
    margin-top: 30px
}

.block p.text5 {
    font-size: 1.6rem;
    text-align: center;
    line-height: 1.7;
    margin-top: 20px
}

.number {
    text-align: center;
    margin-top: -20px;
}

.block ul {
    display: flex;
}

.block ul li.col-block {
    width: 150px;
    display: flex;
}

.block .yel {
    background-color: #fff000;
    margin: 10px;
    border: #000 solid 1px;
    width: 111px;
    height: 111px;
}

.col-block img {
    padding-top: 45px;
    height: 45px;
}

.col-block img {
    width: 15px;
}

.number img {
    width: 32px;
    height: 32px;
    padding-top: 0 !important;
}


/*------------------------------------------------------------
      Q&A
  ------------------------------------------------------------*/

.q-and-a-SP {
    display: none;
}

.question {
    background: #fff;
    text-align: center
}

.question h2,
.question h3 {
    color: #0068b7;
    text-align: center;
}

.question .title-question {
    border-bottom: #fff000 solid 10px;
    width: 520px;
    margin: auto;
}

.question h2 {
    font-size: 3.6rem;
    font-weight: 600;
    margin-top: 60px;
}

.question h3 {
    font-size: 2.2rem;
    margin: 30px auto;
    line-height: 1.5;
}

.img-question {
    float: right;
    margin-top: -130px;
    margin-right: 70px;
}

.content-question {
    display: grid;
    grid-template-columns: 50% 50%;
}

.qs1 {
    padding: 0 50px 0 70px;
}

.qs2 {
    padding: 100px 70px 0 50px;
}

.question p {
    text-align: center;
    margin-bottom: 20px;
}

.qs1 ul li {
    background-color: #0068b7;
    border: #fff000 10px solid;
    border-radius: 20px;
    padding: 30px;
    margin-bottom: 20px;
}

.question .q2 {
    font-weight: 600;
    font-size: 1.6rem;
    color: #fff000;
    line-height: 2;
}

.question .a2 {
    font-size: 1.6rem;
    color: #fff;
    line-height: 2;
    text-align: left;
}

.q-a-1 img {
    float: left;
    margin-top: -100px;
    margin-left: -120px;
}

.q-a-2 img {
    float: right;
    margin-top: -100px;
    margin-right: -120px;
}

.q-a-5 img {
    float: right;
    margin-top: -60px;
    margin-right: -120px;
}

.q-a-3 img {
    float: left;
    margin-top: -100px;
    margin-left: -120px;
}

.q-a-4 img {
    float: right;
    margin-top: -200px;
    margin-right: -90px;
}

.question .q1 img,
.question .a1 img {
    float: none;
    margin: 10px auto;
}

.qs2 ul li {
    background-color: #0068b7;
    border: #fff000 10px solid;
    border-radius: 20px;
    padding: 30px;
    margin-bottom: 20px;
}

.question .h1 {
    font-size: 4.2rem;
    color: #0068b7;
    font-weight: 500;
    margin: 70px auto 40px auto;
}

.question h4 {
    font-size: 3rem;
    color: #0068b7;
    font-weight: 500;
    line-height: 1.5;
    text-align: center;
    margin: 40px auto 90px auto;
}

.img6 img {
    margin: auto;
}


/*------------------------------------------------------------
      Footer
  ------------------------------------------------------------*/

.f-fixed {
    display: none;
}

footer {
    background-color: #0068b7;
    color: #fff;
    height: auto;
    font-size: 1.2rem;
    /* width: 100%; */
}

.content-footer {
    width: 1136px;
    margin: 50px auto 0 auto;
}

footer .f-img {
    display: flex;
    margin: 0 auto;
}

.f-item {
    margin: 0 20px;
    text-align: center
}

.f-item1 {
    text-align: left;
    margin-left: 0 !important;
}

.item4 {
    text-align: right;
}

.f-tell,
.f-mail {
    padding-top: 50px;
    margin-right: 0 !important;
}

.f-logo {
    width: 135px;
    margin-top: 48px;
    position: relative;
    z-index: 99;
}

img.f-person {
    margin-top: -137px;
    margin-left: -45px;
}

footer hr {
    position: absolute;
    z-index: 10;
    width: 100%;
    margin-top: 134px;
    color: #fff;
}

.f-text {
    display: flex;
    flex-direction: row;
}

.f-text .f-t-item {
    width: 500px;
}

.f-text .f-t-item2 {
    flex-grow: 1;
}

footer .f-t-item1 p {
    line-height: 1.5;
    margin: 30px auto
}

footer .f-t-item2 p {
    text-align: right;
    margin-top: 50px
}