/**
 * Slider Fulll Width
 */
var images = new Array('assets/img/slide1.jpg', 'assets/img/slide2.jpg', 'assets/img/slide3.jpg', 'assets/img/slide4.jpg');
var nextimage = 0;

doSlideshow();

function doSlideshow() {

    if (nextimage >= images.length)
        nextimage = 0;
    $('.c-mv__img').css('background-image', 'url("' + images[nextimage++] + '")').fadeIn(3000, function() {
        setTimeout(doSlideshow, 5000);
    });
}

/**
 * Slider Pickup
 */
$(window).resize(function() {
    $('.carousel').slick('resize');
});
$(document).ready(function() {
    $('.carousel').slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        dots: true,
        centerMode: true,
        autoplay: true,
        autoplaySpeed: 7000,
        responsive: [{
                breakpoint: 1921,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 3,
                    centerMode: true,
                }
            }, {
                breakpoint: 1441,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 3
                }
            }, {
                breakpoint: 1025,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 641,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 537,
                settings: {
                    slidesToShow: 1.5,
                    slidesToScroll: 3
                }
            }, {
                breakpoint: 376,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 3
                }
            }
        ]
    });
    $(window).on('resize', function() {
        $('.carousel').slick('resize');
    });
});

/**
 * Form Validate
 */
$(document).ready(function() {

    let checkInPutNull = function(id, error, mess) {
        $("#" + error).html("");
        $("#check-error").html("");
        if ($("#" + id).val().trim().length == 0) {
            $("#" + error).html(mess);
            $("#check-error").html("入力内容を確認してください。");
        }
    }

    let checkEmail = function(id, error, mess) {
        $("#check-error").html("");
        if ($("#" + error).html().length == 0) {
            if (!isEmail($("#" + id).val().trim())) {
                $("#" + error).html(mess);
                $("#check-error").html("入力内容を確認してください。");
            }
        }
    }

    let checkPhone = function(id, error, mess) {
        if ($("#" + error).html().length == 0) {
            if (!isPhoneNumber($("#" + id).val().trim())) {
                $("#" + error).html(mess);
                $("#check-error").html("入力内容を確認してください。");
            }
        }
    }

    $("#btn-contact").click(function() {
        checkInPutNull("name", "error-name", "『氏名』を入力してください。");
        checkInPutNull("email", "error-email", "『メールアドレス』を入力してください。");
        checkInPutNull("phone", "error-phone", "『電話番号』を入力してください。");
        checkInPutNull("inquiry", "error-inquiry", "『お問い合わせ内容』を入力してください。");
        checkEmail("email", "error-email", "『メールアドレス』を入力してください。")
        checkPhone("phone", "error-phone", "『電話番号』を入力してください。")
    });

    function isEmail(email) {
        let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }

    function isPhoneNumber(phone) {
        return $.isNumeric(phone);
    }

});
/**
 * Nav Mobile
 */
$(document).ready(function() {
    $(".c-openmenu").click(function() {
        $(".c-gnavi").fadeIn();
    });
    $(".closemenu").click(function() {
        $(".c-gnavi").fadeOut();
    });
});
/**
 * Move top
 */
if ($('#back-to-top').length) {
    var scrollTrigger = 100, // px
        backToTop = function() {
            var scrollTop = $(window).scrollTop();
            if (scrollTop > scrollTrigger) {
                $('#back-to-top').addClass('show');
            } else {
                $('#back-to-top').removeClass('show');
            }
        };
    backToTop();
    $(window).on('scroll', function() {
        backToTop();
    });
    $('#back-to-top').on('click', function(e) {
        e.preventDefault();
        $('html,body').animate({
            scrollTop: 0
        }, 700);
    });
}