<?php get_header(); ?>

<!--▼ Content area ▼-->
<div class="archive-title">
    <h2>
        <?php
            if ( is_tag() ) :
                    printf( __('Posts Tagged: %1$s','camtu'), single_tag_title( '', false ) );
            elseif ( is_category() ) :
                    printf( __('Posts Categorized: %1$s','camtu'), single_cat_title( '', false ) );
            elseif ( is_day() ) :
                    printf( __('Daily Archives: %1$s','camtu'), get_the_time('l, F j, Y') );
            elseif ( is_month() ) :
                    printf( __('Monthly Archives: %1$s','camtu'), get_the_time('F Y') );
            elseif ( is_year() ) :
                    printf( __('Yearly Archives: %1$s','camtu'), get_the_time('Y') );
            endif;
        ?>
    </h2>
</div>
<?php if ( is_tag() || is_category() ) : ?>
<div class="archive-description">
    <?php echo term_description(); ?>
</div>
<?php endif; ?>
<main class="p-index" id="main-content">
<div class="l-container">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <!-- Lấy tiêu đề bài viết -->
    <?php the_title(); ?>

    <!-- Lấy ảnh đại diện của Post -->
    <?php 
    if(has_post_thumbnail()): ?>
    <img src="<?php the_post_thumbnail_url();?>" />
    <!-- Hoặc the_post_thumbnail(); -->
    <?php endif;?>
    <!-- Lấy nội dung bài viết -->
    <?php the_content();?>
    <?php endwhile; ?>
    <?php hakkayu_pagination(); ?>
    <?php else : ?>
    <div>
        <?php _e('Mothing post founnd.','camtu'); ?>
    </div>
    <?php endif; ?>
    </div>
</main>
<!--▲ main ▲-->

<?php get_footer(); ?>