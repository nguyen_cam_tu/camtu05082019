<!DOCTYPE html>
<!--[if IE 8]> <html <?php language_attributes(); ?> class="ie8"> <![endif]-->
<!--[if !IE]> <html <?php language_attributes(); ?>> <![endif]-->

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="profile" href="http://gmgp.org/xfn/11" />

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css">
    <link href="https://fonts.googleapis.com/css?family=Sawarabi+Mincho&display=swap&subset=japanese,latin-ext"
        rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <?php wp_head(); ?>
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
</head>

<body <?php body_class(); ?>>
    <!--Thêm class tượng trưng cho mỗi trang lên <body> để tùy biến-->
    <!--▼ Header ▼-->
    <header class="c-header" id="home">
        <div class="sp-only btn-nav" id="toggle">
            <span class="top"></span>
            <span class="middle"></span>
            <span class="bottom"></span>
        </div>
        <div class="c-header__nav" id="overlay">
            <div class="sp-only c-header__nav__leaf">
                <div><img class="c-header__nav__leaf1"
                        src="<?php echo get_template_directory_uri(); ?>/images/leaf-top-sp.png" width="160" alt="">
                </div>
                <div><img class="c-header__nav__leaf2"
                        src="<?php echo get_template_directory_uri(); ?>/images/leaf-right-sp.png" width="179" alt="">
                </div>
            </div>
            <?php hakkayu_menu( 'primary-menu' ); ?>
        </div>
        <div class="fix-leaf">
            <img id="fixed-leaf-top" src="<?php echo get_template_directory_uri(); ?>/images/leaf-top.png" alt="">
            <img id="fixed-leaf-right" src="<?php echo get_template_directory_uri(); ?>/images/leaf-right.png" alt="">
        </div>
    </header>

    <!--▲ Header ▲-->