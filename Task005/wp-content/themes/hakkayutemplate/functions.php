<?php
/**
  * Thiết lập các hằng dữ liệu quan trọng
  * THEME_URL = get_stylesheet_directory() - đường dẫn tới thư mục theme
  **/
  define( 'THEME_URL', get_stylesheet_directory() );
  /**
 *@ Thiết lập $content_width để khai báo kích thước chiều rộng của nội dung
 **/
 if ( ! isset( $content_width ) ) {
    /*
     * Nếu biến $content_width chưa có dữ liệu thì gán giá trị cho nó
     */
    $content_width = 1030;
}
/**
  *@ Thiết lập các chức năng sẽ được theme hỗ trợ
  **/
  if ( ! function_exists( 'hakkayu_theme_setup' ) ) {
    /*
     * Nếu chưa có hàm hakkayu_theme_setup() thì sẽ tạo mới hàm đó
     */
    function hakkayu_theme_setup() {
            /*
             * Thiết lập theme có thể dịch được
             */
            $language_folder = THEME_URL . '/languages';
            load_theme_textdomain( 'camtu', $language_folder );

            /*
             * Tự chèn RSS Feed links trong <head>
             */
            add_theme_support( 'automatic-feed-links' );

            /*
             * Thêm chức năng post thumbnail
             */
            add_theme_support( 'post-thumbnails' );

            /*
             * Thêm chức năng title-tag để tự thêm <title>
             */
            add_theme_support( 'title-tag' );

            /*
             * Thêm chức năng post format
             */
            add_theme_support( 'post-formats',
                    array(
                            'video',
                            'image',
                            'audio',
                            'gallery'
                    )
             );

            /*
             * Tạo menu cho theme
             */
             register_nav_menu ( 'primary-menu', __('Primary Menu', 'camtu') );

            /*
             * Tạo sidebar cho theme
             */
            //  $sidebar = array(
            //     'name' => __('Main Sidebar', 'camtu'),
            //     'id' => 'main-sidebar',
            //     'description' => 'Main sidebar for hakkayu theme',
            //     'class' => 'main-sidebar',
            //     'before_title' => '<h3 class="widgettitle">',
            //     'after_sidebar' => '</h3>'
            //  );
            //  register_sidebar( $sidebar );
    }
    add_action ( 'init', 'hakkayu_theme_setup' );

}

/**
*@ Thiết lập hàm hiển thị menu
*@ hakkayu_menu( $slug )
**/
if ( ! function_exists( 'hakkayu_menu' ) ) {
    function hakkayu_menu( $slug ) {
      $menu = array(
        'theme_location' => $slug,
        'container' => 'nav',
        'container_class' => $slug,
      );
      wp_nav_menu( $menu );
    }
  }
  /**
*@ Tạo hàm phân trang cho index, archive.
*@ Hàm này sẽ hiển thị liên kết phân trang theo dạng chữ: Newer Posts & Older Posts
*@ hakkayu_pagination()
**/
if ( ! function_exists( 'hakkayu_pagination' ) ) {
    function hakkayu_pagination() {
      /*
       * Không hiển thị phân trang nếu trang đó có ít hơn 2 trang
       */
      if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
        return '';
      }
    ?>
   
    <nav class="pagination" role="navigation">
      <?php if ( get_next_post_link() ) : ?>
        <div class="prev"><?php next_posts_link( __('Older Posts', 'camtu') ); ?></div>
      <?php endif; ?>
   
      <?php if ( get_previous_post_link() ) : ?>
        <div class="next"><?php previous_posts_link( __('Newer Posts', 'camtu') ); ?></div>
      <?php endif; ?>
   
    </nav><?php
    }
  }

  /**
*@ Chèn CSS và Javascript vào theme
*@ sử dụng hook wp_enqueue_scripts() để hiển thị nó ra ngoài front-end
**/
function hakkayu_styles() {
    /*
     * Hàm get_stylesheet_uri() sẽ trả về giá trị dẫn đến file style.css của theme
     * Nếu sử dụng child theme, thì file style.css này vẫn load ra từ theme mẹ
     */
    wp_register_style( 'main-style', get_template_directory_uri() . '/style.css', 'all' );
    wp_enqueue_style( 'main-style' );

  }
  add_action( 'wp_enqueue_scripts', 'hakkayu_styles' );

  function wpb_adding_scripts() {
    wp_register_script('main-script', get_template_directory_uri() . '/js/main.js', array('jquery'),'1.1', true);
    wp_enqueue_script('main-script');
    }
    
    add_action( 'wp_enqueue_scripts', 'wpb_adding_scripts' ); 
  
