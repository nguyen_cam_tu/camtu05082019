<!--▲ main ▲-->

    <!--▼ Footer ▼-->
<footer class="c-footer">
        <div class="l-container">
            <p class="item item1">商品に関するお問い合わせは<br> 健栄製薬株式会社
                <br> TEL |｜06-6231-5626　受付時間 |｜8:45〜17:30(土日祝日を除く)</p>
            <div class="item item2">
                <h1>
                    <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/logo_kenei.png" alt="logo"></a>
                </h1>
                <p>©KENEI Pharmaceutical</p>
            </div>
        </div>
    </footer>
    <!--▲ Footer ▲-->
    <?php wp_footer(); ?>
</body>

</html>