<?php get_header(); ?>

<!--▼ Content area ▼-->
<main class="p-index" id="main-content">
<div class="l-container">
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <!-- Lấy tiêu đề bài viết -->
    <?php the_title(); ?>

    <!-- Lấy ảnh đại diện của Post -->
    <?php 
    if(has_post_thumbnail()): ?>
    <img src="<?php the_post_thumbnail_url();?>" />
    <!-- Hoặc the_post_thumbnail(); -->
    <?php endif;?>
    <!-- Lấy nội dung bài viết -->
    <?php the_content();?>
    <?php endwhile; ?>
    <?php hakkayu_pagination(); ?>
    <?php else : ?>
    <div>
        <?php _e('Mothing post founnd.','camtu'); ?>
    </div>
    <?php endif; ?>
    </div>
</main>
<!--▲ main ▲-->

<?php get_footer(); ?>