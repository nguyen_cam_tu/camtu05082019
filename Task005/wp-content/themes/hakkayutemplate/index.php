<?php get_header(); ?>
<div class="l-main">
    <div class="l-main__main">

        <div class="l-container">
            <div class="l-main__text">
                <h2>
                    <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/c-header__text1.png"
                        alt="健栄製薬の ハッカ油_">
                    <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/images/c-header__text1_sp.png"
                        width="145" alt="健栄製薬の ハッカ油_">
                </h2>
                <hr>
                <h3>MENTHA OIL
                </h3>
                <p>ハッカ油は、毎日の暮らしを豊かにする</p>
                <h4>魔法のしずく</h4>

                <div class="c-btn1">
                    <a href=" ">ハッカ？ミント？</a>
                </div>
            </div>
        </div>
        <div class="l-main__main__bg">
            <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/bg_main.png" alt="">
            <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/images/bg_main_sp.png" alt="">
        </div>

    </div>
    <div class="l-main__bubble" id="bubble">
        <img src="<?php echo get_template_directory_uri(); ?>/images/bubble.png" alt="">

    </div>
    <div class="l-main__case" id="case">
        <p>使い方いろいろ</p>
        <h2>毎日にハッカ油
        </h2>
        <div class="l-main__case__img">
            <div class="l-main__case__img__item">
                <div class="l-main__case__img__item1">
                    <a href="">
                        <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_1.png"
                            alt="ハッカ水">
                        <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_1_sp.png"
                            width="145" alt="ハッカ水">
                    </a>
                </div>
                <div class="l-main__case__img__item2">
                    <a href="">
                        <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_2.png"
                            alt="ハッカの虫除けスプレー">
                        <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_2_sp.png"
                            width="209" alt="ハッカの虫除けスプレー">
                    </a>
                </div>
                <div class="l-main__case__img__item3">
                    <a href="">
                        <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_3.png"
                            alt="ハッカのリネンウォーター">
                        <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_3_sp.png"
                            width="220" alt="ハッカのリネンウォーター">
                    </a>
                </div>
                <div class="l-main__case__img__item4">
                    <a href="">
                        <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_4.png"
                            alt="ハッカのエアフレッシュナー">
                        <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_4_sp.png"
                            width="277" alt="ハッカのエアフレッシュナー">
                    </a>
                </div>
            </div>
            <div class="l-main__case__img__item">
                <div class="l-main__case__img__item5">
                    <a href="">
                        <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_5.png"
                            alt="ハッカの清涼スプレー">
                        <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_5_sp.png"
                            width="209" alt="ハッカの清涼スプレー">
                    </a>
                </div>
                <div class="l-main__case__img__item6">
                    <a href="">
                        <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_6.png"
                            alt="ハッカマスク">
                        <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_6_sp.png"
                            width="173" alt="ハッカマスク">
                    </a>
                </div>
                <div class="l-main__case__img__item7">
                    <a href="">
                        <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_7.png"
                            alt="ハッカで安息">
                        <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_7_sp.png"
                            width="199" alt="ハッカで安息">
                    </a>
                </div>
                <div class="l-main__case__img__item8">
                    <a href="">
                        <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_8.png"
                            alt="ハッカの入浴剤">
                        <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/images/img_item_8_sp.png"
                            width="195" alt="ハッカの入浴剤">
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!--▼ Content area ▼-->
<main class="p-index" id="main-content">
    <!--▼ Point ▼-->
    <section class="p-index__point" id="point">
        <div class="l-container">
            <div class="p-index__point__item">
               
                <div class="p-index__point__item1">
                    <div class="item-point">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/point1.png" alt="point 1">
                    </div>
                    <div class="item-text">
                        <h2>気持ちリフレッシュ</h2>
                        <p>鼻や口から入った香りの信号は、視床下部などに直接作用し<br class="pc-only"> 気分をリフレッシュさせてくれます。
                        </p>
                    </div>
                </div>
                <div class="p-index__point__item2">
                    <?php
                        $catpoint = new WP_Query(array(
                        'post_type'=>'post',
                        'post_status'=>'publish',
                        'cat' => 4,
                        'orderby' => 'ID',
                        'order' => 'ASC',
                        'posts_per_page'=> 6));
                        ?>
                    <?php $i=1; while ($catpoint->have_posts()) : $catpoint->the_post(); ?>

                    <div class="box-item">
                        <a href="<?php the_permalink() ;?>">
                            <?php the_post_thumbnail("thumbnail",array( "title" => get_the_title(),"alt" => get_the_title() ));?>
                        </a>
                        <div class="box">
                            <h3><?php the_title() ;?></h3>
                            <hr>
                            <p><?php the_excerpt() ;?></p>

                            <a class="c-btn-case" href="<?php the_permalink() ;?>">ハッカ油コラムへ</a>
                        </div>
                        <a class="btn_more" href="<?php the_permalink() ;?>">
                            <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/btn_more.png"
                                alt="">
                            <img class="sp-only"
                                src="<?php echo get_template_directory_uri(); ?>/images/btn_more_sp.png" alt="">
                        </a>
                    </div>
                    <?php $i++; endwhile ; wp_reset_query() ;?>
                </div>
            </div>
            <div class="p-index__point__item">
                <div class="p-index__point__item1">
                    <div class="item-point">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/point2.png" alt="point 2">
                    </div>
                    <div class="item-text">
                        <h2>気持ちリフレッシュ</h2>
                        <p>メﾝトールの刺激ですっきりクールダウン。<br class="pc-only"> ミントの香りで気分も爽快！
                        </p>
                    </div>
                </div>
                <div class="p-index__point__item2">
                    <?php
                        $catpoint = new WP_Query(array(
                        'post_type'=>'post',
                        'post_status'=>'publish',
                        'cat' => 5,
                        'orderby' => 'ID',
                        'order' => 'ASC',
                        'posts_per_page'=> 6));
                        ?>
                    <?php $i=1; while ($catpoint->have_posts()) : $catpoint->the_post(); ?>

                    <div class="box-item">
                        <a href="<?php the_permalink() ;?>">
                            <?php the_post_thumbnail("thumbnail",array( "title" => get_the_title(),"alt" => get_the_title() ));?>
                        </a>
                        <div class="box">
                            <h3><?php the_title() ;?></h3>
                            <hr>
                            <p><?php the_excerpt() ;?></p>

                            <a class="c-btn-case" href="<?php the_permalink() ;?>">ハッカ油コラムへ</a>
                        </div>
                        <a class="btn_more" href="<?php the_permalink() ;?>">
                            <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/btn_more.png"
                                alt="">
                            <img class="sp-only"
                                src="<?php echo get_template_directory_uri(); ?>/images/btn_more_sp.png" alt="">
                        </a>
                    </div>
                    <?php $i++; endwhile ; wp_reset_query() ;?>
                </div>
            </div>
            <div class="p-index__point__item">
                <div class="p-index__point__item1">
                    <div class="item-point">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/point3.png" alt="point 3">
                    </div>
                    <div class="item-text">
                        <h2><?php echo get_cat_name(6);?></h2>
                        <p>ハッカ油に含まれるメントールは、お部屋のスッキリを演出。<br class="pc-only">お家の害虫対策にもおすすめです。
                        </p>
                    </div>
                </div>
                <div class="p-index__point__item2">
                    <?php
                        $catpoint = new WP_Query(array(
                        'post_type'=>'post',
                        'post_status'=>'publish',
                        'cat' => 6,
                        'orderby' => 'ID',
                        'order' => 'ASC',
                        'posts_per_page'=> 6));
                        ?>
                    <?php $i=1; while ($catpoint->have_posts()) : $catpoint->the_post(); ?>

                    <div class="box-item">
                        <a href="<?php the_permalink() ;?>">
                            <?php the_post_thumbnail("thumbnail",array( "title" => get_the_title(),"alt" => get_the_title() ));?>
                        </a>
                        <div class="box">
                            <h3><?php the_title() ;?></h3>
                            <hr>
                            <p><?php the_excerpt() ;?></p>

                            <a class="c-btn-case" href="<?php the_permalink() ;?>">ハッカ油コラムへ</a>
                        </div>
                        <a class="btn_more" href="<?php the_permalink() ;?>">
                            <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/btn_more.png"
                                alt="">
                            <img class="sp-only"
                                src="<?php echo get_template_directory_uri(); ?>/images/btn_more_sp.png" alt="">
                        </a>
                    </div>
                    <?php $i++; endwhile ; wp_reset_query() ;?>
                </div>
            </div>
        </div>
    </section>
    <!--▲ Point ▲-->

    <!--▼ Contact ▼-->
    <section class="p-index__contact">
        <h2>お買い求めは全国のドラッグストア、またはこちらから</h2>
        <div class="p-index__contact__box">
            <div class="p-index__contact__box1">
                <div class="p-index__contact__box__item">
                    <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/cont_box1.png"
                            alt="amazon"></a>
                </div>
                <div class="p-index__contact__box__item">
                    <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/cont_box2.png"
                            alt="kenko.com"></a>
                </div>
            </div>

            <div class="p-index__contact__box1">
                <div class="p-index__contact__box__item">
                    <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/cont_box3.png"
                            alt="lohaco"></a>
                </div>
                <div class="p-index__contact__box__item no-margin">
                    <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/cont_box4.png" alt=""></a>
                </div>
            </div>

        </div>
        <h4>Amazon、Amazon.co.jp、及びAmazon.co.jpロゴはAmazon.com, Inc.又はその関連会社の商</h4>
    </section>
    <!--▲ Contact ▲-->

    <!--▼ Q&A ▼-->
    <section id="QA" class="p-index__QA">
        <div class="l-container">
            <h2>Q&A</h2>
            <ul>
                <li>
                    <div class="title-icon">
                        <div class="icon-Q"><img src="<?php echo get_template_directory_uri(); ?>/images/icon_Q.png"
                                alt="Q"></div>
                        <h3>虫除け対策に直接体や環境に噴きかけても良いですか？</h3>
                    </div>
                    <p>刺激が強過ぎる可能性がありますので、薄めて使用してください。また素材によっては変質(変色)するおそれがありますので注意してください。
                    </p>
                </li>
                <li>
                    <div class="title-icon">
                        <div class="icon-Q"><img src="<?php echo get_template_directory_uri(); ?>/images/icon_Q.png"
                                alt="Q"></div>
                        <h3>体に使用する際の注意点は？</h3>
                    </div>
                    <p>刺激作用があるため、目や目の周囲、粘膜等に付着しないように注意してください。
                    </p>
                </li>
                <li>
                    <div class="title-icon">
                        <div class="icon-Q"><img src="<?php echo get_template_directory_uri(); ?>/images/icon_Q.png"
                                alt="Q"></div>
                        <h3>ハッカ油アイテムを作る際に使用する量は紹介されている量を守らないといけませんか？</h3>
                    </div>
                    <p>紹介している量はあくまでも目安になります。ただし、これ以上の量を加えると刺激が強過ぎる可能性があるので注意してください。
                    </p>
                </li>
                <li>
                    <div class="title-icon">
                        <div class="icon-Q"><img src="<?php echo get_template_directory_uri(); ?>/images/icon_Q.png"
                                alt="Q"></div>
                        <h3>使用期限はどのくらいですか？</h3>
                    </div>
                    <p>パッケージ裏面に賞味期限を記載しています。遮光して、涼しい所に保管してください。
                    </p>
                </li>
                <li>
                    <div class="title-icon">
                        <div class="icon-Q"><img src="<?php echo get_template_directory_uri(); ?>/images/icon_Q.png"
                                alt="Q"></div>
                        <h3>ハッカ油は何からできたものですか？</h3>
                    </div>
                    <p>ハッカ油とはハッカソウというミントを乾燥させて抽出した植物油のことです。
                    </p>
                </li>
            </ul>
        </div>
    </section>
    <!--▲ Q&A ▲-->
    <!--▼ Column ▼-->
    <section class="p-index__column" id="column">
        <div class="l-container">
            <div class="p-index__column__banner">
                <div class="p-index__column__banner__bg">
                    <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/banner_1.png"
                        alt="banner">
                    <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/images/banner_1_sp.png"
                        alt="banner">
                </div>
                <div class="p-index__column__banner__text">
                    <p>日々の暮らしにお役立ち </p>
                    <h3>「ハッカ油コラム」へ</h3>
                </div>
            </div>
            <ul>
                <?php
                        $catpoint = new WP_Query(array(
                        'post_type'=>'post',
                        'post_status'=>'publish',
                        'cat' => 1,
                        'order' => 'DESC',
                        'posts_per_page'=> 15));
                        ?>
                <?php  $postsInCat = get_term_by('id',1,'category');
                        $postsInCat = $postsInCat->count; ?>
                <?php $i=$postsInCat ; while ($catpoint->have_posts()) : $catpoint->the_post(); ?>
                <li>
                    <div class="column-img">
                        <div class="icon-column">
                            <div class="pc-only">
                                <?php the_post_thumbnail("thumbnail",array( "title" => get_the_title(),"alt" => get_the_title() ));?>
                            </div>
                            <img class="sp-only icon-column__texthk"
                                src="<?php echo get_template_directory_uri(); ?>/images/text_hk.png" alt="hakkayu">
                            <p class="sp-only">
                                <?php  
                                    echo $var=$i<10 ? '0'.$i : $i;
                                ?>
                            </p>
                        </div>
                        <div class="title_col">
                            <h4><?php the_title() ;?></h4>
                            <h3><?php the_excerpt() ;?></h3>
                        </div>
                    </div>
                    <div class="column-text" id="toggle_col<?php echo $i ?>">
                        <div><?php the_content(); ?></div>
                        <div class="icon-right">
                            <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/icon-right.png"
                                alt="">
                            <img class="sp-only"
                                src="<?php echo get_template_directory_uri(); ?>/images/text-toggle.png" alt="詳しく見る">
                        </div>
                    </div>
                    <a class="sp-only " onclick="myFunction('toggle_col<?php echo $i ?>','img_toggle<?php echo $i ?>')">
                        <img id="img_toggle<?php echo $i ?>"
                            src="<?php echo get_template_directory_uri(); ?>/images/icon-white.png" width="35" alt="">
                    </a>
                </li>
                <?php $i--; endwhile ; wp_reset_query() ;?>
            </ul>


            <div class="p-index__column__banner">
                <div class="p-index__column__banner__bg">
                    <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/banner_2.png"
                        alt="banner">
                    <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/images/banner_2_sp.png"
                        alt="banner">
                </div>
                <div class="p-index__column__banner__text text-center">
                    <img class="pc-only" src="<?php echo get_template_directory_uri(); ?>/images/banner2__text.png"
                        alt="banner LIVING INFORMATION さわやかハッカ油で虫除け対策のイメージ写真">
                    <img class="sp-only" src="<?php echo get_template_directory_uri(); ?>/images/banner2__text_sp.png"
                        width="205" alt="banner LIVING INFORMATION さわやかハッカ油で虫除け対策のイメージ写真">
                </div>
            </div>
        </div>
    </section>
    <!--▲ Column ▲-->

</main>

<!--▲ main ▲-->

<?php get_footer(); ?>