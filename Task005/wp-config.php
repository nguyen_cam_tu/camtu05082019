<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'hakkayu' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', '123456' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'RRSNv8,+oR`VR|ZGnCT|56C0HC#rZEUB4%<Vc:J^v~]<jcyN0.+=>HawQ6PIbyKn' );
define( 'SECURE_AUTH_KEY',  'E*WyN[A!r<]0BP%mBxc}!h!wki!1b {6IulD{CEo/(3DbA w99LKZmyCJJ5V{Mwr' );
define( 'LOGGED_IN_KEY',    '/zX+$-/8@qQ?h0yo%sk]Gl&vLl^_$9PX&(j|P{&*^hPMqS0dmouFOatF*ZVwd=2d' );
define( 'NONCE_KEY',        'z(53bPo)#Vf8QOks#rQN8AFF@v&QIL`[<vx!.XN!x/wT/0gp[jZxe[ WY;a9$~^m' );
define( 'AUTH_SALT',        '#wMO<.0uR0r|cgd$(lI~Ee1/6g-w3UlHSEqyq95(X|jK]{p[(4k0z-B9T+w5GXE+' );
define( 'SECURE_AUTH_SALT', 'e.$pG}:t+.iVbb)wQ.4xX16:9 xPC(Xem~)kd4iKV?l`2^`mi5LFqapfa)Yfj{F5' );
define( 'LOGGED_IN_SALT',   'Z#w={azsc7eS~ m9eg9#&@|o.gq/Hr_+h$!LqA|Thwd}p)+zBhYu,3 50J/oBCBe' );
define( 'NONCE_SALT',       '9B]!MW{S71T1 4B7.V(xG*D1J,VcENhs!ArD{wb-,&a>mKZ3d,Hm-R_.GJ^!vx)E' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
