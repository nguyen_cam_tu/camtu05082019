//-----------Scroll Animation--------------//
window.onscroll = function() { scrollFunction() };
var leaf_top = document.getElementById("fixed-leaf-top");
var leaf_right = document.getElementById("fixed-leaf-right");
var bubble = document.getElementById("bubble");
$(window).resize(function() {
    scrollFunction();
});

function scrollFunction() {
    if (screen.availWidth > 767) {
        if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
            leaf_top.style.width = "232px";
            leaf_right.style.width = "155px";
            bubble.style.animationName = "bubble_float";
            bubble.style.animationDuration = "3s";
        } else {
            leaf_top.style.width = "302px";
            leaf_right.style.width = "235px";
            bubble.style.animationName = "bubble";
            bubble.style.animationDuration = "1s";
        }
    } else {
        if (document.body.scrollTop > 200 || document.documentElement.scrollTop > 200) {
            leaf_top.style.width = "121px";
            leaf_right.style.width = "88px";
        } else {
            leaf_top.style.width = "151px";
            leaf_right.style.width = "118px";
        }
    }
}

//-----------End Scroll Animation--------------//
//-----------Nav Mobile--------------//

$('#toggle').click(function() {
    $(this).toggleClass('active');
    $('#overlay').toggleClass('open');


});

//-----------End Nav Mobile--------------//
function myFunction(toggle_col_id, img_toggle_id) {
    var y = document.getElementById(toggle_col_id);

    if (y.style.display === "none") {
        y.style.display = "block";
        document.getElementById(img_toggle_id).style.WebkitTransform = "rotate(0deg)";
        // Code for IE9
        document.getElementById(img_toggle_id).style.msTransform = "rotate(0deg)";
        // Standard syntax
        document.getElementById(img_toggle_id).style.transform = "rotate(0deg)";

    } else {
        y.style.display = "none";
        document.getElementById(img_toggle_id).style.WebkitTransform = "rotate(180deg)";
        // Code for IE9
        document.getElementById(img_toggle_id).style.msTransform = "rotate(180deg)";
        // Standard syntax
        document.getElementById(img_toggle_id).style.transform = "rotate(180deg)";
    }
}