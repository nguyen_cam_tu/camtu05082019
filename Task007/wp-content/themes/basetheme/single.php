<?php get_header(); ?>

<main class="p-single">
    <div class="c-title c-title--page">
        <h1>TOPICS</h1>
    </div>
    <div class="l-container">
        <ul class="l-sidebar">
            <?php get_sidebar(); ?>
        </ul>

        <div class="l-main">
            <?php if(have_posts()):?>
            <?php while(have_posts()): the_post(); ?>
            <h2 class="single_title"><?php the_title() ?></h2>
            <div class="single_info">
                <span><?php the_time('Y/m/d')?></span>
                <?php echo the_category($post->ID) ; ?>
            </div>

            <div class="single_content">
                <?php the_content() ?>
            </div>
            <?php endwhile ?>
            <?php endif ?>
            <div class="groupbtn">
                <ul>
                    <li class="prev_link"><a href="<?php previous_posts_link( 'Prev' ); ?>">Prev</a></li>
                    <li class="next_link"><a href="<?php next_posts_link('Next') ?>">Next</a></li>
                </ul>
            </div>
        </div>
    </div>
    </div>
    </div>
</main>

<?php get_footer();?>