<?php get_header(); ?>

<main class="p-topics">
    <div class="c-title c-title--page">
        <h1>TOPICS</h1>
    </div>
    <div class="l-container">
        <ul class="p-toppics__list">
            <?php
                $custom_query_args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
                $custom_query = new WP_Query( $custom_query_args );
                $wp_query = $custom_query;
                if ( $custom_query->have_posts() ) :
                while ( $custom_query->have_posts() ) :
                $custom_query->the_post();?>
                <li>
                    <span class="datepost"><?php echo get_the_date('Y/m/d'); ?></span>
                    <?php echo the_category($post->ID) ; ?>
                    <a class="title" href="<?php the_permalink();?>"><?php the_title();?></a>
                </li>
               <?php endwhile;?>
                        </ul>
                        <?php
                endif;
                wp_reset_postdata();
                wp_corenavi_table();
                 ?>
    </div>
</main>

<?php get_footer();?>