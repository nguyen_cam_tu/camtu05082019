<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="utf-8">
    <title>
        <?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?>
    </title>
    <meta name="description" content="<?= bloginfo('description'); ?>">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url');?>">

    <!-- css -->
    <link rel="stylesheet" type="text/css" defer href="<?php echo get_stylesheet_directory_uri()?>/css/reset.css">
    <link rel="stylesheet" type="text/css" defer href="<?php echo get_stylesheet_directory_uri()?>/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
    <!-- css -->

    <!-- js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  	<script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
    <!-- js -->

    <!-- ▼ WP HEAD -->
    <?php wp_head(); ?>
    <!-- ▲ WP HEAD -->
</head>

<body>
    <header class="c-header">
        <div class="l-container">
            <div class="c-header__top">
                <div class="logo">
                    <a href="<?php echo site_url(); ?>"><img defer src="<?php echo get_stylesheet_directory_uri()?>/images/logo.png"
                            alt="税理士法人下平会計事務所"></a>
                </div>
                <div class="contact">
                    <img defer src="<?php echo get_stylesheet_directory_uri()?>/images/hed_tel.png" alt=""><br />
                    <img defer src="<?php echo get_stylesheet_directory_uri()?>/images/hed_con_no.png" alt=""
                        class="imglink">

                </div>
            </div>

            <?php wp_nav_menu( array(
				'theme_location' => 'primary-menu', 
				'container' => 'nav', 
				'container_class' => 'c-nav', 
				'menu_class' => 'menu clearfix' 
			) ); ?>
        </div>
    </header>