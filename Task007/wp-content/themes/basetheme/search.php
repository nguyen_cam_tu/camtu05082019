<?php get_header(); ?>
 
<main>
    <div class="c-title c-title--page">
    <h1>SEARCH RESULTS</h1>
    </div>
    <div class="l-container">
                <?php
                        $search_query = new WP_Query( 's='.$s );
                        
                ?>
        </div>
        <ul class="p-toppics__list">
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <li>
                <span class="datepost"><?php echo get_the_date('Y/m/d'); ?></span>
                <?php echo the_category($post->ID) ; ?>
                <a class="title" href="<?php the_permalink();?>"><?php the_title();?></a>
            </li>
                <?php endwhile; ?>
                <?php wp_corenavi_table(); ?>
                <?php else : ?>
                  <p>Sorry, Not found !</p>      
                <?php endif; ?>
        </ul>
</div>
</main>
<?php get_footer(); ?>