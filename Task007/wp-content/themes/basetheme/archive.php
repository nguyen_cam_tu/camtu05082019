<?php get_header(); ?>
<?php
/* 
Template Name: Topics
*/
?>
<!-- ▼content ================================================== -->
<main>
	<div class="l-container">
	<?php if (have_posts()): ?>
        <ul class="p-toppics__list">
            <?php while(have_posts() ) : the_post();?>
            <li>
                <span class="datepost"><?php echo get_the_date('Y/m/d'); ?></span>
                <?php echo the_category($post->ID) ; ?>
                <a class="title" href="<?php the_permalink();?>"><?php the_title();?></a>
            </li>
            <?php endwhile;?>
            <?php wp_corenavi_table(); ?>
        </ul>
        <?php endif;?>

	</div>
</main>
<!-- ▲content ================================================== -->

<?php get_footer();?>