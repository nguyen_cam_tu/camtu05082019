<?php get_header(); ?>
<!-- ▼Slider ================================================== -->
<div class="mainvisual">
    <div class="l-container">
        <?php if( have_rows('bxslider','option') ): ?>
        <div class="l-slider">
            <?php while( have_rows('bxslider','option') ): the_row(); 
                // vars
                $image = get_sub_field('image_slide');
                $link = get_sub_field('link_image_slide');
                ?>
            <?php if( $link ): ?>
            <a href="<?php echo $link; ?>">
                <?php endif; ?>
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                <?php if( $link ): ?>
            </a>
            <?php endif; ?>
            <?php endwhile; ?>
        </div>
        <?php endif; ?>
    </div>
</div>
<!-- ▲Slider ================================================== -->

<!-- ▼content ================================================== -->
<main>
    <div class="l-container">
        <!-- ▼ Group Link ================================================== -->
        <div class="c-grouplink c-flex">
            <a href="#"><img defer src="<?php echo get_stylesheet_directory_uri()?>/images/img_01_no.png" alt=""
                    class="imglink"></a>
            <a href="#"><img defer src="<?php echo get_stylesheet_directory_uri()?>/images/img_02_no.png" alt=""
                    class="imglink"></a>
            <a href="#"><img defer src="<?php echo get_stylesheet_directory_uri()?>/images/img_03_no.png" alt=""
                    class="imglink"></a>
        </div>
        <!-- ▲ Group Link ================================================== -->

        <!-- ▼ Topics ================================================== -->
        <div class="p-toppics">
            <h2 class="c-title">Topics</h2>
            <ul class="p-toppics__list"> <?php 
                    $args = array(
                        'post_status' => 'publish',
                        'posts_per_page' => 5,
                        'order'     => 'DESC',
                        'orderby'     => 'date'
                    );
                ?>
                <?php $getposts = new WP_query($args); ?>
                <?php while ($getposts->have_posts()) : $getposts->the_post(); ?>
                <li>
                    <span class="datepost"><?php echo get_the_date('Y/m/d'); ?></span>
                    <?php echo the_category($post->ID) ; ?>
                    <a class="title" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                </li>
                <?php endwhile; wp_reset_postdata(); ?>
            </ul>
            <div class="l-btn">
                <a href="<?php echo site_url('/topics/'); ?>" class="c-btn c-btn--small">一覧を見る</a>
            </div>
        </div>
        <!-- ▲ Topics ================================================== -->

        <!-- ▼ Group Link ================================================== -->
        <div class="c-grouplink c-grouplink--2">
            <a href="#"><img defer src="<?php echo get_stylesheet_directory_uri()?>/images/btn_03_no.png" alt=""
                    class="imglink"></a>
            <a href="#"><img defer src="<?php echo get_stylesheet_directory_uri()?>/images/btn_04_no.png" alt=""
                    class="imglink"></a>
        </div>
        <!-- ▲ Group Link ================================================== -->

        <!-- ▼ Access Map================================================== -->

        <div class="c-access">
            <?php
    if( have_rows('map','option') ):
        while ( have_rows('map','option') ) : the_row(); ?>
            <div class="c-access__inner c-flex">
                <div class="c-access__items">
                    <h3 class="c-title c-title--sub"><?php echo the_sub_field('title_map'); ?></h3>
                    <p class="address"><?php the_sub_field('address'); ?></p>
                    <p class="time"><?php the_sub_field('time'); ?></p>
                    <p>
                        <span class="tel"><?php the_sub_field('tel'); ?></span>
                        <span class="fax"><?php the_sub_field('fax'); ?></span>
                        <br />
                        <span class="email"><?php the_sub_field('email'); ?></span>
                    </p>
                </div>
                <div class="c-access__items">
                    <?php  $img= get_sub_field('image_map'); ?>
                    <img src="<?php echo $img['url'] ?>" alt="<?php echo $img['alt'] ?>" />

                </div>
            </div>
        </div>
        <?php
            endwhile;
            else :

    echo 'nothing found';

        endif; ?>
        <!-- ▲ Access Map================================================== -->
    </div>
</main>
<!-- ▲content ================================================== -->

<?php get_footer();?>