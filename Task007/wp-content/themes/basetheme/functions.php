<?php
define('THEME_URL', get_stylesheet_directory());
add_filter('show_admin_bar', '__return_false');

/**
 * Setup themes
 * 
 */
if(!function_exists('agl_theme_setup'))
{
	function agl_theme_setup()
	{
		add_theme_support('automatic-feed-links');
		add_theme_support( 'post-thumbnails' );
        add_theme_support( 'post-formats', array(
            'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
        ) );
    /* Add menu */
    add_theme_support( 'menus' );
    register_nav_menus(
      array(
        'primary-menu' => 'Main Menu',
        'footer-menu' => 'Footer Menu'
        )
    );
    add_theme_support(
      'html5',
      array(
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
      )
    );
    /**Add Sidebar */
    $sidebar = array(
      'name' => __('Main Sidebar', 'camtu'),
      'id' => 'main-sidebar',
      'class' => 'l-sidebar',
      'before_title' => '<h3 class="widgettitle">',
      'after_sidebar' => '</h3>'
    );
    register_sidebar( $sidebar );
	}
	add_action('init','agl_theme_setup');
}


/**
*Create function pagination
**/
function wp_corenavi_table($custom_query = null) {
  global $wp_query;
  if($custom_query) $main_query = $custom_query;
  else $main_query = $wp_query;
  $big = 999999999;
  $total = isset($main_query->max_num_pages)?$main_query->max_num_pages:'';
  if($total > 1) echo '<div class="c-pnav">';
  echo paginate_links( array(
     'base'        => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
     'format'   => '?paged=%#%',
     'current'  => max( 1, get_query_var('paged') ),
     'total'    => $total,
     'mid_size' => '10',
     'prev_text'    => __(' ','camtu'),
     'next_text'    => __(' ','camtu'),
  ) );
  if($total > 1) echo '</div>';
}


/** Option Pages */
if( function_exists('acf_add_options_page') ) {
	
  acf_add_options_page();
  
  acf_add_options_sub_page(
    array(
      'page_title' => 'Map access',
      'menu_title' =>'Map access',
      'menu_slug' => 'map-access',
      'capability' =>'edit_posts',
      'parent_slug' => '',
      'position' =>false,
      'icon_url' => false,
      'redirect' =>false
    )
  );
  acf_add_options_sub_page(
    array(
      'page_title' => 'Slider',
      'menu_title' =>'Slider',
      'menu_slug' => 'slider',
      'capability' =>'edit_posts',
      'parent_slug' => '',
      'position' =>false,
      'icon_url' => false,
      'redirect' =>false
    )
  );
	
}