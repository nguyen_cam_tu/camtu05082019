<footer class="c-footer">
	<div class="c-footer__logo">
		<div class="l-container">
			<a href="<?php echo site_url(); ?>"><img defer src="<?php echo get_stylesheet_directory_uri()?>/images/logo.png" alt="税理士法人下平会計事務所"></a>
		</div>
	</div>

	<div class="c-footer__link">
		<div class="l-container">
		<?php wp_nav_menu( array(
				'theme_location' => 'footer-menu', 
				'container' => 'ul', 
				'container_class' => '', 
				'menu_class' => 'menu ' 
			) ); ?>
		</div>
	</div>

	<div class="copyright">
		<div class="l-container">
			<p>Copyright 2015 Shimodaira Tax Accounting Office All Right Reserved.</p>
		</div>
	</div>
</footer>
<script defer src="<?php echo get_stylesheet_directory_uri()?>/js/common.js"></script>
</body>
</html>