-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 23, 2019 at 10:39 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `basic_wp`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-09-16 04:16:01', '2019-09-16 04:16:01', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_masterslider_options`
--

CREATE TABLE `wp_masterslider_options` (
  `ID` smallint(5) UNSIGNED NOT NULL,
  `option_name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `option_value` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_masterslider_options`
--

INSERT INTO `wp_masterslider_options` (`ID`, `option_name`, `option_value`) VALUES
(1, 'masterslider_custom_css_ver', '1.3'),
(2, 'master-slider-notice-installtion-time', '1568857673');

-- --------------------------------------------------------

--
-- Table structure for table `wp_masterslider_sliders`
--

CREATE TABLE `wp_masterslider_sliders` (
  `ID` mediumint(8) UNSIGNED NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slides_num` smallint(5) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `params` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_styles` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `custom_fonts` text COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `status` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'draft'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_masterslider_sliders`
--

INSERT INTO `wp_masterslider_sliders` (`ID`, `title`, `type`, `slides_num`, `date_created`, `date_modified`, `params`, `custom_styles`, `custom_fonts`, `status`) VALUES
(1, 'Simple Autoplay Slider', 'custom', 4, '2019-09-19 01:48:56', '2019-09-19 01:52:02', 'eyJtZXRhIjp7IlNldHRpbmdzIWlkcyI6IjEiLCJTZXR0aW5ncyFuZXh0SWQiOjIsIlNsaWRlIWlkcyI6IjE1LDE2LDE3LDE4IiwiU2xpZGUhbmV4dElkIjoxOSwiQ29udHJvbCFpZHMiOiIxLDQsNSw2IiwiQ29udHJvbCFuZXh0SWQiOjd9LCJNU1BhbmVsLlNldHRpbmdzIjp7IjEiOiJ7XCJpZFwiOlwiMVwiLFwic25hcHBpbmdcIjp0cnVlLFwiZGlzYWJsZUNvbnRyb2xzXCI6ZmFsc2UsXCJuYW1lXCI6XCJTaW1wbGUgQXV0b3BsYXkgU2xpZGVyXCIsXCJ3aWR0aFwiOjgwMCxcImhlaWdodFwiOjQ4MCxcIndyYXBwZXJXaWR0aFwiOjgwMCxcIndyYXBwZXJXaWR0aFVuaXRcIjpcInB4XCIsXCJhdXRvQ3JvcFwiOmZhbHNlLFwidHlwZVwiOlwiY3VzdG9tXCIsXCJzbGlkZXJJZFwiOlwiMVwiLFwiZW5hYmxlT3ZlcmxheUxheWVyc1wiOnRydWUsXCJsYXlvdXRcIjpcImJveGVkXCIsXCJhdXRvSGVpZ2h0XCI6ZmFsc2UsXCJ0clZpZXdcIjpcImJhc2ljXCIsXCJzcGVlZFwiOjIwLFwic3BhY2VcIjowLFwic3RhcnRcIjoxLFwiZ3JhYkN1cnNvclwiOnRydWUsXCJzd2lwZVwiOnRydWUsXCJtb3VzZVwiOnRydWUsXCJ3aGVlbFwiOmZhbHNlLFwiYXV0b3BsYXlcIjp0cnVlLFwibG9vcFwiOnRydWUsXCJzaHVmZmxlXCI6ZmFsc2UsXCJwcmVsb2FkXCI6XCItMVwiLFwib3ZlclBhdXNlXCI6dHJ1ZSxcImVuZFBhdXNlXCI6ZmFsc2UsXCJoaWRlTGF5ZXJzXCI6ZmFsc2UsXCJkaXJcIjpcImhcIixcInBhcmFsbGF4TW9kZVwiOlwic3dpcGVcIixcInVzZURlZXBMaW5rXCI6ZmFsc2UsXCJkZWVwTGlua1R5cGVcIjpcInBhdGhcIixcInNjcm9sbFBhcmFsbGF4TW92ZVwiOjMwLFwic2Nyb2xsUGFyYWxsYXhCR01vdmVcIjo1MCxcInNjcm9sbFBhcmFsbGF4RmFkZVwiOnRydWUsXCJjZW50ZXJDb250cm9sc1wiOnRydWUsXCJpbnN0YW50U2hvd0xheWVyc1wiOmZhbHNlLFwiY2xhc3NOYW1lXCI6XCJcIixcImJnQ29sb3JcIjpcIiMwMDAwMDBcIixcInNraW5cIjpcIm1zLXNraW4tbGlnaHQtM1wiLFwibXNUZW1wbGF0ZVwiOlwiY3VzdG9tXCIsXCJtc1RlbXBsYXRlQ2xhc3NcIjpcIlwiLFwidXNlZEZvbnRzXCI6XCJcIn0ifSwiTVNQYW5lbC5TbGlkZSI6eyIxNSI6IntcImlkXCI6MTUsXCJ0aW1lbGluZV9oXCI6MjAwLFwiYmdUaHVtYlwiOlwiLzIwMTkvMDkvbWFpbl9pbWcwM19uby0xNTB4MTUwLnBuZ1wiLFwiaXNPdmVybGF5TGF5ZXJzXCI6ZmFsc2UsXCJvcmRlclwiOjAsXCJiZ1wiOlwiLzIwMTkvMDkvbWFpbl9pbWcwM19uby5wbmdcIixcImR1cmF0aW9uXCI6XCIzXCIsXCJmaWxsTW9kZVwiOlwiZmlsbFwiLFwiYmd2X2ZpbGxtb2RlXCI6XCJmaWxsXCIsXCJiZ3ZfbG9vcFwiOlwiMVwiLFwiYmd2X211dGVcIjpcIjFcIixcImJndl9hdXRvcGF1c2VcIjpcIlwiLFwiYmdBbHRcIjpcIlwiLFwiYmdUaXRsZVwiOlwibWFpbl9pbWcwM19ub1wiLFwibGF5ZXJfaWRzXCI6W119IiwiMTYiOiJ7XCJpZFwiOjE2LFwidGltZWxpbmVfaFwiOjIwMCxcImJnVGh1bWJcIjpcIi8yMDE5LzA5L21haW5faW1nMDJfbm8tMTUweDE1MC5wbmdcIixcImlzT3ZlcmxheUxheWVyc1wiOmZhbHNlLFwib3JkZXJcIjoxLFwiYmdcIjpcIi8yMDE5LzA5L21haW5faW1nMDJfbm8ucG5nXCIsXCJkdXJhdGlvblwiOlwiM1wiLFwiZmlsbE1vZGVcIjpcImZpbGxcIixcImJndl9maWxsbW9kZVwiOlwiZmlsbFwiLFwiYmd2X2xvb3BcIjpcIjFcIixcImJndl9tdXRlXCI6XCIxXCIsXCJiZ3ZfYXV0b3BhdXNlXCI6XCJcIixcImJnQWx0XCI6XCJcIixcImJnVGl0bGVcIjpcIm1haW5faW1nMDJfbm9cIixcImxheWVyX2lkc1wiOltdfSIsIjE3Ijoie1wiaWRcIjoxNyxcInRpbWVsaW5lX2hcIjoyMDAsXCJiZ1RodW1iXCI6XCIvMjAxOS8wOS9tYWluX2ltZzA0X25vLTE1MHgxNTAucG5nXCIsXCJpc092ZXJsYXlMYXllcnNcIjpmYWxzZSxcIm9yZGVyXCI6MixcImJnXCI6XCIvMjAxOS8wOS9tYWluX2ltZzA0X25vLnBuZ1wiLFwiZHVyYXRpb25cIjpcIjNcIixcImZpbGxNb2RlXCI6XCJmaWxsXCIsXCJiZ3ZfZmlsbG1vZGVcIjpcImZpbGxcIixcImJndl9sb29wXCI6XCIxXCIsXCJiZ3ZfbXV0ZVwiOlwiMVwiLFwiYmd2X2F1dG9wYXVzZVwiOlwiXCIsXCJiZ0FsdFwiOlwiXCIsXCJiZ1RpdGxlXCI6XCJtYWluX2ltZzA0X25vXCIsXCJsYXllcl9pZHNcIjpbXX0iLCIxOCI6IntcImlkXCI6MTgsXCJ0aW1lbGluZV9oXCI6MjAwLFwiYmdUaHVtYlwiOlwiLzIwMTkvMDkvbWFpbl9pbWcwMV9uby0xNTB4MTUwLnBuZ1wiLFwiaXNPdmVybGF5TGF5ZXJzXCI6ZmFsc2UsXCJvcmRlclwiOjMsXCJiZ1wiOlwiLzIwMTkvMDkvbWFpbl9pbWcwMV9uby5wbmdcIixcImR1cmF0aW9uXCI6MyxcImZpbGxNb2RlXCI6XCJjZW50ZXJcIixcImJndl9maWxsbW9kZVwiOlwiZmlsbFwiLFwiYmd2X2xvb3BcIjpcIjFcIixcImJndl9tdXRlXCI6XCIxXCIsXCJiZ3ZfYXV0b3BhdXNlXCI6XCJcIixcImJnQWx0XCI6XCJcIixcImJnVGl0bGVcIjpcIm1haW5faW1nMDFfbm9cIixcImxheWVyX2lkc1wiOltdfSJ9LCJNU1BhbmVsLkNvbnRyb2wiOnsiMSI6IntcImlkXCI6XCIxXCIsXCJsYWJlbFwiOlwiQXJyb3dzXCIsXCJuYW1lXCI6XCJhcnJvd3NcIixcImF1dG9IaWRlXCI6ZmFsc2UsXCJvdmVyVmlkZW9cIjp0cnVlLFwiaW5zZXRcIjp0cnVlfSIsIjQiOiJ7XCJpZFwiOjQsXCJsYWJlbFwiOlwiQ2lyY2xlIFRpbWVyXCIsXCJuYW1lXCI6XCJjaXJjbGV0aW1lclwiLFwiYXV0b0hpZGVcIjpmYWxzZSxcIm92ZXJWaWRlb1wiOnRydWUsXCJjb2xvclwiOlwiIzQzNDM0M1wiLFwicmFkaXVzXCI6MTAsXCJzdHJva2VcIjozLFwiaW5zZXRcIjp0cnVlfSIsIjUiOiJ7XCJpZFwiOjUsXCJsYWJlbFwiOlwiTGluZSBUaW1lclwiLFwibmFtZVwiOlwidGltZWJhclwiLFwiYXV0b0hpZGVcIjpmYWxzZSxcIm92ZXJWaWRlb1wiOnRydWUsXCJjb2xvclwiOlwiI0ZGRkZGRlwiLFwid2lkdGhcIjo0LFwiYWxpZ25cIjpcImJvdHRvbVwiLFwiaW5zZXRcIjp0cnVlfSIsIjYiOiJ7XCJpZFwiOjYsXCJsYWJlbFwiOlwiQnVsbGV0c1wiLFwibmFtZVwiOlwiYnVsbGV0c1wiLFwiYXV0b0hpZGVcIjp0cnVlLFwib3ZlclZpZGVvXCI6dHJ1ZSxcIm1hcmdpblwiOjEwLFwiZGlyXCI6XCJoXCIsXCJhbGlnblwiOlwiYm90dG9tXCIsXCJpbnNldFwiOnRydWV9In19', '', '', 'published');

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_image_storage`
--

CREATE TABLE `wp_nextend2_image_storage` (
  `id` int(11) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `image` text NOT NULL,
  `value` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_nextend2_image_storage`
--

INSERT INTO `wp_nextend2_image_storage` (`id`, `hash`, `image`, `value`) VALUES
(1, '38a4cd910a884eb578cd893a83a08b61', '$upload$/2019/09/main_img01_no.png', 'eyJkZXNrdG9wIjp7InNpemUiOiIwfCp8MCJ9LCJkZXNrdG9wLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJ0YWJsZXQiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwidGFibGV0LXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJtb2JpbGUiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwibW9iaWxlLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9fQ=='),
(2, '7dbc568dc175451a41b9137215d06a54', '$upload$/2019/09/main_img02_no.png', 'eyJkZXNrdG9wIjp7InNpemUiOiIwfCp8MCJ9LCJkZXNrdG9wLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJ0YWJsZXQiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwidGFibGV0LXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJtb2JpbGUiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwibW9iaWxlLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9fQ=='),
(3, '4285f3feb43fe010c1f2dd89b9629329', '$upload$/2019/09/main_img03_no.png', 'eyJkZXNrdG9wIjp7InNpemUiOiIwfCp8MCJ9LCJkZXNrdG9wLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJ0YWJsZXQiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwidGFibGV0LXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJtb2JpbGUiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwibW9iaWxlLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9fQ=='),
(4, '52bb75ab3c84a503311aced22deca7b1', '$upload$/2019/09/main_img04_no.png', 'eyJkZXNrdG9wIjp7InNpemUiOiIwfCp8MCJ9LCJkZXNrdG9wLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJ0YWJsZXQiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwidGFibGV0LXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJtb2JpbGUiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwibW9iaWxlLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9fQ==');

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_section_storage`
--

CREATE TABLE `wp_nextend2_section_storage` (
  `id` int(11) NOT NULL,
  `application` varchar(20) NOT NULL,
  `section` varchar(128) NOT NULL,
  `referencekey` varchar(128) NOT NULL,
  `value` mediumtext NOT NULL,
  `system` int(11) NOT NULL DEFAULT 0,
  `editable` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_nextend2_section_storage`
--

INSERT INTO `wp_nextend2_section_storage` (`id`, `application`, `section`, `referencekey`, `value`, `system`, `editable`) VALUES
(10000, 'system', 'global', 'n2_ss3_version', '3.3.22r4878', 1, 1),
(10002, 'smartslider', 'free', 'subscribeOnImport', '1', 0, 1),
(10004, 'smartslider', 'free', 'promoUpgrade', '1', 0, 1),
(10113, 'cache', 'notweb/n2-ss-2', 'data.manifest', '{\"generator\":[]}', 0, 1),
(10114, 'cache', 'notweb/n2-ss-2', 'variations.manifest', '1', 0, 1),
(10115, 'cache', 'notweb/n2-ss-2', 'slideren_US1.manifest', '{\"hash\":\"\",\"nextCacheRefresh\":1884478799,\"currentPath\":\"25457d298a8d3719e69f940c183882d0\",\"version\":\"3.3.22\"}', 0, 1),
(10116, 'cache', 'notweb/n2-ss-2', 'slideren_US1', '{\"html\":\"<div class=\\\"n2-section-smartslider \\\" role=\\\"region\\\" aria-label=\\\"Slider\\\"><style>div#n2-ss-2{width:1000px;float:left;margin:0px 0px 0px 0px;}html[dir=\\\"rtl\\\"] div#n2-ss-2{float:right;}div#n2-ss-2 .n2-ss-slider-1{position:relative;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;height:400px;border-style:solid;border-width:0px;border-color:#3e3e3e;border-color:RGBA(62,62,62,1);border-radius:0px;background-clip:padding-box;background-repeat:repeat;background-position:50% 50%;background-size:cover;background-attachment:scroll;}div#n2-ss-2 .n2-ss-slider-background-video-container{position:absolute;left:0;top:0;width:100%;height:100%;overflow:hidden;}div#n2-ss-2 .n2-ss-slider-2{position:relative;width:100%;height:100%;}.x-firefox div#n2-ss-2 .n2-ss-slider-2{opacity:0.99999;}div#n2-ss-2 .n2-ss-slider-3{position:relative;width:100%;height:100%;overflow:hidden;outline:1px solid rgba(0,0,0,0);z-index:10;}div#n2-ss-2 .n2-ss-slide-backgrounds,div#n2-ss-2 .n2-ss-slider-3 > .n-particles-js-canvas-el,div#n2-ss-2 .n2-ss-slider-3 > .n2-ss-divider{position:absolute;left:0;top:0;width:100%;height:100%;}div#n2-ss-2 .n2-ss-slide-backgrounds{z-index:10;}div#n2-ss-2 .n2-ss-slider-3 > .n-particles-js-canvas-el{z-index:12;}div#n2-ss-2 .n2-ss-slide-backgrounds > *{overflow:hidden;}div#n2-ss-2 .n2-ss-slide{position:absolute;top:0;left:0;width:100%;height:100%;z-index:20;display:block;-webkit-backface-visibility:hidden;}div#n2-ss-2 .n2-ss-layers-container{position:relative;width:1000px;height:400px;}div#n2-ss-2 .n2-ss-parallax-clip > .n2-ss-layers-container{position:absolute;right:0;}div#n2-ss-2 .n2-ss-slide{perspective:1500px;}div#n2-ss-2[data-ie] .n2-ss-slide{perspective:none;transform:perspective(1500px);}div#n2-ss-2 .n2-ss-slide-active{z-index:21;}div#n2-ss-2 .nextend-arrow{cursor:pointer;overflow:hidden;line-height:0 !important;z-index:20;}div#n2-ss-2 .nextend-arrow img{position:relative;min-height:0;min-width:0;vertical-align:top;width:auto;height:auto;max-width:100%;max-height:100%;display:inline;}div#n2-ss-2 .nextend-arrow img.n2-arrow-hover-img{display:none;}div#n2-ss-2 .nextend-arrow:HOVER img.n2-arrow-hover-img{display:inline;}div#n2-ss-2 .nextend-arrow:HOVER img.n2-arrow-normal-img{display:none;}div#n2-ss-2 .nextend-arrow-animated{overflow:hidden;}div#n2-ss-2 .nextend-arrow-animated > div{position:relative;}div#n2-ss-2 .nextend-arrow-animated .n2-active{position:absolute;}div#n2-ss-2 .nextend-arrow-animated-fade{transition:background 0.3s, opacity 0.4s;}div#n2-ss-2 .nextend-arrow-animated-horizontal > div{transition:all 0.4s;left:0;}div#n2-ss-2 .nextend-arrow-animated-horizontal .n2-active{top:0;}div#n2-ss-2 .nextend-arrow-previous.nextend-arrow-animated-horizontal:HOVER > div,div#n2-ss-2 .nextend-arrow-next.nextend-arrow-animated-horizontal .n2-active{left:-100%;}div#n2-ss-2 .nextend-arrow-previous.nextend-arrow-animated-horizontal .n2-active,div#n2-ss-2 .nextend-arrow-next.nextend-arrow-animated-horizontal:HOVER > div{left:100%;}div#n2-ss-2 .nextend-arrow.nextend-arrow-animated-horizontal:HOVER .n2-active{left:0;}div#n2-ss-2 .nextend-arrow-animated-vertical > div{transition:all 0.4s;top:0;}div#n2-ss-2 .nextend-arrow-animated-vertical .n2-active{left:0;}div#n2-ss-2 .nextend-arrow-animated-vertical .n2-active{top:-100%;}div#n2-ss-2 .nextend-arrow-animated-vertical:HOVER > div{top:100%;}div#n2-ss-2 .nextend-arrow-animated-vertical:HOVER .n2-active{top:0;}div#n2-ss-2 .n2-style-1045855924f7a9e63dd7136127d954e5-heading{background: #ffffff;background: RGBA(255,255,255,0);opacity:1;padding:0px 0px 0px 0px ;box-shadow: none;border-width: 0px;border-style: solid;border-color: #000000; border-color: RGBA(0,0,0,1);border-radius:0px;}<\\/style><div id=\\\"n2-ss-2-align\\\" class=\\\"n2-ss-align\\\"><div class=\\\"n2-padding\\\"><div id=\\\"n2-ss-2\\\" data-creator=\\\"Smart Slider 3\\\" class=\\\"n2-ss-slider n2-ow n2-has-hover n2notransition  n2-ss-load-fade \\\" data-minFontSizedesktopPortrait=\\\"4\\\" data-minFontSizedesktopLandscape=\\\"4\\\" data-minFontSizetabletPortrait=\\\"4\\\" data-minFontSizetabletLandscape=\\\"4\\\" data-minFontSizemobilePortrait=\\\"4\\\" data-minFontSizemobileLandscape=\\\"4\\\" style=\\\"font-size: 1rem;\\\" data-fontsize=\\\"16\\\">\\r\\n        <div class=\\\"n2-ss-slider-1 n2-ss-swipe-element n2-ow\\\" style=\\\"\\\">\\r\\n                        <div class=\\\"n2-ss-slider-2 n2-ow\\\">\\r\\n                                <div class=\\\"n2-ss-slider-3 n2-ow\\\" style=\\\"\\\">\\r\\n\\r\\n                    <div class=\\\"n2-ss-slide-backgrounds\\\"><\\/div><div data-first=\\\"1\\\" data-slide-duration=\\\"0\\\" data-id=\\\"4\\\" style=\\\"\\\" class=\\\" n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-4\\\"><div class=\\\"n2-ss-slide-background n2-ow\\\" data-mode=\\\"center\\\"><div data-hash=\\\"5a8df84a228498b77bac8e86ffc63df0\\\" data-desktop=\\\"\\/\\/localhost:8080\\/CamTu\\/Task007\\/wp-content\\/uploads\\/resized\\/31a3496303500345a963f670196abc06\\/main_img01_no.png\\\" class=\\\"n2-ss-slide-background-image\\\" data-blur=\\\"0\\\"><img src=\\\"\\/\\/localhost:8080\\/CamTu\\/Task007\\/wp-content\\/uploads\\/resized\\/31a3496303500345a963f670196abc06\\/main_img01_no.png\\\" alt=\\\"\\\" \\/><\\/div><\\/div><div class=\\\"n2-ss-layers-container n2-ow\\\" data-csstextalign=\\\"center\\\" style=\\\"\\\"><\\/div><\\/div><div data-slide-duration=\\\"0\\\" data-id=\\\"5\\\" style=\\\"\\\" class=\\\" n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-5\\\"><div class=\\\"n2-ss-slide-background n2-ow\\\" data-mode=\\\"center\\\"><div data-hash=\\\"702bc0b1dcc216d00a0db8a09f460ad1\\\" data-desktop=\\\"\\/\\/localhost:8080\\/CamTu\\/Task007\\/wp-content\\/uploads\\/resized\\/eddeb292d55dd1f40a97ad737ed42c2e\\/main_img02_no.png\\\" class=\\\"n2-ss-slide-background-image\\\" data-blur=\\\"0\\\"><img src=\\\"\\/\\/localhost:8080\\/CamTu\\/Task007\\/wp-content\\/uploads\\/resized\\/eddeb292d55dd1f40a97ad737ed42c2e\\/main_img02_no.png\\\" alt=\\\"\\\" \\/><\\/div><\\/div><div class=\\\"n2-ss-layers-container n2-ow\\\" data-csstextalign=\\\"center\\\" style=\\\"\\\"><\\/div><\\/div><div data-slide-duration=\\\"0\\\" data-id=\\\"6\\\" style=\\\"\\\" class=\\\" n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-6\\\"><div class=\\\"n2-ss-slide-background n2-ow\\\" data-mode=\\\"center\\\"><div data-hash=\\\"030d25694a5fb66e4b1633bc8a21c7f2\\\" data-desktop=\\\"\\/\\/localhost:8080\\/CamTu\\/Task007\\/wp-content\\/uploads\\/resized\\/702aba94778bf63de6c95cbc4506a594\\/main_img03_no.png\\\" class=\\\"n2-ss-slide-background-image\\\" data-blur=\\\"0\\\"><img src=\\\"\\/\\/localhost:8080\\/CamTu\\/Task007\\/wp-content\\/uploads\\/resized\\/702aba94778bf63de6c95cbc4506a594\\/main_img03_no.png\\\" alt=\\\"\\\" \\/><\\/div><\\/div><div class=\\\"n2-ss-layers-container n2-ow\\\" data-csstextalign=\\\"center\\\" style=\\\"\\\"><\\/div><\\/div><div data-slide-duration=\\\"0\\\" data-id=\\\"7\\\" style=\\\"\\\" class=\\\" n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-7\\\"><div class=\\\"n2-ss-slide-background n2-ow\\\" data-mode=\\\"center\\\"><div data-hash=\\\"48ef8fa270b33807b5bee83f26bba219\\\" data-desktop=\\\"\\/\\/localhost:8080\\/CamTu\\/Task007\\/wp-content\\/uploads\\/resized\\/6e693595e699d5dd1b208a9f14bfa5b9\\/main_img04_no.png\\\" class=\\\"n2-ss-slide-background-image\\\" data-blur=\\\"0\\\"><img src=\\\"\\/\\/localhost:8080\\/CamTu\\/Task007\\/wp-content\\/uploads\\/resized\\/6e693595e699d5dd1b208a9f14bfa5b9\\/main_img04_no.png\\\" alt=\\\"\\\" \\/><\\/div><\\/div><div class=\\\"n2-ss-layers-container n2-ow\\\" data-csstextalign=\\\"center\\\" style=\\\"\\\"><\\/div><\\/div>                <\\/div>\\r\\n            <\\/div>\\r\\n            <div data-ssleft=\\\"0+15\\\" data-sstop=\\\"height\\/2-previousheight\\/2\\\" id=\\\"n2-ss-2-arrow-previous\\\" class=\\\"n2-ss-widget n2-ss-widget-display-desktop n2-ss-widget-display-tablet n2-ss-widget-display-mobile n2-style-1045855924f7a9e63dd7136127d954e5-heading nextend-arrow n2-ow nextend-arrow-previous  nextend-arrow-animated-fade n2-ib\\\" style=\\\"position: absolute;\\\" role=\\\"button\\\" aria-label=\\\"previous arrow\\\" tabindex=\\\"0\\\"><img class=\\\"n2-ow\\\" data-no-lazy=\\\"1\\\" data-hack=\\\"data-lazy-src\\\" src=\\\"data:image\\/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMiIgaGVpZ2h0PSIzMiIgdmlld0JveD0iMCAwIDMyIDMyIj4KICAgIDxwYXRoIGZpbGw9IiNmZmZmZmYiIGZpbGwtcnVsZT0iZXZlbm9kZCIgZD0iTTIwLjY1NCA5LjQzbC02LjMyNiA2LjUyNSA2LjMyNiA2LjZjLjI0Mi4yNTMuMzYuNTQ1LjM2Ljg4IDAgLjMzMy0uMTE4LjYyNS0uMzYuODc3bC0xLjI0NCAxLjNjLS4yNC4yNS0uNTIzLjM3NS0uODQuMzc1LS4zMiAwLS42LS4xMjQtLjg0LS4zNzZsLTguMzY3LTguNzJjLS4yNC0uMjUtLjM2My0uNTQtLjM2My0uODggMC0uMzM0LjEyMi0uNjMuMzYzLS44OGw4LjM2Ny04Ljc1Yy4yMy0uMjUyLjUxLS4zNzcuODMtLjM3Ny4zMjUgMCAuNjA3LjEyNi44NS4zNzhsMS4yNDIgMS4zMjZjLjI0Mi4yNTIuMzYuNTQuMzYuODY0IDAgLjMyLS4xMTguNjEtLjM2Ljg2eiIgb3BhY2l0eT0iMC40NSIvPgo8L3N2Zz4K\\\" alt=\\\"previous arrow\\\" \\/><\\/div>\\n<div data-ssright=\\\"0+15\\\" data-sstop=\\\"height\\/2-nextheight\\/2\\\" id=\\\"n2-ss-2-arrow-next\\\" class=\\\"n2-ss-widget n2-ss-widget-display-desktop n2-ss-widget-display-tablet n2-ss-widget-display-mobile n2-style-1045855924f7a9e63dd7136127d954e5-heading nextend-arrow n2-ow nextend-arrow-next  nextend-arrow-animated-fade n2-ib\\\" style=\\\"position: absolute;\\\" role=\\\"button\\\" aria-label=\\\"next arrow\\\" tabindex=\\\"0\\\"><img class=\\\"n2-ow\\\" data-no-lazy=\\\"1\\\" data-hack=\\\"data-lazy-src\\\" src=\\\"data:image\\/svg+xml;base64,PHN2ZyB3aWR0aD0iMzIiIGhlaWdodD0iMzIiIHZpZXdCb3g9IjAgMCAzMiAzMiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJNMTMuMjg2IDI1LjYxYy0uMjQuMjUzLS41Mi4zNzctLjg0LjM3Ny0uMzE4IDAtLjYtLjEyNC0uODQtLjM3NmwtMS4yNDUtMS4yOTdjLS4yNC0uMjUyLS4zNi0uNTQ0LS4zNi0uODc4IDAtLjMzNC4xMi0uNjI2LjM2LS44NzhsNi4zMjgtNi42TDEwLjM2IDkuNDNjLS4yNC0uMjUtLjM2LS41NC0uMzYtLjg2NCAwLS4zMjMuMTItLjYxMi4zNi0uODY0bDEuMjQ1LTEuMzI1Yy4yNC0uMjUyLjUyMy0uMzc3Ljg0OC0uMzc3LjMyMyAwIC42MDIuMTI1LjgzMy4zNzdsOC4zNjYgOC43NDZjLjI0LjI1LjM2My41NDYuMzYzLjg4MiAwIC4zNC0uMTIyLjYzNC0uMzYzLjg4NmwtOC4zNjYgOC43MnoiIGZpbGw9IiNmZmZmZmYiIG9wYWNpdHk9IjAuNDUiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPjwvc3ZnPg==\\\" alt=\\\"next arrow\\\" \\/><\\/div>\\n        <\\/div>\\r\\n        <\\/div><div class=\\\"n2-clear\\\"><\\/div><div id=\\\"n2-ss-2-spinner\\\" style=\\\"display: none;\\\"><div><div class=\\\"n2-ss-spinner-simple-white-container\\\"><div class=\\\"n2-ss-spinner-simple-white\\\"><\\/div><\\/div><\\/div><\\/div><\\/div><\\/div><div id=\\\"n2-ss-2-placeholder\\\" style=\\\"position: relative;z-index:2;background-color:RGBA(0,0,0,0);max-height:3000px; background-color:RGBA(255,255,255,0);\\\"><img style=\\\"width: 100%; max-width:3000px; display: block;opacity:0;margin:0px;\\\" class=\\\"n2-ow\\\" src=\\\"data:image\\/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMCIgd2lkdGg9IjEwMDAiIGhlaWdodD0iNDAwIiA+PC9zdmc+\\\" alt=\\\"Slider\\\" \\/><\\/div><\\/div>\",\"assets\":{\"css\":{\"staticGroup\":{\"smartslider\":\"C:\\\\xampp\\\\htdocs\\\\CamTu\\\\Task007\\\\wp-content\\\\plugins\\\\smart-slider-3\\\\library\\\\media\\/smartslider.min.css\"},\"files\":[],\"urls\":[],\"codes\":[],\"firstCodes\":[],\"inline\":[\".n2-ss-spinner-simple-white-container {\\r\\n    position: absolute;\\r\\n    top: 50%;\\r\\n    left: 50%;\\r\\n    margin: -20px;\\r\\n    background: #fff;\\r\\n    width: 20px;\\r\\n    height: 20px;\\r\\n    padding: 10px;\\r\\n    border-radius: 50%;\\r\\n    z-index: 1000;\\r\\n}\\r\\n\\r\\n.n2-ss-spinner-simple-white {\\r\\n  outline: 1px solid RGBA(0,0,0,0);\\r\\n  width:100%;\\r\\n  height: 100%;\\r\\n}\\r\\n\\r\\n.n2-ss-spinner-simple-white:before {\\r\\n    position: absolute;\\r\\n    top: 50%;\\r\\n    left: 50%;\\r\\n    width: 20px;\\r\\n    height: 20px;\\r\\n    margin-top: -11px;\\r\\n    margin-left: -11px;\\r\\n}\\r\\n\\r\\n.n2-ss-spinner-simple-white:not(:required):before {\\r\\n    content: \'\';\\r\\n    border-radius: 50%;\\r\\n    border-top: 2px solid #333;\\r\\n    border-right: 2px solid transparent;\\r\\n    animation: n2SimpleWhite .6s linear infinite;\\r\\n}\\r\\n@keyframes n2SimpleWhite {\\r\\n    to {transform: rotate(360deg);}\\r\\n}\"],\"globalInline\":[]},\"less\":{\"staticGroup\":[],\"files\":[],\"urls\":[],\"codes\":[],\"firstCodes\":[],\"inline\":[],\"globalInline\":[]},\"js\":{\"staticGroup\":{\"smartslider-simple-type-frontend\":\"C:\\\\xampp\\\\htdocs\\\\CamTu\\\\Task007\\\\wp-content\\\\plugins\\\\smart-slider-3\\\\library\\\\media\\/plugins\\\\type\\\\simple\\\\simple\\/dist\\/smartslider-simple-type-frontend.min.js\"},\"files\":[],\"urls\":[],\"codes\":[],\"firstCodes\":[],\"inline\":[\"N2R([\\\"nextend-frontend\\\",\\\"smartslider-frontend\\\",\\\"smartslider-simple-type-frontend\\\"],function(){new N2Classes.SmartSliderSimple(\'#n2-ss-2\', {\\\"admin\\\":false,\\\"translate3d\\\":1,\\\"callbacks\\\":\\\"\\\",\\\"background.video.mobile\\\":1,\\\"align\\\":\\\"normal\\\",\\\"isDelayed\\\":0,\\\"load\\\":{\\\"fade\\\":1,\\\"scroll\\\":0},\\\"playWhenVisible\\\":1,\\\"playWhenVisibleAt\\\":0.5,\\\"responsive\\\":{\\\"desktop\\\":1,\\\"tablet\\\":1,\\\"mobile\\\":1,\\\"onResizeEnabled\\\":true,\\\"type\\\":\\\"auto\\\",\\\"downscale\\\":1,\\\"upscale\\\":1,\\\"minimumHeight\\\":0,\\\"maximumHeight\\\":3000,\\\"maximumSlideWidth\\\":3000,\\\"maximumSlideWidthLandscape\\\":3000,\\\"maximumSlideWidthTablet\\\":3000,\\\"maximumSlideWidthTabletLandscape\\\":3000,\\\"maximumSlideWidthMobile\\\":3000,\\\"maximumSlideWidthMobileLandscape\\\":3000,\\\"maximumSlideWidthConstrainHeight\\\":0,\\\"forceFull\\\":0,\\\"forceFullOverflowX\\\":\\\"body\\\",\\\"forceFullHorizontalSelector\\\":\\\"\\\",\\\"constrainRatio\\\":1,\\\"sliderHeightBasedOn\\\":\\\"real\\\",\\\"decreaseSliderHeight\\\":0,\\\"focusUser\\\":1,\\\"deviceModes\\\":{\\\"desktopPortrait\\\":1,\\\"desktopLandscape\\\":0,\\\"tabletPortrait\\\":1,\\\"tabletLandscape\\\":0,\\\"mobilePortrait\\\":1,\\\"mobileLandscape\\\":0},\\\"normalizedDeviceModes\\\":{\\\"unknownUnknown\\\":[\\\"unknown\\\",\\\"Unknown\\\"],\\\"desktopPortrait\\\":[\\\"desktop\\\",\\\"Portrait\\\"],\\\"desktopLandscape\\\":[\\\"desktop\\\",\\\"Portrait\\\"],\\\"tabletPortrait\\\":[\\\"tablet\\\",\\\"Portrait\\\"],\\\"tabletLandscape\\\":[\\\"tablet\\\",\\\"Portrait\\\"],\\\"mobilePortrait\\\":[\\\"mobile\\\",\\\"Portrait\\\"],\\\"mobileLandscape\\\":[\\\"mobile\\\",\\\"Portrait\\\"]},\\\"verticalRatioModifiers\\\":{\\\"unknownUnknown\\\":1,\\\"desktopPortrait\\\":1,\\\"desktopLandscape\\\":1,\\\"tabletPortrait\\\":1,\\\"tabletLandscape\\\":1,\\\"mobilePortrait\\\":1,\\\"mobileLandscape\\\":1},\\\"minimumFontSizes\\\":{\\\"desktopPortrait\\\":4,\\\"desktopLandscape\\\":4,\\\"tabletPortrait\\\":4,\\\"tabletLandscape\\\":4,\\\"mobilePortrait\\\":4,\\\"mobileLandscape\\\":4},\\\"ratioToDevice\\\":{\\\"Portrait\\\":{\\\"tablet\\\":0.7,\\\"mobile\\\":0.5},\\\"Landscape\\\":{\\\"tablet\\\":0,\\\"mobile\\\":0}},\\\"sliderWidthToDevice\\\":{\\\"desktopPortrait\\\":1000,\\\"desktopLandscape\\\":1000,\\\"tabletPortrait\\\":700,\\\"tabletLandscape\\\":0,\\\"mobilePortrait\\\":500,\\\"mobileLandscape\\\":0},\\\"basedOn\\\":\\\"combined\\\",\\\"orientationMode\\\":\\\"width_and_height\\\",\\\"overflowHiddenPage\\\":0,\\\"desktopPortraitScreenWidth\\\":1200,\\\"tabletPortraitScreenWidth\\\":800,\\\"mobilePortraitScreenWidth\\\":440,\\\"tabletLandscapeScreenWidth\\\":800,\\\"mobileLandscapeScreenWidth\\\":440,\\\"focus\\\":{\\\"offsetTop\\\":\\\"#wpadminbar\\\",\\\"offsetBottom\\\":\\\"\\\"}},\\\"controls\\\":{\\\"mousewheel\\\":0,\\\"touch\\\":\\\"horizontal\\\",\\\"keyboard\\\":1,\\\"blockCarouselInteraction\\\":1},\\\"lazyLoad\\\":0,\\\"lazyLoadNeighbor\\\":0,\\\"blockrightclick\\\":0,\\\"maintainSession\\\":0,\\\"autoplay\\\":{\\\"enabled\\\":1,\\\"start\\\":1,\\\"duration\\\":2000,\\\"autoplayToSlide\\\":-1,\\\"autoplayToSlideIndex\\\":-1,\\\"allowReStart\\\":0,\\\"pause\\\":{\\\"click\\\":0,\\\"mouse\\\":\\\"0\\\",\\\"mediaStarted\\\":1},\\\"resume\\\":{\\\"click\\\":0,\\\"mouse\\\":0,\\\"mediaEnded\\\":1,\\\"slidechanged\\\":0}},\\\"perspective\\\":1500,\\\"layerMode\\\":{\\\"playOnce\\\":0,\\\"playFirstLayer\\\":1,\\\"mode\\\":\\\"skippable\\\",\\\"inAnimation\\\":\\\"mainInEnd\\\"},\\\"initCallbacks\\\":[\\\"N2D(\\\\\\\"SmartSliderWidgetArrowImage\\\\\\\",function(i,e){function s(e,s,t,h){this.slider=e,this.slider.started(i.proxy(this.start,this,s,t,h))}return s.prototype.start=function(e,s,t){return this.slider.sliderElement.data(\\\\\\\"arrow\\\\\\\")?!1:(this.slider.sliderElement.data(\\\\\\\"arrow\\\\\\\",this),this.deferred=i.Deferred(),this.slider.sliderElement.on(\\\\\\\"SliderDevice\\\\\\\",i.proxy(this.onDevice,this)).trigger(\\\\\\\"addWidget\\\\\\\",this.deferred),this.previous=i(\\\\\\\"#\\\\\\\"+this.slider.elementID+\\\\\\\"-arrow-previous\\\\\\\").on(\\\\\\\"click\\\\\\\",i.proxy(function(i){i.stopPropagation(),this.slider[this.slider.getDirectionPrevious()]()},this)),this.previousResize=this.previous.find(\\\\\\\".n2-resize\\\\\\\"),0===this.previousResize.length&&(this.previousResize=this.previous),this.next=i(\\\\\\\"#\\\\\\\"+this.slider.elementID+\\\\\\\"-arrow-next\\\\\\\").on(\\\\\\\"click\\\\\\\",i.proxy(function(i){i.stopPropagation(),this.slider[this.slider.getDirectionNext()]()},this)),this.nextResize=this.next.find(\\\\\\\".n2-resize\\\\\\\"),0===this.nextResize.length&&(this.nextResize=this.next),this.desktopRatio=e,this.tabletRatio=s,this.mobileRatio=t,void i.when(this.previous.n2imagesLoaded(),this.next.n2imagesLoaded()).always(i.proxy(this.loaded,this)))},s.prototype.loaded=function(){this.previous.css(\\\\\\\"display\\\\\\\",\\\\\\\"inline-block\\\\\\\"),this.previousResize.css(\\\\\\\"display\\\\\\\",\\\\\\\"inline-block\\\\\\\"),this.previousWidth=this.previousResize.width(),this.previousHeight=this.previousResize.height(),this.previousResize.css(\\\\\\\"display\\\\\\\",\\\\\\\"\\\\\\\"),this.previous.css(\\\\\\\"display\\\\\\\",\\\\\\\"\\\\\\\"),this.next.css(\\\\\\\"display\\\\\\\",\\\\\\\"inline-block\\\\\\\"),this.nextResize.css(\\\\\\\"display\\\\\\\",\\\\\\\"inline-block\\\\\\\"),this.nextWidth=this.nextResize.width(),this.nextHeight=this.nextResize.height(),this.nextResize.css(\\\\\\\"display\\\\\\\",\\\\\\\"\\\\\\\"),this.next.css(\\\\\\\"display\\\\\\\",\\\\\\\"\\\\\\\"),this.previousResize.find(\\\\\\\"img\\\\\\\").css(\\\\\\\"width\\\\\\\",\\\\\\\"100%\\\\\\\"),this.nextResize.find(\\\\\\\"img\\\\\\\").css(\\\\\\\"width\\\\\\\",\\\\\\\"100%\\\\\\\"),this.onDevice(null,{device:this.slider.responsive.getDeviceMode()}),this.deferred.resolve()},s.prototype.onDevice=function(i,e){var s=1;switch(e.device){case\\\\\\\"tablet\\\\\\\":s=this.tabletRatio;break;case\\\\\\\"mobile\\\\\\\":s=this.mobileRatio;break;default:s=this.desktopRatio}this.previousResize.width(this.previousWidth*s),this.previousResize.height(this.previousHeight*s),this.nextResize.width(this.nextWidth*s),this.nextResize.height(this.nextHeight*s)},s});\\\",\\\"new N2Classes.SmartSliderWidgetArrowImage(this, 1, 0.7, 0.5);\\\"],\\\"allowBGImageAttachmentFixed\\\":false,\\\"bgAnimationsColor\\\":\\\"RGBA(51,51,51,1)\\\",\\\"bgAnimations\\\":0,\\\"mainanimation\\\":{\\\"type\\\":\\\"horizontal\\\",\\\"duration\\\":800,\\\"delay\\\":0,\\\"ease\\\":\\\"easeOutQuad\\\",\\\"parallax\\\":0,\\\"shiftedBackgroundAnimation\\\":0},\\\"carousel\\\":1,\\\"dynamicHeight\\\":0});});\"],\"globalInline\":[]},\"googleFonts\":{\"staticGroup\":[],\"files\":[],\"urls\":[],\"codes\":[],\"firstCodes\":[],\"inline\":[],\"globalInline\":[]},\"image\":{\"images\":[\"\\/\\/localhost:8080\\/CamTu\\/Task007\\/wp-content\\/uploads\\/resized\\/31a3496303500345a963f670196abc06\\/main_img01_no.png\",\"\\/\\/localhost:8080\\/CamTu\\/Task007\\/wp-content\\/uploads\\/resized\\/eddeb292d55dd1f40a97ad737ed42c2e\\/main_img02_no.png\",\"\\/\\/localhost:8080\\/CamTu\\/Task007\\/wp-content\\/uploads\\/resized\\/702aba94778bf63de6c95cbc4506a594\\/main_img03_no.png\",\"\\/\\/localhost:8080\\/CamTu\\/Task007\\/wp-content\\/uploads\\/resized\\/6e693595e699d5dd1b208a9f14bfa5b9\\/main_img04_no.png\"]}}}', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_smartslider3_generators`
--

CREATE TABLE `wp_nextend2_smartslider3_generators` (
  `id` int(11) NOT NULL,
  `group` varchar(254) NOT NULL,
  `type` varchar(254) NOT NULL,
  `params` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_smartslider3_sliders`
--

CREATE TABLE `wp_nextend2_smartslider3_sliders` (
  `id` int(11) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL,
  `params` mediumtext NOT NULL,
  `time` datetime NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_nextend2_smartslider3_sliders`
--

INSERT INTO `wp_nextend2_smartslider3_sliders` (`id`, `alias`, `title`, `type`, `params`, `time`, `thumbnail`, `ordering`) VALUES
(2, NULL, 'Slider homepage', 'simple', '{\"aria-label\":\"Slider\",\"alias-id\":\"\",\"alias-smoothscroll\":\"\",\"alias-slideswitch\":\"\",\"controlsTouch\":\"horizontal\",\"controlsScroll\":\"0\",\"controlsKeyboard\":\"1\",\"thumbnail\":\"\",\"align\":\"normal\",\"backgroundMode\":\"center\",\"animation\":\"horizontal\",\"animation-duration\":\"800\",\"background-animation\":\"\",\"background-animation-color\":\"333333ff\",\"background-animation-speed\":\"normal\",\"width\":\"1000\",\"height\":\"400\",\"margin\":\"0|*|0|*|0|*|0\",\"responsive-mode\":\"auto\",\"responsiveScaleDown\":\"1\",\"responsiveScaleUp\":\"1\",\"responsiveSliderHeightMin\":\"0\",\"responsiveSliderHeightMax\":\"3000\",\"responsiveSlideWidthMax\":\"3000\",\"autoplay\":\"1\",\"autoplayDuration\":\"2000\",\"autoplayStopClick\":\"0\",\"autoplayStopMouse\":\"0\",\"autoplayStopMedia\":\"1\",\"optimize\":\"1\",\"optimize-quality\":\"70\",\"optimize-background-image-custom\":\"0\",\"optimize-background-image-width\":\"800\",\"optimize-background-image-height\":\"600\",\"optimizeThumbnailWidth\":\"100\",\"optimizeThumbnailHeight\":\"60\",\"playWhenVisible\":\"1\",\"playWhenVisibleAt\":\"50\",\"dependency\":\"\",\"delay\":\"0\",\"is-delayed\":\"0\",\"overflow-hidden-page\":\"0\",\"clear-both\":\"0\",\"clear-both-after\":\"1\",\"media-query-hide-slider\":\"0\",\"media-query-under-over\":\"max-width\",\"media-query-width\":\"640\",\"responsiveFocusUser\":\"1\",\"custom-css-codes\":\"\",\"callbacks\":\"\",\"classes\":\"\",\"related-posts\":\"\",\"widgetarrow\":\"imageEmpty\",\"widget-arrow-display-hover\":\"0\",\"widget-arrow-previous\":\"$ss$\\/plugins\\/widgetarrow\\/image\\/image\\/previous\\/normal.svg\",\"widget-arrow-previous-color\":\"ffffff73\",\"widget-arrow-previous-hover\":\"0\",\"widget-arrow-previous-hover-color\":\"ffffffcc\",\"widget-arrow-style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiZmZmZmZmMDAiLCJvcGFjaXR5IjoxMDAsInBhZGRpbmciOiIwfCp8MHwqfDB8KnwwfCp8cHgiLCJib3hzaGFkb3ciOiIwfCp8MHwqfDB8KnwwfCp8MDAwMDAwZmYiLCJib3JkZXIiOiIwfCp8c29saWR8KnwwMDAwMDBmZiIsImJvcmRlcnJhZGl1cyI6IjAiLCJleHRyYSI6IiJ9LHsiZXh0cmEiOiIifV19\",\"widget-arrow-previous-position-mode\":\"simple\",\"widget-arrow-previous-position-area\":\"6\",\"widget-arrow-previous-position-stack\":\"1\",\"widget-arrow-previous-position-offset\":\"15\",\"widget-arrow-previous-position-horizontal\":\"left\",\"widget-arrow-previous-position-horizontal-position\":\"0\",\"widget-arrow-previous-position-horizontal-unit\":\"px\",\"widget-arrow-previous-position-vertical\":\"top\",\"widget-arrow-previous-position-vertical-position\":\"0\",\"widget-arrow-previous-position-vertical-unit\":\"px\",\"widget-arrow-next-position-mode\":\"simple\",\"widget-arrow-next-position-area\":\"7\",\"widget-arrow-next-position-stack\":\"1\",\"widget-arrow-next-position-offset\":\"15\",\"widget-arrow-next-position-horizontal\":\"left\",\"widget-arrow-next-position-horizontal-position\":\"0\",\"widget-arrow-next-position-horizontal-unit\":\"px\",\"widget-arrow-next-position-vertical\":\"top\",\"widget-arrow-next-position-vertical-position\":\"0\",\"widget-arrow-next-position-vertical-unit\":\"px\",\"widget-arrow-previous-alt\":\"previous arrow\",\"widget-arrow-next-alt\":\"next arrow\",\"widget-arrow-base64\":\"1\",\"widgetbullet\":\"disabled\",\"widget-bullet-display-hover\":\"0\",\"widget-bullet-thumbnail-show-image\":\"0\",\"widget-bullet-thumbnail-width\":\"100\",\"widget-bullet-thumbnail-height\":\"60\",\"widget-bullet-thumbnail-style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwODAiLCJwYWRkaW5nIjoiM3wqfDN8KnwzfCp8M3wqfHB4IiwiYm94c2hhZG93IjoiMHwqfDB8KnwwfCp8MHwqfDAwMDAwMGZmIiwiYm9yZGVyIjoiMHwqfHNvbGlkfCp8MDAwMDAwZmYiLCJib3JkZXJyYWRpdXMiOiIzIiwiZXh0cmEiOiJtYXJnaW46IDVweDtiYWNrZ3JvdW5kLXNpemU6Y292ZXI7In1dfQ==\",\"widget-bullet-thumbnail-side\":\"before\",\"widgetautoplay\":\"disabled\",\"widget-autoplay-display-hover\":\"1\",\"widgetbar\":\"disabled\",\"widget-bar-display-hover\":\"0\",\"widgetthumbnail\":\"disabled\",\"widget-thumbnail-display-hover\":\"0\",\"widget-thumbnail-width\":\"100\",\"widget-thumbnail-height\":\"60\",\"widgetshadow\":\"disabled\",\"widgets\":\"arrow\"}', '2019-09-19 02:19:40', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_smartslider3_sliders_xref`
--

CREATE TABLE `wp_nextend2_smartslider3_sliders_xref` (
  `group_id` int(11) NOT NULL,
  `slider_id` int(11) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_nextend2_smartslider3_sliders_xref`
--

INSERT INTO `wp_nextend2_smartslider3_sliders_xref` (`group_id`, `slider_id`, `ordering`) VALUES
(0, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_smartslider3_slides`
--

CREATE TABLE `wp_nextend2_smartslider3_slides` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `slider` int(11) NOT NULL,
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  `first` int(11) NOT NULL,
  `slide` longtext DEFAULT NULL,
  `description` text NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `generator_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_nextend2_smartslider3_slides`
--

INSERT INTO `wp_nextend2_smartslider3_slides` (`id`, `title`, `slider`, `publish_up`, `publish_down`, `published`, `first`, `slide`, `description`, `thumbnail`, `params`, `ordering`, `generator_id`) VALUES
(4, 'main_img01_no', 2, '2019-09-18 02:19:59', '2029-09-19 02:19:59', 1, 0, '[]', '', '$upload$/2019/09/main_img01_no.png', '{\"backgroundImage\":\"$upload$\\/2019\\/09\\/main_img01_no.png\",\"version\":\"3.3.22\"}', 0, 0),
(5, 'main_img02_no', 2, '2019-09-18 02:19:59', '2029-09-19 02:19:59', 1, 0, '[]', '', '$upload$/2019/09/main_img02_no.png', '{\"backgroundImage\":\"$upload$\\/2019\\/09\\/main_img02_no.png\",\"version\":\"3.3.22\"}', 1, 0),
(6, 'main_img03_no', 2, '2019-09-18 02:19:59', '2029-09-19 02:19:59', 1, 0, '[]', '', '$upload$/2019/09/main_img03_no.png', '{\"backgroundImage\":\"$upload$\\/2019\\/09\\/main_img03_no.png\",\"version\":\"3.3.22\"}', 2, 0),
(7, 'main_img04_no', 2, '2019-09-18 02:19:59', '2029-09-19 02:19:59', 1, 0, '[]', '', '$upload$/2019/09/main_img04_no.png', '{\"backgroundImage\":\"$upload$\\/2019\\/09\\/main_img04_no.png\",\"version\":\"3.3.22\"}', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost:8080/CamTu/Task007', 'yes'),
(2, 'home', 'http://localhost:8080/CamTu/Task007', 'yes'),
(3, 'blogname', 'Basic WordPress', 'yes'),
(4, 'blogdescription', 'Just another WordPress site', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'camtun80@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:89:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:1:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', 'a:3:{i:0;s:79:\"C:\\xampp\\htdocs\\CamTu\\Task007/wp-content/plugins/advanced-custom-fields/acf.php\";i:2;s:68:\"C:\\xampp\\htdocs\\CamTu\\Task007/wp-content/plugins/akismet/akismet.php\";i:3;s:0:\"\";}', 'no'),
(40, 'template', 'basetheme', 'yes'),
(41, 'stylesheet', 'basetheme', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:3:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}i:3;a:4:{s:5:\"title\";s:10:\"Categories\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'wp_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:75:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:19:\"access_masterslider\";b:1;s:20:\"publish_masterslider\";b:1;s:19:\"delete_masterslider\";b:1;s:19:\"create_masterslider\";b:1;s:19:\"export_masterslider\";b:1;s:22:\"duplicate_masterslider\";b:1;s:7:\"nextend\";b:1;s:14:\"nextend_config\";b:1;s:19:\"nextend_visual_edit\";b:1;s:21:\"nextend_visual_delete\";b:1;s:11:\"smartslider\";b:1;s:18:\"smartslider_config\";b:1;s:16:\"smartslider_edit\";b:1;s:18:\"smartslider_delete\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:48:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:19:\"access_masterslider\";b:1;s:20:\"publish_masterslider\";b:1;s:19:\"delete_masterslider\";b:1;s:19:\"create_masterslider\";b:1;s:19:\"export_masterslider\";b:1;s:22:\"duplicate_masterslider\";b:1;s:7:\"nextend\";b:1;s:14:\"nextend_config\";b:1;s:19:\"nextend_visual_edit\";b:1;s:21:\"nextend_visual_delete\";b:1;s:11:\"smartslider\";b:1;s:18:\"smartslider_config\";b:1;s:16:\"smartslider_edit\";b:1;s:18:\"smartslider_delete\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:3:{i:2;a:1:{s:5:\"title\";s:6:\"Search\";}i:3;a:1:{s:5:\"title\";s:6:\"Search\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:4:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}i:3;a:3:{s:5:\"title\";s:8:\"Archives\";s:5:\"count\";i:1;s:8:\"dropdown\";i:0;}i:4;a:3:{s:5:\"title\";s:8:\"Archives\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:4:{s:19:\"wp_inactive_widgets\";a:5:{i:0;s:14:\"recent-posts-2\";i:1;s:17:\"recent-comments-2\";i:2;s:10:\"archives-2\";i:3;s:12:\"categories-2\";i:4;s:6:\"meta-2\";}s:12:\"main-sidebar\";a:3:{i:0;s:8:\"search-3\";i:1;s:12:\"categories-3\";i:2;s:10:\"archives-4\";}s:19:\"widget-product-cart\";a:2:{i:0;s:8:\"search-2\";i:1;s:10:\"archives-3\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'cron', 'a:6:{i:1569230163;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1569255363;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1569298562;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1569298579;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1569298582;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(103, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'recovery_keys', 'a:1:{s:22:\"i5DyCJ75ylznOVx5aepp77\";a:2:{s:10:\"hashed_key\";s:34:\"$P$Beqm8vt7NzFZfFqGkgJ2giGALxf87w.\";s:10:\"created_at\";i:1569209841;}}', 'yes'),
(114, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:2:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:7:\"upgrade\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.3-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2.3-partial-2.zip\";s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.3\";s:7:\"version\";s:5:\"5.2.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:5:\"5.2.2\";}i:1;O:8:\"stdClass\":11:{s:8:\"response\";s:10:\"autoupdate\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.2.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.2.3-new-bundled.zip\";s:7:\"partial\";s:69:\"https://downloads.wordpress.org/release/wordpress-5.2.3-partial-2.zip\";s:8:\"rollback\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.2.3-rollback-2.zip\";}s:7:\"current\";s:5:\"5.2.3\";s:7:\"version\";s:5:\"5.2.3\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:5:\"5.2.2\";s:9:\"new_files\";s:0:\"\";}}s:12:\"last_checked\";i:1569212400;s:15:\"version_checked\";s:5:\"5.2.2\";s:12:\"translations\";a:0:{}}', 'no'),
(115, 'theme_mods_twentynineteen', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1568682354;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}s:18:\"nav_menu_locations\";a:0:{}}', 'yes'),
(128, 'can_compress_scripts', '1', 'no'),
(139, 'current_theme', 'Basic wordpress', 'yes'),
(140, 'theme_mods_basetheme', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:2:{s:12:\"primary-menu\";i:7;s:11:\"footer-menu\";i:8;}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1568684534;s:4:\"data\";a:1:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(141, 'theme_switched', '', 'yes'),
(149, 'slider_cat_children', 'a:0:{}', 'yes'),
(155, 'recently_activated', 'a:11:{s:30:\"advanced-custom-fields/acf.php\";i:1569213785;s:33:\"smart-slider-3/smart-slider-3.php\";i:1569208821;s:47:\"simple-yearly-archive/simple-yearly-archive.php\";i:1568868368;s:32:\"anual-archive/annual_archive.php\";i:1568868006;s:31:\"master-slider/master-slider.php\";i:1568859404;s:23:\"ml-slider/ml-slider.php\";i:1568857590;s:29:\"slider-by-supsystic/index.php\";i:1568856152;s:22:\"ez-slider/ez_slide.php\";i:1568799182;s:27:\"os-bxslider/os-bxslider.php\";i:1568799001;s:45:\"bxslider-integration/bxslider-integration.php\";i:1568797231;s:40:\"global-s-h-bxslider/bx-slider-for-wp.php\";i:1568796165;}', 'yes'),
(188, 'theme_mods_twentysixteen', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1568682459;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(191, 'theme_mods_twentyseventeen', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1568682372;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}}', 'yes'),
(193, '_transient_twentyseventeen_categories', '1', 'yes'),
(208, 'theme_mods_lyrical', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1568684390;s:4:\"data\";a:7:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-1\";a:0:{}s:9:\"sidebar-2\";a:0:{}s:8:\"footer-1\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:4:\"hero\";a:0:{}}}}', 'yes'),
(209, 'widget_primer-hero-text', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(213, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1569212399;s:7:\"checked\";a:1:{s:9:\"basetheme\";s:3:\"1.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(215, 'theme_mods_cruzy', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1568685507;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-1\";a:0:{}}}}', 'yes'),
(216, 'cruzy_admin_notice_welcome', '1', 'yes'),
(222, '_transient_cruzy_categories', '2', 'yes'),
(252, 'category_children', 'a:0:{}', 'yes'),
(253, 'recovery_mode_email_last_sent', '1569209840', 'yes'),
(303, 'acf_version', '5.8.2', 'yes'),
(321, 'bxsg_options', 'a:21:{s:11:\"include_css\";b:1;s:10:\"include_js\";b:1;s:10:\"transition\";s:10:\"horizontal\";s:28:\"gs_replace_default_galleries\";b:1;s:19:\"gs_exclude_featured\";b:1;s:16:\"gs_hide_carousel\";b:1;s:18:\"gs_adaptive_height\";b:1;s:13:\"gs_auto_start\";b:1;s:8:\"gs_speed\";i:500;s:11:\"gs_duration\";i:2000;s:16:\"gs_extra_options\";s:6:\"op[op;\";s:23:\"gs_carousel_thumb_width\";i:60;s:24:\"gs_carousel_thumb_margin\";i:5;s:22:\"gs_carousel_min_thumbs\";i:4;s:22:\"gs_carousel_max_thumbs\";i:10;s:23:\"gs_carousel_thumbs_move\";i:0;s:18:\"sl_adaptive_height\";b:1;s:13:\"sl_auto_start\";b:1;s:8:\"sl_speed\";i:500;s:11:\"sl_duration\";i:2000;s:16:\"sl_extra_options\";s:10:\"\'?\r\n\\ư\\po\";}', 'yes'),
(348, 'ss_send_stats', '1', 'yes'),
(349, 'widget_sslwidget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(350, 'rs_reviewNotice', 'a:2:{s:4:\"time\";i:1569407336;s:8:\"is_shown\";b:0;}', 'yes'),
(363, 'widget_metaslider_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(364, 'ms_hide_all_ads_until', '1570066250', 'yes'),
(365, 'metaslider_systemcheck', 'a:2:{s:16:\"wordPressVersion\";b:0;s:12:\"imageLibrary\";b:0;}', 'no'),
(366, 'ml-slider_children', 'a:0:{}', 'yes'),
(367, 'metaslider_tour_cancelled_on', 'add-slide', 'yes'),
(372, 'masterslider_db_version', '1.03', 'yes'),
(373, 'masterslider_capabilities_added', '1', 'yes'),
(374, 'master-slider_ab_pro_feature_panel_content_type', '1', 'yes'),
(375, 'master-slider_ab_pro_feature_setting_content_type', '1', 'yes'),
(376, 'widget_master-slider-main-widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(377, 'msp_general_setting', '', 'yes'),
(378, 'msp_advanced', '', 'yes'),
(379, 'upgrade_to_pro', '', 'yes'),
(380, 'masterslider_lite_plugin_version', '3.5.3', 'yes'),
(398, 'n2_ss3_version', '3.3.22r4878', 'yes'),
(399, 'widget_smartslider3', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(431, 'widget_annual_archive_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(434, 'sya_dateformat', 'Y', 'yes'),
(435, 'sya_datetitleseperator', '', 'yes'),
(436, 'sya_linkyears', '1', 'yes'),
(437, 'sya_collapseyears', '1', 'yes'),
(438, 'sya_prepend', '', 'yes'),
(439, 'sya_append', '', 'yes'),
(440, 'sya_excerpt_indent', '', 'yes'),
(441, 'sya_excerpt_maxchars', '', 'yes'),
(442, 'sya_showyearoverview', '1', 'yes'),
(443, 'sya_dateformatchanged2012', '1', 'yes'),
(444, 'sya_dateformatchanged2015', '1', 'yes'),
(445, 'sya_postthumbnail_size', '', 'yes'),
(454, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(456, '_transient_timeout_plugin_slugs', '1569300186', 'no'),
(457, '_transient_plugin_slugs', 'a:4:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:34:\"advanced-custom-fields-pro/acf.php\";i:2;s:19:\"akismet/akismet.php\";i:3;s:9:\"hello.php\";}', 'no'),
(458, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1569218372', 'no'),
(459, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:\"stdClass\":100:{s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";i:4617;}s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";i:3641;}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";i:2643;}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";i:2514;}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";i:1936;}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";i:1759;}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";i:1748;}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";i:1472;}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";i:1451;}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";i:1447;}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";i:1437;}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";i:1384;}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";i:1358;}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";i:1295;}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";i:1157;}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";i:1136;}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";i:1105;}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";i:1076;}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";i:1042;}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";i:951;}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";i:862;}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";i:848;}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";i:845;}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";i:814;}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";i:753;}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";i:746;}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";i:735;}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";i:734;}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";i:723;}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";i:706;}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";i:703;}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";i:688;}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";i:681;}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";i:672;}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";i:671;}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";i:657;}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";i:636;}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";i:631;}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";i:624;}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"ajax\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";i:624;}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";i:606;}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";i:599;}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";i:577;}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"css\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";i:573;}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";i:564;}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";i:563;}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";i:557;}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";i:544;}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";i:537;}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";i:534;}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";i:531;}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";i:525;}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";i:520;}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";i:517;}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";i:516;}s:7:\"payment\";a:3:{s:4:\"name\";s:7:\"payment\";s:4:\"slug\";s:7:\"payment\";s:5:\"count\";i:513;}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";i:505;}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";i:484;}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";i:479;}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";i:479;}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";i:478;}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";i:477;}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";i:459;}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";i:458;}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";i:450;}s:15:\"payment-gateway\";a:3:{s:4:\"name\";s:15:\"payment gateway\";s:4:\"slug\";s:15:\"payment-gateway\";s:5:\"count\";i:441;}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";i:431;}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";i:424;}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";i:418;}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";i:417;}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";i:416;}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";i:413;}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";i:409;}s:4:\"chat\";a:3:{s:4:\"name\";s:4:\"chat\";s:4:\"slug\";s:4:\"chat\";s:5:\"count\";i:402;}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";i:401;}s:9:\"gutenberg\";a:3:{s:4:\"name\";s:9:\"gutenberg\";s:4:\"slug\";s:9:\"gutenberg\";s:5:\"count\";i:397;}s:5:\"popup\";a:3:{s:4:\"name\";s:5:\"popup\";s:4:\"slug\";s:5:\"popup\";s:5:\"count\";i:396;}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";i:389;}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";i:387;}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"news\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";i:386;}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";i:382;}s:5:\"forms\";a:3:{s:4:\"name\";s:5:\"forms\";s:4:\"slug\";s:5:\"forms\";s:5:\"count\";i:380;}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";i:369;}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";i:363;}s:8:\"redirect\";a:3:{s:4:\"name\";s:8:\"redirect\";s:4:\"slug\";s:8:\"redirect\";s:5:\"count\";i:363;}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";i:359;}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";i:358;}s:14:\"contact-form-7\";a:3:{s:4:\"name\";s:14:\"contact form 7\";s:4:\"slug\";s:14:\"contact-form-7\";s:5:\"count\";i:356;}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";i:352;}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";i:348;}s:11:\"performance\";a:3:{s:4:\"name\";s:11:\"performance\";s:4:\"slug\";s:11:\"performance\";s:5:\"count\";i:346;}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";i:336;}s:16:\"custom-post-type\";a:3:{s:4:\"name\";s:16:\"custom post type\";s:4:\"slug\";s:16:\"custom-post-type\";s:5:\"count\";i:326;}s:8:\"tracking\";a:3:{s:4:\"name\";s:8:\"tracking\";s:4:\"slug\";s:8:\"tracking\";s:5:\"count\";i:326;}s:11:\"advertising\";a:3:{s:4:\"name\";s:11:\"advertising\";s:4:\"slug\";s:11:\"advertising\";s:5:\"count\";i:326;}s:16:\"google-analytics\";a:3:{s:4:\"name\";s:16:\"google analytics\";s:4:\"slug\";s:16:\"google-analytics\";s:5:\"count\";i:323;}s:6:\"simple\";a:3:{s:4:\"name\";s:6:\"simple\";s:4:\"slug\";s:6:\"simple\";s:5:\"count\";i:321;}s:4:\"html\";a:3:{s:4:\"name\";s:4:\"html\";s:4:\"slug\";s:4:\"html\";s:5:\"count\";i:318;}s:6:\"author\";a:3:{s:4:\"name\";s:6:\"author\";s:4:\"slug\";s:6:\"author\";s:5:\"count\";i:314;}s:3:\"map\";a:3:{s:4:\"name\";s:3:\"map\";s:4:\"slug\";s:3:\"map\";s:5:\"count\";i:312;}}', 'no'),
(461, '_site_transient_timeout_theme_roots', '1569212434', 'no'),
(462, '_site_transient_theme_roots', 'a:1:{s:9:\"basetheme\";s:7:\"/themes\";}', 'no'),
(465, '_transient_timeout_acf_plugin_updates', '1569383446', 'no'),
(466, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"5.8.4\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:3:\"5.2\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.8.2\";}}', 'no'),
(467, '_transient_timeout_acf_plugin_info_pro', '1569297067', 'no'),
(468, '_transient_acf_plugin_info_pro', 'a:18:{s:4:\"name\";s:26:\"Advanced Custom Fields PRO\";s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:7:\"version\";s:5:\"5.8.4\";s:8:\"homepage\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"author\";s:64:\"<a href=\"https://www.advancedcustomfields.com\">Elliot Condon</a>\";s:12:\"contributors\";a:1:{s:12:\"elliotcondon\";a:3:{s:7:\"profile\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"avatar\";s:81:\"https://secure.gravatar.com/avatar/c296f449f92233e8d1102ff01c9bc71e?s=96&d=mm&r=g\";s:12:\"display_name\";s:22:\"Advanced Custom Fields\";}}s:8:\"requires\";s:5:\"4.7.0\";s:12:\"requires_php\";s:3:\"5.4\";s:6:\"tested\";s:3:\"5.2\";s:5:\"added\";s:10:\"2014-07-11\";s:12:\"last_updated\";s:10:\"2019-09-04\";s:11:\"description\";s:1349:\"<p>Use the Advanced Custom Fields plugin to take full control of your WordPress edit screens & custom field data.</p>\n\n<p><strong>Add fields on demand.</strong> Our field builder allows you to quickly and easily add fields to WP edit screens with only the click of a few buttons!</p>\n\n<p><strong>Add them anywhere.</strong> Fields can be added all over WP including posts, users, taxonomy terms, media, comments and even custom options pages!</p>\n\n<p><strong>Show them everywhere.</strong> Load and display your custom field values in any theme template file with our hassle free developer friendly functions!</p>\n\n<h4>Features</h4>\n<ul>\n<li>Simple & Intuitive</li>\n<li>Powerful Functions</li>\n<li>Over 30 Field Types</li>\n<li>Extensive Documentation</li>\n<li>Millions of Users</li>\n</ul>\n\n<h4>Links</h4>\n<ul>\n<li><a href=\"https://www.advancedcustomfields.com\">Website</a></li>\n<li><a href=\"https://www.advancedcustomfields.com/resources/\">Documentation</a></li>\n<li><a href=\"https://support.advancedcustomfields.com\">Support</a></li>\n<li><a href=\"https://www.advancedcustomfields.com/pro/\">ACF PRO</a></li>\n</ul>\n\n<h4>PRO</h4>\n<p>The Advanced Custom Fields plugin is also available in a professional version which includes more fields, more functionality, and more flexibility! <a href=\"https://www.advancedcustomfields.com/pro/\">Learn more</a></p>\";s:12:\"installation\";s:503:\"<p>From your WordPress dashboard</p>\n\n<ol>\n<li><strong>Visit</strong> Plugins > Add New</li>\n<li><strong>Search</strong> for \"Advanced Custom Fields\"</li>\n<li><strong>Activate</strong> Advanced Custom Fields from your Plugins page</li>\n<li><strong>Click</strong> on the new menu item \"Custom Fields\" and create your first Custom Field Group!</li>\n<li><strong>Read</strong> the documentation to <a href=\"https://www.advancedcustomfields.com/resources/getting-started-with-acf/\">get started</a></li>\n</ol>\";s:9:\"changelog\";s:1849:\"<h4>5.8.4</h4>\n<p><em>Release Date - 3 September 2019</em></p>\n\n<ul>\n<li>New - Optimized Relationship field by delaying AJAX call until UI is visible.</li>\n<li>Fix - Fixed bug incorrectly escaping HTML in the Link field title.</li>\n<li>Fix - Fixed bug showing Discussion and Comment metaboxes for newly imported field groups.</li>\n<li>Fix - Fixed PHP warning when loading meta from Post 0.</li>\n<li>Dev - Ensure Checkbox field value is an array even when empty.</li>\n<li>Dev - Added new <code>ACF_MAJOR_VERSION</code> constant.</li>\n</ul>\n\n<h4>5.8.3</h4>\n<p><em>Release Date - 7 August 2019</em></p>\n\n<ul>\n<li>Tweak - Changed Options Page location rules to show \"page_title\" instead of \"menu_title\".</li>\n<li>Fix - Fixed bug causing Textarea field to incorrectly validate maxlength.</li>\n<li>Fix - Fixed bug allowing Range field values outside of the min and max settings.</li>\n<li>Fix - Fixed bug in block RegExp causing some blocks to miss the \"acf/pre_save_block\" filter.</li>\n<li>Dev - Added <code>$block_type</code> parameter to block settings \"enqueue_assets\" callback.</li>\n<li>i18n - Added French Canadian language thanks to Bérenger Zyla.</li>\n<li>i18n - Updated French language thanks to Bérenger Zyla.</li>\n</ul>\n\n<h4>5.8.2</h4>\n<p><em>Release Date - 15 July 2019</em></p>\n\n<ul>\n<li>Fix - Fixed bug where validation did not prevent new user registration.</li>\n<li>Fix - Fixed bug causing some \"reordered\" metaboxes to not appear in the Gutenberg editor.</li>\n<li>Fix - Fixed bug causing WYSIWYG field with delayed initialization to appear blank.</li>\n<li>Fix - Fixed bug when editing a post and adding a new tag did not refresh metaboxes.</li>\n<li>Dev - Added missing <code>$value</code> parameter in \"acf/pre_format_value\" filter.</li>\n</ul>\n\n<p><a href=\"https://www.advancedcustomfields.com/changelog/\">View the full changelog</a></p>\";s:14:\"upgrade_notice\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}s:8:\"versions\";a:101:{i:0;s:5:\"5.8.3\";i:1;s:5:\"5.8.2\";i:2;s:5:\"5.8.1\";i:3;s:13:\"5.8.0-beta4.1\";i:4;s:11:\"5.8.0-beta4\";i:5;s:11:\"5.8.0-beta3\";i:6;s:11:\"5.8.0-beta2\";i:7;s:11:\"5.8.0-beta1\";i:8;s:9:\"5.8.0-RC2\";i:9;s:9:\"5.8.0-RC1\";i:10;s:5:\"5.8.0\";i:11;s:5:\"5.7.9\";i:12;s:5:\"5.7.8\";i:13;s:5:\"5.7.7\";i:14;s:5:\"5.7.6\";i:15;s:5:\"5.7.5\";i:16;s:5:\"5.7.4\";i:17;s:5:\"5.7.3\";i:18;s:5:\"5.7.2\";i:19;s:6:\"5.7.13\";i:20;s:6:\"5.7.12\";i:21;s:6:\"5.7.10\";i:22;s:5:\"5.7.1\";i:23;s:5:\"5.7.0\";i:24;s:5:\"5.6.9\";i:25;s:5:\"5.6.8\";i:26;s:5:\"5.6.7\";i:27;s:5:\"5.6.6\";i:28;s:5:\"5.6.5\";i:29;s:5:\"5.6.4\";i:30;s:5:\"5.6.3\";i:31;s:5:\"5.6.2\";i:32;s:6:\"5.6.10\";i:33;s:5:\"5.6.1\";i:34;s:11:\"5.6.0-beta2\";i:35;s:11:\"5.6.0-beta1\";i:36;s:9:\"5.6.0-RC2\";i:37;s:9:\"5.6.0-RC1\";i:38;s:5:\"5.6.0\";i:39;s:5:\"5.5.9\";i:40;s:5:\"5.5.7\";i:41;s:5:\"5.5.5\";i:42;s:5:\"5.5.3\";i:43;s:5:\"5.5.2\";i:44;s:6:\"5.5.14\";i:45;s:6:\"5.5.13\";i:46;s:6:\"5.5.12\";i:47;s:6:\"5.5.11\";i:48;s:6:\"5.5.10\";i:49;s:5:\"5.5.1\";i:50;s:5:\"5.5.0\";i:51;s:5:\"5.4.8\";i:52;s:5:\"5.4.7\";i:53;s:5:\"5.4.6\";i:54;s:5:\"5.4.5\";i:55;s:5:\"5.4.4\";i:56;s:5:\"5.4.3\";i:57;s:5:\"5.4.2\";i:58;s:5:\"5.4.1\";i:59;s:5:\"5.4.0\";i:60;s:5:\"5.3.9\";i:61;s:5:\"5.3.8\";i:62;s:5:\"5.3.7\";i:63;s:5:\"5.3.6\";i:64;s:5:\"5.3.5\";i:65;s:5:\"5.3.4\";i:66;s:5:\"5.3.3\";i:67;s:5:\"5.3.2\";i:68;s:6:\"5.3.10\";i:69;s:5:\"5.3.1\";i:70;s:5:\"5.3.0\";i:71;s:5:\"5.2.9\";i:72;s:5:\"5.2.8\";i:73;s:5:\"5.2.7\";i:74;s:5:\"5.2.6\";i:75;s:5:\"5.2.5\";i:76;s:5:\"5.2.4\";i:77;s:5:\"5.2.3\";i:78;s:5:\"5.2.2\";i:79;s:5:\"5.2.1\";i:80;s:5:\"5.2.0\";i:81;s:5:\"5.1.9\";i:82;s:5:\"5.1.8\";i:83;s:5:\"5.1.7\";i:84;s:5:\"5.1.6\";i:85;s:5:\"5.1.5\";i:86;s:5:\"5.1.4\";i:87;s:5:\"5.1.3\";i:88;s:5:\"5.1.2\";i:89;s:5:\"5.1.1\";i:90;s:5:\"5.1.0\";i:91;s:5:\"5.0.9\";i:92;s:5:\"5.0.8\";i:93;s:5:\"5.0.7\";i:94;s:5:\"5.0.6\";i:95;s:5:\"5.0.5\";i:96;s:5:\"5.0.4\";i:97;s:5:\"5.0.3\";i:98;s:5:\"5.0.2\";i:99;s:5:\"5.0.1\";i:100;s:5:\"5.0.0\";}}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(471, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1569212420;s:7:\"checked\";a:4:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.8.3\";s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.8.2\";s:19:\"akismet/akismet.php\";s:5:\"4.1.2\";s:9:\"hello.php\";s:5:\"1.7.2\";}s:8:\"response\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"5.8.4\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:3:\"5.2\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.8.3\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.8.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.2\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:5:\"1.7.2\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/hello-dolly.1.7.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(472, 'options_adress', '', 'no'),
(473, '_options_adress', 'field_5d88470577019', 'no'),
(474, 'options_time', '', 'no'),
(475, '_options_time', 'field_5d8847377701a', 'no'),
(476, 'options_tel', '', 'no'),
(477, '_options_tel', 'field_5d8847457701b', 'no'),
(478, 'options_fax', '', 'no'),
(479, '_options_fax', 'field_5d8847547701c', 'no'),
(480, 'options_email', '', 'no'),
(481, '_options_email', 'field_5d88475f7701d', 'no'),
(482, 'options_image_map', '', 'no'),
(483, '_options_image_map', 'field_5d88476a7701e', 'no'),
(484, 'options_title_map_0_image_map', '50', 'no'),
(485, '_options_title_map_0_image_map', 'field_5d884911acb7e', 'no'),
(486, 'options_title_map_1_image_map', '46', 'no'),
(487, '_options_title_map_1_image_map', 'field_5d884911acb7e', 'no'),
(488, 'options_title_map', '2', 'no'),
(489, '_options_title_map', 'field_5d8848f5acb7d', 'no'),
(490, 'options_map_0_title_map', '本店', 'no'),
(491, '_options_map_0_title_map', 'field_5d8849ba7fdf2', 'no'),
(492, 'options_map_0_adresss', '〒210-0005　川崎市川崎区東田町8 パレール三井ビル8階', 'no'),
(493, '_options_map_0_adresss', 'field_5d8849d67fdf3', 'no'),
(494, 'options_map_0_time', 'JR川崎駅東口より徒歩7分　京急川崎駅より徒歩5分', 'no'),
(495, '_options_map_0_time', 'field_5d8849e87fdf4', 'no'),
(496, 'options_map_0_tel', 'tel : 044-233-2811', 'no'),
(497, '_options_map_0_tel', 'field_5d884a037fdf5', 'no'),
(498, 'options_map_0_fax', 'fax : 044-233-0818', 'no'),
(499, '_options_map_0_fax', 'field_5d884a0e7fdf6', 'no'),
(500, 'options_map_0_email', 'mail : info@wms.or.jp', 'no'),
(501, '_options_map_0_email', 'field_5d884a1a7fdf7', 'no'),
(502, 'options_map_0_image_map', '46', 'no'),
(503, '_options_map_0_image_map', 'field_5d884a257fdf8', 'no'),
(504, 'options_map_1_title_map', '相模原支店', 'no'),
(505, '_options_map_1_title_map', 'field_5d8849ba7fdf2', 'no'),
(506, 'options_map_1_adresss', '〒252-0231　相模原市中央区相模原3-8-25 第3JSビル7階', 'no'),
(507, '_options_map_1_adresss', 'field_5d8849d67fdf3', 'no'),
(508, 'options_map_1_time', 'JR横浜線相模原駅より徒歩2分', 'no'),
(509, '_options_map_1_time', 'field_5d8849e87fdf4', 'no'),
(510, 'options_map_1_tel', 'tel : 042-704-9581', 'no'),
(511, '_options_map_1_tel', 'field_5d884a037fdf5', 'no'),
(512, 'options_map_1_fax', 'fax : 042-704-9582', 'no'),
(513, '_options_map_1_fax', 'field_5d884a0e7fdf6', 'no'),
(514, 'options_map_1_email', '', 'no'),
(515, '_options_map_1_email', 'field_5d884a1a7fdf7', 'no'),
(516, 'options_map_1_image_map', '50', 'no'),
(517, '_options_map_1_image_map', 'field_5d884a257fdf8', 'no'),
(518, 'options_map', '2', 'no'),
(519, '_options_map', 'field_5d8849a47fdf1', 'no'),
(522, 'options_bxslider_0_image_slide', '6', 'no'),
(523, '_options_bxslider_0_image_slide', 'field_5d8860e1dc8f6', 'no'),
(524, 'options_bxslider_0_link_imge_slide', '', 'no'),
(525, '_options_bxslider_0_link_imge_slide', 'field_5d886112dc8f7', 'no'),
(526, 'options_bxslider_1_image_slide', '13', 'no'),
(527, '_options_bxslider_1_image_slide', 'field_5d8860e1dc8f6', 'no'),
(528, 'options_bxslider_1_link_imge_slide', '', 'no'),
(529, '_options_bxslider_1_link_imge_slide', 'field_5d886112dc8f7', 'no'),
(530, 'options_bxslider_2_image_slide', '14', 'no'),
(531, '_options_bxslider_2_image_slide', 'field_5d8860e1dc8f6', 'no'),
(532, 'options_bxslider_2_link_imge_slide', '', 'no'),
(533, '_options_bxslider_2_link_imge_slide', 'field_5d886112dc8f7', 'no'),
(534, 'options_bxslider_3_image_slide', '15', 'no'),
(535, '_options_bxslider_3_image_slide', 'field_5d8860e1dc8f6', 'no'),
(536, 'options_bxslider_3_link_imge_slide', '', 'no'),
(537, '_options_bxslider_3_link_imge_slide', 'field_5d886112dc8f7', 'no'),
(538, 'options_bxslider', '4', 'no'),
(539, '_options_bxslider', 'field_5d8860c8dc8f5', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_edit_lock', '1568619996:1'),
(4, 6, '_wp_attached_file', '2019/09/main_img01_no.png'),
(5, 6, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:400;s:4:\"file\";s:25:\"2019/09/main_img01_no.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"main_img01_no-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"main_img01_no-300x120.png\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"main_img01_no-768x307.png\";s:5:\"width\";i:768;s:6:\"height\";i:307;s:9:\"mime-type\";s:9:\"image/png\";}s:27:\"meta-slider-resized-700x300\";a:4:{s:4:\"file\";s:25:\"main_img01_no-700x300.png\";s:5:\"width\";i:700;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:27:\"meta-slider-resized-620x266\";a:4:{s:4:\"file\";s:25:\"main_img01_no-620x266.png\";s:5:\"width\";i:620;s:6:\"height\";i:266;s:9:\"mime-type\";s:9:\"image/png\";}s:27:\"meta-slider-resized-400x171\";a:4:{s:4:\"file\";s:25:\"main_img01_no-400x171.png\";s:5:\"width\";i:400;s:6:\"height\";i:171;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(8, 5, '_thumbnail_id', '6'),
(10, 5, '_wp_trash_meta_status', 'publish'),
(11, 5, '_wp_trash_meta_time', '1568620152'),
(12, 5, '_wp_desired_post_slug', 'slide1'),
(13, 1, '_wp_trash_meta_status', 'publish'),
(14, 1, '_wp_trash_meta_time', '1568620153'),
(15, 1, '_wp_desired_post_slug', 'hello-world'),
(16, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}'),
(17, 10, '_edit_last', '1'),
(18, 10, '_edit_lock', '1568624318:1'),
(19, 10, '_thumbnail_id', '6'),
(20, 11, '_edit_last', '1'),
(21, 11, '_edit_lock', '1568620770:1'),
(22, 11, '_thumbnail_id', '6'),
(23, 11, 'url', ''),
(24, 12, '_edit_last', '1'),
(25, 12, '_edit_lock', '1568620916:1'),
(26, 13, '_wp_attached_file', '2019/09/main_img02_no.png'),
(27, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:400;s:4:\"file\";s:25:\"2019/09/main_img02_no.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"main_img02_no-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"main_img02_no-300x120.png\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"main_img02_no-768x307.png\";s:5:\"width\";i:768;s:6:\"height\";i:307;s:9:\"mime-type\";s:9:\"image/png\";}s:27:\"meta-slider-resized-700x300\";a:4:{s:4:\"file\";s:25:\"main_img02_no-700x300.png\";s:5:\"width\";i:700;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:27:\"meta-slider-resized-620x266\";a:4:{s:4:\"file\";s:25:\"main_img02_no-620x266.png\";s:5:\"width\";i:620;s:6:\"height\";i:266;s:9:\"mime-type\";s:9:\"image/png\";}s:27:\"meta-slider-resized-400x171\";a:4:{s:4:\"file\";s:25:\"main_img02_no-400x171.png\";s:5:\"width\";i:400;s:6:\"height\";i:171;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(28, 14, '_wp_attached_file', '2019/09/main_img03_no.png'),
(29, 14, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:400;s:4:\"file\";s:25:\"2019/09/main_img03_no.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"main_img03_no-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"main_img03_no-300x120.png\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"main_img03_no-768x307.png\";s:5:\"width\";i:768;s:6:\"height\";i:307;s:9:\"mime-type\";s:9:\"image/png\";}s:27:\"meta-slider-resized-700x300\";a:4:{s:4:\"file\";s:25:\"main_img03_no-700x300.png\";s:5:\"width\";i:700;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:27:\"meta-slider-resized-620x266\";a:4:{s:4:\"file\";s:25:\"main_img03_no-620x266.png\";s:5:\"width\";i:620;s:6:\"height\";i:266;s:9:\"mime-type\";s:9:\"image/png\";}s:27:\"meta-slider-resized-400x171\";a:4:{s:4:\"file\";s:25:\"main_img03_no-400x171.png\";s:5:\"width\";i:400;s:6:\"height\";i:171;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(30, 15, '_wp_attached_file', '2019/09/main_img04_no.png'),
(31, 15, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1000;s:6:\"height\";i:400;s:4:\"file\";s:25:\"2019/09/main_img04_no.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"main_img04_no-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"main_img04_no-300x120.png\";s:5:\"width\";i:300;s:6:\"height\";i:120;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"main_img04_no-768x307.png\";s:5:\"width\";i:768;s:6:\"height\";i:307;s:9:\"mime-type\";s:9:\"image/png\";}s:27:\"meta-slider-resized-700x300\";a:4:{s:4:\"file\";s:25:\"main_img04_no-700x300.png\";s:5:\"width\";i:700;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:27:\"meta-slider-resized-620x266\";a:4:{s:4:\"file\";s:25:\"main_img04_no-620x266.png\";s:5:\"width\";i:620;s:6:\"height\";i:266;s:9:\"mime-type\";s:9:\"image/png\";}s:27:\"meta-slider-resized-400x171\";a:4:{s:4:\"file\";s:25:\"main_img04_no-400x171.png\";s:5:\"width\";i:400;s:6:\"height\";i:171;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(32, 12, '_thumbnail_id', '13'),
(33, 12, 'url', ''),
(35, 17, '_edit_last', '1'),
(36, 17, '_edit_lock', '1568620942:1'),
(37, 17, '_thumbnail_id', '14'),
(38, 17, 'url', ''),
(39, 18, '_edit_last', '1'),
(40, 18, '_edit_lock', '1568621421:1'),
(41, 18, '_thumbnail_id', '15'),
(42, 18, 'url', ''),
(43, 20, '_edit_last', '1'),
(44, 20, '_edit_lock', '1568621534:1'),
(45, 20, '_thumbnail_id', '13'),
(46, 20, 'url', ''),
(47, 21, '_edit_last', '1'),
(48, 21, '_edit_lock', '1568621553:1'),
(49, 21, '_thumbnail_id', '14'),
(50, 21, 'url', ''),
(51, 22, '_edit_last', '1'),
(52, 22, '_edit_lock', '1568626693:1'),
(53, 22, '_thumbnail_id', '15'),
(54, 22, 'url', ''),
(56, 24, '_edit_lock', '1568628463:1'),
(61, 24, '_wp_old_date', '2019-09-16'),
(62, 24, '_wp_trash_meta_status', 'publish'),
(63, 24, '_wp_trash_meta_time', '1568682436'),
(64, 24, '_wp_desired_post_slug', '%e5%83%8d%e3%81%8d%e6%96%b9%e6%94%b9%e9%9d%a9%e3%81%a8%e7%ae%a1%e7%90%86%e8%80%85'),
(65, 26, '_edit_lock', '1568684449:1'),
(68, 26, '_thumbnail_id', '15'),
(69, 28, '_edit_lock', '1568684468:1'),
(70, 29, '_edit_lock', '1568685344:1'),
(73, 31, '_edit_lock', '1568687353:1'),
(74, 29, '_wp_trash_meta_status', 'publish'),
(75, 29, '_wp_trash_meta_time', '1568687549'),
(76, 29, '_wp_desired_post_slug', 'mdvnlko'),
(77, 32, '_edit_lock', '1568692373:1'),
(80, 32, '_wp_trash_meta_status', 'publish'),
(81, 32, '_wp_trash_meta_time', '1568692525'),
(82, 32, '_wp_desired_post_slug', 'ghjftyu'),
(83, 26, '_wp_trash_meta_status', 'publish'),
(84, 26, '_wp_trash_meta_time', '1568692525'),
(85, 26, '_wp_desired_post_slug', 'ljlhj'),
(86, 34, '_edit_lock', '1568715706:1'),
(91, 34, '_wp_old_date', '2019-09-17'),
(92, 36, '_edit_lock', '1568715452:1'),
(97, 36, '_wp_old_date', '2019-09-17'),
(98, 38, '_edit_lock', '1568715427:1'),
(103, 38, '_wp_old_date', '2019-09-17'),
(104, 40, '_edit_lock', '1568715385:1'),
(109, 40, '_wp_old_date', '2019-09-17'),
(110, 42, '_edit_lock', '1568692839:1'),
(111, 43, '_edit_lock', '1568715349:1'),
(116, 43, '_wp_old_date', '2019-09-17'),
(117, 45, '_edit_last', '1'),
(118, 45, '_edit_lock', '1568702814:1'),
(119, 46, '_wp_attached_file', '2019/09/map_01.png'),
(120, 46, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:290;s:4:\"file\";s:18:\"2019/09/map_01.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"map_01-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"map_01-300x174.png\";s:5:\"width\";i:300;s:6:\"height\";i:174;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(121, 45, '_thumbnail_id', '46'),
(122, 49, '_edit_last', '1'),
(123, 49, '_edit_lock', '1568705818:1'),
(124, 50, '_wp_attached_file', '2019/09/map_02.png'),
(125, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:290;s:4:\"file\";s:18:\"2019/09/map_02.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"map_02-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"map_02-300x174.png\";s:5:\"width\";i:300;s:6:\"height\";i:174;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(126, 49, '_thumbnail_id', '50'),
(127, 51, '_edit_lock', '1568710098:1'),
(128, 54, '_edit_lock', '1569205004:1'),
(129, 56, '_edit_lock', '1568710538:1'),
(130, 57, '_edit_lock', '1568714990:1'),
(133, 61, '_wp_attached_file', '2019/09/dummyimg.png'),
(134, 61, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:760;s:6:\"height\";i:400;s:4:\"file\";s:20:\"2019/09/dummyimg.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"dummyimg-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"dummyimg-300x158.png\";s:5:\"width\";i:300;s:6:\"height\";i:158;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(145, 67, '_edit_lock', '1568768481:1'),
(146, 68, '_edit_lock', '1568768590:1'),
(151, 68, '_wp_old_date', '2019-09-18'),
(152, 70, '_edit_lock', '1568768752:1'),
(157, 70, '_wp_old_date', '2019-09-18'),
(158, 72, '_edit_lock', '1568769670:1'),
(163, 72, '_wp_old_date', '2019-09-18'),
(164, 74, '_edit_lock', '1568768934:1'),
(165, 75, '_edit_lock', '1568768827:1'),
(170, 75, '_wp_old_date', '2019-09-18'),
(171, 77, '_edit_lock', '1568768871:1'),
(176, 77, '_wp_old_date', '2019-09-18'),
(177, 79, '_edit_lock', '1568768901:1'),
(182, 79, '_wp_old_date', '2019-09-18'),
(183, 81, '_edit_lock', '1568769046:1'),
(184, 82, '_edit_lock', '1568768956:1'),
(189, 82, '_wp_old_date', '2019-09-18'),
(190, 84, '_edit_lock', '1568768987:1'),
(195, 84, '_wp_old_date', '2019-09-18'),
(196, 86, '_edit_lock', '1568769407:1'),
(201, 86, '_wp_old_date', '2019-09-18'),
(204, 72, '_wp_old_date', '2018-08-30'),
(205, 88, '_edit_lock', '1568778452:1'),
(210, 88, '_wp_old_date', '2019-09-18'),
(211, 2, '_wp_trash_meta_status', 'publish'),
(212, 2, '_wp_trash_meta_time', '1568797001'),
(213, 2, '_wp_desired_post_slug', 'sample-page'),
(214, 91, '_edit_last', '1'),
(215, 91, '_edit_lock', '1568798853:1'),
(216, 91, 'osbx_slider_custom_meta', 'a:8:{s:7:\"general\";a:9:{s:11:\"slider-mode\";s:10:\"horizontal\";s:12:\"slider-speed\";s:3:\"500\";s:12:\"slide-margin\";s:1:\"0\";s:11:\"start-slide\";s:3:\"500\";s:12:\"random-start\";s:5:\"false\";s:13:\"infinite-loop\";s:4:\"true\";s:19:\"hide-control-on-end\";s:4:\"true\";s:15:\"adaptive-height\";s:4:\"true\";s:10:\"responsive\";s:4:\"true\";}s:6:\"slides\";a:5:{i:1;a:4:{s:13:\"attachment_id\";s:1:\"6\";s:4:\"link\";s:0:\"\";s:11:\"link_target\";s:5:\"_self\";s:7:\"caption\";s:0:\"\";}i:2;a:4:{s:13:\"attachment_id\";s:2:\"13\";s:4:\"link\";s:0:\"\";s:11:\"link_target\";s:5:\"_self\";s:7:\"caption\";s:0:\"\";}i:3;a:4:{s:13:\"attachment_id\";s:2:\"14\";s:4:\"link\";s:0:\"\";s:11:\"link_target\";s:5:\"_self\";s:7:\"caption\";s:0:\"\";}i:4;a:4:{s:13:\"attachment_id\";s:2:\"15\";s:4:\"link\";s:0:\"\";s:11:\"link_target\";s:5:\"_self\";s:7:\"caption\";s:0:\"\";}s:4:\"{id}\";a:4:{s:13:\"attachment_id\";s:0:\"\";s:4:\"link\";s:0:\"\";s:11:\"link_target\";s:5:\"_self\";s:7:\"caption\";s:0:\"\";}}s:11:\"slider_type\";s:14:\"slider_caption\";s:6:\"design\";a:6:{s:7:\"bgcolor\";s:7:\"#ffffff\";s:10:\"pager-type\";s:5:\"round\";s:11:\"pager-color\";s:7:\"#666666\";s:17:\"pager-hover-color\";s:7:\"#000000\";s:11:\"prev-button\";s:0:\"\";s:11:\"next-button\";s:0:\"\";}s:5:\"pager\";a:3:{s:5:\"pager\";s:4:\"true\";s:4:\"type\";s:4:\"full\";s:15:\"short-separator\";s:1:\"/\";}s:8:\"controls\";a:6:{s:8:\"controls\";s:4:\"true\";s:9:\"next-text\";s:4:\"Next\";s:9:\"prev-text\";s:4:\"Prev\";s:13:\"auto-controls\";s:4:\"true\";s:10:\"start-text\";s:0:\"\";s:9:\"stop-text\";s:0:\"\";}s:4:\"auto\";a:6:{s:4:\"auto\";s:4:\"true\";s:5:\"pause\";s:4:\"4000\";s:13:\"auto-controls\";s:4:\"true\";s:14:\"auto-direction\";s:4:\"next\";s:10:\"auto-hover\";s:4:\"true\";s:10:\"auto-delay\";s:4:\"1000\";}s:8:\"carousal\";a:4:{s:10:\"min-slides\";s:1:\"1\";s:10:\"max-slides\";s:1:\"1\";s:11:\"move-slides\";s:1:\"1\";s:11:\"slide-width\";s:1:\"1\";}}'),
(217, 93, '_edit_last', '1'),
(218, 93, '_edit_lock', '1569210567:1'),
(219, 101, '_edit_lock', '1568868820:1'),
(222, 101, '_edit_last', '1'),
(225, 101, 'title_map', '本店'),
(226, 101, '_title_map', 'field_5d81fac6f28ed'),
(227, 101, 'address', '〒210-0005　川崎市川崎区東田町8 パレール三井ビル8階'),
(228, 101, '_address', 'field_5d81fb47f28ee'),
(229, 101, 'time', 'JR川崎駅東口より徒歩7分　京急川崎駅より徒歩5分'),
(230, 101, '_time', 'field_5d81fb68f28ef'),
(231, 101, 'tel', 'tel : 044-233-2811'),
(232, 101, '_tel', 'field_5d81fbdcf28f0'),
(233, 101, 'fax', 'fax : 044-233-0818'),
(234, 101, '_fax', 'field_5d81fbf8f28f1'),
(235, 101, 'email', 'mail : info@wms.or.jp'),
(236, 101, '_email', 'field_5d81fc36f28f2'),
(237, 101, 'image_map', '46'),
(238, 101, '_image_map', 'field_5d81fc56f28f3'),
(239, 103, 'title_map', '本店'),
(240, 103, '_title_map', 'field_5d81fac6f28ed'),
(241, 103, 'address', '〒210-0005　川崎市川崎区東田町8 パレール三井ビル8階'),
(242, 103, '_address', 'field_5d81fb47f28ee'),
(243, 103, 'time', 'JR川崎駅東口より徒歩7分　京急川崎駅より徒歩5分'),
(244, 103, '_time', 'field_5d81fb68f28ef'),
(245, 103, 'tel', 'tel : 044-233-2811'),
(246, 103, '_tel', 'field_5d81fbdcf28f0'),
(247, 103, 'fax', 'fax : 044-233-0818'),
(248, 103, '_fax', 'field_5d81fbf8f28f1'),
(249, 103, 'email', 'mail : info@wms.or.jp'),
(250, 103, '_email', 'field_5d81fc36f28f2'),
(251, 103, 'image_map', '46'),
(252, 103, '_image_map', 'field_5d81fc56f28f3'),
(253, 15, '_wp_attachment_image_alt', ''),
(254, 15, '_slider_link', NULL),
(255, 15, 'target', NULL),
(256, 15, 'cropPosition', 'center-center'),
(257, 15, 'cropPositionNeedUpdate', 'a:3:{i:0;b:0;i:1;i:250;i:2;d:133.33333333333334;}'),
(258, 105, 'ml-slider_settings', 'a:38:{s:4:\"type\";s:10:\"responsive\";s:6:\"random\";s:5:\"false\";s:8:\"cssClass\";s:0:\"\";s:8:\"printCss\";s:4:\"true\";s:7:\"printJs\";s:4:\"true\";s:5:\"width\";s:4:\"1000\";s:6:\"height\";s:3:\"400\";s:3:\"spw\";s:1:\"7\";s:3:\"sph\";s:1:\"5\";s:5:\"delay\";s:4:\"1000\";s:6:\"sDelay\";s:2:\"30\";s:7:\"opacity\";s:3:\"0.7\";s:10:\"titleSpeed\";s:3:\"500\";s:6:\"effect\";s:4:\"fade\";s:10:\"navigation\";s:4:\"true\";s:5:\"links\";s:4:\"true\";s:10:\"hoverPause\";s:5:\"false\";s:5:\"theme\";s:0:\"\";s:9:\"direction\";s:10:\"horizontal\";s:7:\"reverse\";s:5:\"false\";s:14:\"animationSpeed\";s:3:\"600\";s:8:\"prevText\";s:8:\"Previous\";s:8:\"nextText\";s:4:\"Next\";s:6:\"slices\";s:2:\"15\";s:6:\"center\";s:4:\"true\";s:9:\"smartCrop\";s:5:\"false\";s:12:\"carouselMode\";s:5:\"false\";s:14:\"carouselMargin\";s:1:\"5\";s:16:\"firstSlideFadeIn\";s:5:\"false\";s:6:\"easing\";s:6:\"linear\";s:8:\"autoPlay\";s:4:\"true\";s:11:\"thumb_width\";i:150;s:12:\"thumb_height\";i:100;s:17:\"responsive_thumbs\";s:5:\"false\";s:15:\"thumb_min_width\";i:100;s:9:\"fullWidth\";s:4:\"true\";s:10:\"noConflict\";s:5:\"false\";s:12:\"smoothHeight\";s:5:\"false\";}'),
(259, 105, 'metaslider_slideshow_theme', 'a:7:{s:6:\"folder\";s:6:\"bubble\";s:5:\"title\";s:6:\"Bubble\";s:4:\"type\";s:4:\"free\";s:8:\"supports\";a:4:{i:0;s:4:\"flex\";i:1;s:10:\"responsive\";i:2;s:4:\"nivo\";i:3;s:4:\"coin\";}s:4:\"tags\";a:3:{i:0;s:5:\"light\";i:1;s:4:\"bold\";i:2;s:5:\"round\";}s:11:\"description\";s:87:\"A fun, circular design to brighten up your site. This theme works well with dark images\";s:6:\"images\";a:5:{i:0;s:34:\"timothy-eberly-728185-unsplash.jpg\";i:1;s:30:\"wabi-jayme-578762-unsplash.jpg\";i:2;s:32:\"ella-olsson-1094090-unsplash.jpg\";i:3;s:34:\"fabio-mangione-236846-unsplash.jpg\";i:4;s:34:\"victoria-shes-1096105-unsplash.jpg\";}}'),
(260, 106, '_thumbnail_id', '6'),
(261, 106, 'ml-slider_type', 'image'),
(262, 106, 'ml-slider_inherit_image_title', '1'),
(263, 106, 'ml-slider_inherit_image_alt', '1'),
(264, 107, '_thumbnail_id', '13'),
(265, 107, 'ml-slider_type', 'image'),
(266, 107, 'ml-slider_inherit_image_title', '1'),
(267, 107, 'ml-slider_inherit_image_alt', '1'),
(268, 108, '_thumbnail_id', '14'),
(269, 108, 'ml-slider_type', 'image'),
(270, 108, 'ml-slider_inherit_image_title', '1'),
(271, 108, 'ml-slider_inherit_image_alt', '1'),
(272, 109, '_thumbnail_id', '15'),
(273, 109, 'ml-slider_type', 'image'),
(274, 109, 'ml-slider_inherit_image_title', '1'),
(275, 109, 'ml-slider_inherit_image_alt', '1'),
(276, 6, '_wp_attachment_backup_sizes', 'a:3:{s:15:\"resized-700x300\";a:5:{s:4:\"path\";s:78:\"C:xampphtdocsCamTuTask007/wp-content/uploads/2019/09/main_img01_no-700x300.png\";s:4:\"file\";s:25:\"main_img01_no-700x300.png\";s:5:\"width\";i:700;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"resized-620x266\";a:5:{s:4:\"path\";s:78:\"C:xampphtdocsCamTuTask007/wp-content/uploads/2019/09/main_img01_no-620x266.png\";s:4:\"file\";s:25:\"main_img01_no-620x266.png\";s:5:\"width\";i:620;s:6:\"height\";i:266;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"resized-400x171\";a:5:{s:4:\"path\";s:78:\"C:xampphtdocsCamTuTask007/wp-content/uploads/2019/09/main_img01_no-400x171.png\";s:4:\"file\";s:25:\"main_img01_no-400x171.png\";s:5:\"width\";i:400;s:6:\"height\";i:171;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(277, 13, '_wp_attachment_backup_sizes', 'a:3:{s:15:\"resized-700x300\";a:5:{s:4:\"path\";s:78:\"C:xampphtdocsCamTuTask007/wp-content/uploads/2019/09/main_img02_no-700x300.png\";s:4:\"file\";s:25:\"main_img02_no-700x300.png\";s:5:\"width\";i:700;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"resized-620x266\";a:5:{s:4:\"path\";s:78:\"C:xampphtdocsCamTuTask007/wp-content/uploads/2019/09/main_img02_no-620x266.png\";s:4:\"file\";s:25:\"main_img02_no-620x266.png\";s:5:\"width\";i:620;s:6:\"height\";i:266;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"resized-400x171\";a:5:{s:4:\"path\";s:78:\"C:xampphtdocsCamTuTask007/wp-content/uploads/2019/09/main_img02_no-400x171.png\";s:4:\"file\";s:25:\"main_img02_no-400x171.png\";s:5:\"width\";i:400;s:6:\"height\";i:171;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(278, 14, '_wp_attachment_backup_sizes', 'a:3:{s:15:\"resized-700x300\";a:5:{s:4:\"path\";s:78:\"C:xampphtdocsCamTuTask007/wp-content/uploads/2019/09/main_img03_no-700x300.png\";s:4:\"file\";s:25:\"main_img03_no-700x300.png\";s:5:\"width\";i:700;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"resized-620x266\";a:5:{s:4:\"path\";s:78:\"C:xampphtdocsCamTuTask007/wp-content/uploads/2019/09/main_img03_no-620x266.png\";s:4:\"file\";s:25:\"main_img03_no-620x266.png\";s:5:\"width\";i:620;s:6:\"height\";i:266;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"resized-400x171\";a:5:{s:4:\"path\";s:78:\"C:xampphtdocsCamTuTask007/wp-content/uploads/2019/09/main_img03_no-400x171.png\";s:4:\"file\";s:25:\"main_img03_no-400x171.png\";s:5:\"width\";i:400;s:6:\"height\";i:171;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(279, 15, '_wp_attachment_backup_sizes', 'a:3:{s:15:\"resized-700x300\";a:5:{s:4:\"path\";s:78:\"C:xampphtdocsCamTuTask007/wp-content/uploads/2019/09/main_img04_no-700x300.png\";s:4:\"file\";s:25:\"main_img04_no-700x300.png\";s:5:\"width\";i:700;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"resized-620x266\";a:5:{s:4:\"path\";s:78:\"C:xampphtdocsCamTuTask007/wp-content/uploads/2019/09/main_img04_no-620x266.png\";s:4:\"file\";s:25:\"main_img04_no-620x266.png\";s:5:\"width\";i:620;s:6:\"height\";i:266;s:9:\"mime-type\";s:9:\"image/png\";}s:15:\"resized-400x171\";a:5:{s:4:\"path\";s:78:\"C:xampphtdocsCamTuTask007/wp-content/uploads/2019/09/main_img04_no-400x171.png\";s:4:\"file\";s:25:\"main_img04_no-400x171.png\";s:5:\"width\";i:400;s:6:\"height\";i:171;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(280, 106, 'ml-slider_crop_position', 'center-center'),
(281, 106, 'ml-slider_caption_source', 'image-caption'),
(282, 106, '_wp_attachment_image_alt', ''),
(283, 107, 'ml-slider_crop_position', 'center-center'),
(284, 107, 'ml-slider_caption_source', 'image-caption'),
(285, 107, '_wp_attachment_image_alt', ''),
(286, 108, 'ml-slider_crop_position', 'center-center'),
(287, 108, 'ml-slider_caption_source', 'image-caption'),
(288, 108, '_wp_attachment_image_alt', ''),
(289, 109, 'ml-slider_crop_position', 'center-center'),
(290, 109, 'ml-slider_caption_source', 'image-caption'),
(291, 109, '_wp_attachment_image_alt', ''),
(316, 120, '_edit_lock', '1568868761:1'),
(319, 120, '_edit_last', '1'),
(322, 120, 'title_map', '相模原支店'),
(323, 120, '_title_map', 'field_5d81fac6f28ed'),
(324, 120, 'address', '〒252-0231　相模原市中央区相模原3-8-25 第3JSビル7階'),
(325, 120, '_address', 'field_5d81fb47f28ee'),
(326, 120, 'time', 'JR横浜線相模原駅より徒歩2分'),
(327, 120, '_time', 'field_5d81fb68f28ef'),
(328, 120, 'tel', 'tel : 042-704-9581'),
(329, 120, '_tel', 'field_5d81fbdcf28f0'),
(330, 120, 'fax', 'fax : 042-704-9582'),
(331, 120, '_fax', 'field_5d81fbf8f28f1'),
(332, 120, 'email', ''),
(333, 120, '_email', 'field_5d81fc36f28f2'),
(334, 120, 'image_map', '50'),
(335, 120, '_image_map', 'field_5d81fc56f28f3'),
(336, 122, 'title_map', '相模原支店'),
(337, 122, '_title_map', 'field_5d81fac6f28ed'),
(338, 122, 'address', '〒252-0231　相模原市中央区相模原3-8-25 第3JSビル7階'),
(339, 122, '_address', 'field_5d81fb47f28ee'),
(340, 122, 'time', 'JR横浜線相模原駅より徒歩2分'),
(341, 122, '_time', 'field_5d81fb68f28ef'),
(342, 122, 'tel', 'tel : 042-704-9581'),
(343, 122, '_tel', 'field_5d81fbdcf28f0'),
(344, 122, 'fax', 'fax : 042-704-9582'),
(345, 122, '_fax', 'field_5d81fbf8f28f1'),
(346, 122, 'email', ''),
(347, 122, '_email', 'field_5d81fc36f28f2'),
(348, 122, 'image_map', '50'),
(349, 122, '_image_map', 'field_5d81fc56f28f3'),
(352, 120, '_wp_old_date', '2019-09-19'),
(355, 101, '_wp_old_date', '2019-09-18'),
(356, 123, '_edit_lock', '1569204416:1'),
(357, 125, '_edit_lock', '1569204438:1'),
(358, 127, '_edit_lock', '1569204455:1'),
(359, 129, '_edit_lock', '1569204473:1'),
(360, 131, '_edit_lock', '1569204494:1'),
(361, 54, '_edit_last', '1'),
(362, 54, '_wp_page_template', 'default'),
(363, 133, '_menu_item_type', 'post_type'),
(364, 133, '_menu_item_menu_item_parent', '0'),
(365, 133, '_menu_item_object_id', '129'),
(366, 133, '_menu_item_object', 'page'),
(367, 133, '_menu_item_target', ''),
(368, 133, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(369, 133, '_menu_item_xfn', ''),
(370, 133, '_menu_item_url', ''),
(372, 134, '_menu_item_type', 'post_type'),
(373, 134, '_menu_item_menu_item_parent', '0'),
(374, 134, '_menu_item_object_id', '127'),
(375, 134, '_menu_item_object', 'page'),
(376, 134, '_menu_item_target', ''),
(377, 134, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(378, 134, '_menu_item_xfn', ''),
(379, 134, '_menu_item_url', ''),
(381, 135, '_menu_item_type', 'post_type'),
(382, 135, '_menu_item_menu_item_parent', '0'),
(383, 135, '_menu_item_object_id', '125'),
(384, 135, '_menu_item_object', 'page'),
(385, 135, '_menu_item_target', ''),
(386, 135, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(387, 135, '_menu_item_xfn', ''),
(388, 135, '_menu_item_url', ''),
(390, 136, '_menu_item_type', 'post_type'),
(391, 136, '_menu_item_menu_item_parent', '0'),
(392, 136, '_menu_item_object_id', '123'),
(393, 136, '_menu_item_object', 'page'),
(394, 136, '_menu_item_target', ''),
(395, 136, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(396, 136, '_menu_item_xfn', ''),
(397, 136, '_menu_item_url', ''),
(399, 137, '_menu_item_type', 'post_type'),
(400, 137, '_menu_item_menu_item_parent', '0'),
(401, 137, '_menu_item_object_id', '54'),
(402, 137, '_menu_item_object', 'page'),
(403, 137, '_menu_item_target', ''),
(404, 137, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(405, 137, '_menu_item_xfn', ''),
(406, 137, '_menu_item_url', ''),
(408, 138, '_edit_lock', '1569205281:1'),
(409, 140, '_edit_lock', '1569205302:1'),
(410, 142, '_edit_lock', '1569205306:1'),
(411, 144, '_menu_item_type', 'post_type'),
(412, 144, '_menu_item_menu_item_parent', '0'),
(413, 144, '_menu_item_object_id', '142'),
(414, 144, '_menu_item_object', 'page'),
(415, 144, '_menu_item_target', ''),
(416, 144, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(417, 144, '_menu_item_xfn', ''),
(418, 144, '_menu_item_url', ''),
(420, 145, '_menu_item_type', 'post_type'),
(421, 145, '_menu_item_menu_item_parent', '0'),
(422, 145, '_menu_item_object_id', '140'),
(423, 145, '_menu_item_object', 'page'),
(424, 145, '_menu_item_target', ''),
(425, 145, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(426, 145, '_menu_item_xfn', ''),
(427, 145, '_menu_item_url', ''),
(429, 146, '_menu_item_type', 'post_type'),
(430, 146, '_menu_item_menu_item_parent', '0'),
(431, 146, '_menu_item_object_id', '138'),
(432, 146, '_menu_item_object', 'page'),
(433, 146, '_menu_item_target', ''),
(434, 146, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(435, 146, '_menu_item_xfn', ''),
(436, 146, '_menu_item_url', ''),
(438, 147, '_menu_item_type', 'post_type'),
(439, 147, '_menu_item_menu_item_parent', '0'),
(440, 147, '_menu_item_object_id', '129'),
(441, 147, '_menu_item_object', 'page'),
(442, 147, '_menu_item_target', ''),
(443, 147, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(444, 147, '_menu_item_xfn', ''),
(445, 147, '_menu_item_url', ''),
(447, 148, '_menu_item_type', 'post_type'),
(448, 148, '_menu_item_menu_item_parent', '0'),
(449, 148, '_menu_item_object_id', '127'),
(450, 148, '_menu_item_object', 'page'),
(451, 148, '_menu_item_target', ''),
(452, 148, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(453, 148, '_menu_item_xfn', ''),
(454, 148, '_menu_item_url', ''),
(456, 149, '_menu_item_type', 'post_type'),
(457, 149, '_menu_item_menu_item_parent', '0'),
(458, 149, '_menu_item_object_id', '125'),
(459, 149, '_menu_item_object', 'page'),
(460, 149, '_menu_item_target', ''),
(461, 149, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(462, 149, '_menu_item_xfn', ''),
(463, 149, '_menu_item_url', ''),
(465, 150, '_menu_item_type', 'post_type'),
(466, 150, '_menu_item_menu_item_parent', '0'),
(467, 150, '_menu_item_object_id', '123'),
(468, 150, '_menu_item_object', 'page'),
(469, 150, '_menu_item_target', ''),
(470, 150, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(471, 150, '_menu_item_xfn', ''),
(472, 150, '_menu_item_url', ''),
(474, 151, '_menu_item_type', 'post_type'),
(475, 151, '_menu_item_menu_item_parent', '0'),
(476, 151, '_menu_item_object_id', '54'),
(477, 151, '_menu_item_object', 'page'),
(478, 151, '_menu_item_target', ''),
(479, 151, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(480, 151, '_menu_item_xfn', ''),
(481, 151, '_menu_item_url', ''),
(483, 152, '_menu_item_type', 'post_type'),
(484, 152, '_menu_item_menu_item_parent', '0'),
(485, 152, '_menu_item_object_id', '51'),
(486, 152, '_menu_item_object', 'page'),
(487, 152, '_menu_item_target', ''),
(488, 152, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(489, 152, '_menu_item_xfn', ''),
(490, 152, '_menu_item_url', ''),
(492, 154, '_edit_lock', '1569208758:1'),
(493, 155, '_edit_lock', '1569209399:1'),
(494, 155, '_edit_last', '1'),
(495, 155, 'title_map', '本店'),
(496, 155, '_title_map', 'field_5d81fac6f28ed'),
(497, 155, 'address', '〒210-0005　川崎市川崎区東田町8 パレール三井ビル8階'),
(498, 155, '_address', 'field_5d81fb47f28ee'),
(499, 155, 'time', 'JR川崎駅東口より徒歩7分　京急川崎駅より徒歩5分'),
(500, 155, '_time', 'field_5d81fb68f28ef'),
(501, 155, 'tel', 'tel : 044-233-2811'),
(502, 155, '_tel', 'field_5d81fbdcf28f0'),
(503, 155, 'fax', 'fax : 044-233-0818'),
(504, 155, '_fax', 'field_5d81fbf8f28f1'),
(505, 155, 'email', 'mail : info@wms.or.jp'),
(506, 155, '_email', 'field_5d81fc36f28f2'),
(507, 155, 'image_map', '46'),
(508, 155, '_image_map', 'field_5d81fc56f28f3'),
(509, 157, 'title_map', '本店'),
(510, 157, '_title_map', 'field_5d81fac6f28ed'),
(511, 157, 'address', '〒210-0005　川崎市川崎区東田町8 パレール三井ビル8階'),
(512, 157, '_address', 'field_5d81fb47f28ee'),
(513, 157, 'time', 'JR川崎駅東口より徒歩7分　京急川崎駅より徒歩5分'),
(514, 157, '_time', 'field_5d81fb68f28ef'),
(515, 157, 'tel', 'tel : 044-233-2811'),
(516, 157, '_tel', 'field_5d81fbdcf28f0'),
(517, 157, 'fax', 'fax : 044-233-0818'),
(518, 157, '_fax', 'field_5d81fbf8f28f1'),
(519, 157, 'email', 'mail : info@wms.or.jp'),
(520, 157, '_email', 'field_5d81fc36f28f2'),
(521, 157, 'image_map', '46'),
(522, 157, '_image_map', 'field_5d81fc56f28f3'),
(525, 160, '_edit_lock', '1569211915:1'),
(526, 160, '_edit_last', '1'),
(527, 160, 'title_map', ''),
(528, 160, '_title_map', 'field_5d81fac6f28ed'),
(529, 160, 'address', ''),
(530, 160, '_address', 'field_5d81fb47f28ee'),
(531, 160, 'time', ''),
(532, 160, '_time', 'field_5d81fb68f28ef'),
(533, 160, 'tel', ''),
(534, 160, '_tel', 'field_5d81fbdcf28f0'),
(535, 160, 'fax', ''),
(536, 160, '_fax', 'field_5d81fbf8f28f1'),
(537, 160, 'email', ''),
(538, 160, '_email', 'field_5d81fc36f28f2'),
(539, 160, 'image_map', ''),
(540, 160, '_image_map', 'field_5d81fc56f28f3'),
(541, 161, 'title_map', ''),
(542, 161, '_title_map', 'field_5d81fac6f28ed'),
(543, 161, 'address', ''),
(544, 161, '_address', 'field_5d81fb47f28ee'),
(545, 161, 'time', ''),
(546, 161, '_time', 'field_5d81fb68f28ef'),
(547, 161, 'tel', ''),
(548, 161, '_tel', 'field_5d81fbdcf28f0'),
(549, 161, 'fax', ''),
(550, 161, '_fax', 'field_5d81fbf8f28f1'),
(551, 161, 'email', ''),
(552, 161, '_email', 'field_5d81fc36f28f2'),
(553, 161, 'image_map', ''),
(554, 161, '_image_map', 'field_5d81fc56f28f3'),
(555, 93, '_wp_trash_meta_status', 'publish'),
(556, 93, '_wp_trash_meta_time', '1569212079'),
(557, 93, '_wp_desired_post_slug', 'group_5d81fa706d8e8'),
(558, 94, '_wp_trash_meta_status', 'publish'),
(559, 94, '_wp_trash_meta_time', '1569212079'),
(560, 94, '_wp_desired_post_slug', 'field_5d81fac6f28ed'),
(561, 95, '_wp_trash_meta_status', 'publish'),
(562, 95, '_wp_trash_meta_time', '1569212079'),
(563, 95, '_wp_desired_post_slug', 'field_5d81fb47f28ee'),
(564, 96, '_wp_trash_meta_status', 'publish'),
(565, 96, '_wp_trash_meta_time', '1569212079'),
(566, 96, '_wp_desired_post_slug', 'field_5d81fb68f28ef'),
(567, 97, '_wp_trash_meta_status', 'publish'),
(568, 97, '_wp_trash_meta_time', '1569212079'),
(569, 97, '_wp_desired_post_slug', 'field_5d81fbdcf28f0'),
(570, 98, '_wp_trash_meta_status', 'publish'),
(571, 98, '_wp_trash_meta_time', '1569212079'),
(572, 98, '_wp_desired_post_slug', 'field_5d81fbf8f28f1'),
(573, 99, '_wp_trash_meta_status', 'publish'),
(574, 99, '_wp_trash_meta_time', '1569212080'),
(575, 99, '_wp_desired_post_slug', 'field_5d81fc36f28f2'),
(576, 100, '_wp_trash_meta_status', 'publish'),
(577, 100, '_wp_trash_meta_time', '1569212080'),
(578, 100, '_wp_desired_post_slug', 'field_5d81fc56f28f3'),
(579, 164, '_edit_last', '1'),
(580, 164, '_edit_lock', '1569212387:1'),
(581, 172, '_edit_lock', '1569212203:1'),
(582, 173, '_edit_lock', '1569212219:1'),
(583, 174, '_edit_lock', '1569212235:1'),
(584, 175, '_edit_lock', '1569212252:1'),
(585, 176, '_edit_lock', '1569212317:1'),
(586, 164, '_wp_trash_meta_status', 'publish'),
(587, 164, '_wp_trash_meta_time', '1569212640'),
(588, 164, '_wp_desired_post_slug', 'group_5d8846b7902c7'),
(589, 165, '_wp_trash_meta_status', 'publish'),
(590, 165, '_wp_trash_meta_time', '1569212640'),
(591, 165, '_wp_desired_post_slug', 'field_5d8846bd77018'),
(592, 166, '_wp_trash_meta_status', 'publish'),
(593, 166, '_wp_trash_meta_time', '1569212641'),
(594, 166, '_wp_desired_post_slug', 'field_5d88470577019'),
(595, 167, '_wp_trash_meta_status', 'publish'),
(596, 167, '_wp_trash_meta_time', '1569212641'),
(597, 167, '_wp_desired_post_slug', 'field_5d8847377701a'),
(598, 168, '_wp_trash_meta_status', 'publish'),
(599, 168, '_wp_trash_meta_time', '1569212641'),
(600, 168, '_wp_desired_post_slug', 'field_5d8847457701b'),
(601, 169, '_wp_trash_meta_status', 'publish'),
(602, 169, '_wp_trash_meta_time', '1569212641'),
(603, 169, '_wp_desired_post_slug', 'field_5d8847547701c'),
(604, 170, '_wp_trash_meta_status', 'publish'),
(605, 170, '_wp_trash_meta_time', '1569212641'),
(606, 170, '_wp_desired_post_slug', 'field_5d88475f7701d'),
(607, 171, '_wp_trash_meta_status', 'publish'),
(608, 171, '_wp_trash_meta_time', '1569212641'),
(609, 171, '_wp_desired_post_slug', 'field_5d88476a7701e'),
(610, 177, '_edit_last', '1'),
(611, 177, '_edit_lock', '1569212597:1'),
(612, 177, '_wp_trash_meta_status', 'publish'),
(613, 177, '_wp_trash_meta_time', '1569212815'),
(614, 177, '_wp_desired_post_slug', 'group_5d8848e666da9'),
(615, 178, '_wp_trash_meta_status', 'publish'),
(616, 178, '_wp_trash_meta_time', '1569212816'),
(617, 178, '_wp_desired_post_slug', 'field_5d8848f5acb7d'),
(618, 180, '_edit_last', '1'),
(619, 180, '_edit_lock', '1569214712:1'),
(620, 190, '_edit_last', '1'),
(621, 190, '_edit_lock', '1569218747:1'),
(622, 101, '_wp_trash_meta_status', 'publish'),
(623, 101, '_wp_trash_meta_time', '1569221029'),
(624, 101, '_wp_desired_post_slug', '%e6%9c%ac%e5%ba%97'),
(625, 120, '_wp_trash_meta_status', 'publish'),
(626, 120, '_wp_trash_meta_time', '1569221056'),
(627, 120, '_wp_desired_post_slug', '%e7%9b%b8%e6%a8%a1%e5%8e%9f%e6%94%af%e5%ba%97'),
(628, 57, '_wp_trash_meta_status', 'publish'),
(629, 57, '_wp_trash_meta_time', '1569224955'),
(630, 57, '_wp_desired_post_slug', 'search-page'),
(631, 3, '_wp_trash_meta_status', 'draft'),
(632, 3, '_wp_trash_meta_time', '1569224959'),
(633, 3, '_wp_desired_post_slug', 'privacy-policy'),
(634, 160, '_wp_trash_meta_status', 'publish'),
(635, 160, '_wp_trash_meta_time', '1569224963'),
(636, 160, '_wp_desired_post_slug', 'home');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-09-16 04:16:01', '2019-09-16 04:16:01', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'trash', 'open', 'open', '', 'hello-world__trashed', '', '', '2019-09-16 07:49:13', '2019-09-16 07:49:13', '', 0, 'http://localhost:8080/CamTu/Task007/?p=1', 0, 'post', '', 1),
(2, 1, '2019-09-16 04:16:01', '2019-09-16 04:16:01', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost:8080/CamTu/Task007/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'trash', 'closed', 'open', '', 'sample-page__trashed', '', '', '2019-09-18 08:56:41', '2019-09-18 08:56:41', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-09-16 04:16:01', '2019-09-16 04:16:01', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost:8080/CamTu/Task007.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'trash', 'closed', 'open', '', 'privacy-policy__trashed', '', '', '2019-09-23 07:49:19', '2019-09-23 07:49:19', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=3', 0, 'page', '', 0),
(5, 1, '2019-09-16 06:59:59', '2019-09-16 06:59:59', '', 'slide1', '', 'trash', 'open', 'open', '', 'slide1__trashed', '', '', '2019-09-16 07:49:12', '2019-09-16 07:49:12', '', 0, 'http://localhost:8080/CamTu/Task007/?p=5', 0, 'post', '', 0),
(6, 1, '2019-09-16 06:59:53', '2019-09-16 06:59:53', '', 'main_img01_no', '', 'inherit', 'open', 'closed', '', 'main_img01_no', '', '', '2019-09-16 06:59:53', '2019-09-16 06:59:53', '', 5, 'http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/main_img01_no.png', 0, 'attachment', 'image/png', 0),
(7, 1, '2019-09-16 06:59:59', '2019-09-16 06:59:59', '', 'slide1', '', 'inherit', 'closed', 'closed', '', '5-revision-v1', '', '', '2019-09-16 06:59:59', '2019-09-16 06:59:59', '', 5, 'http://localhost:8080/CamTu/Task007/2019/09/16/5-revision-v1/', 0, 'revision', '', 0),
(9, 1, '2019-09-16 07:49:13', '2019-09-16 07:49:13', '<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->', 'Hello world!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-09-16 07:49:13', '2019-09-16 07:49:13', '', 1, 'http://localhost:8080/CamTu/Task007/2019/09/16/1-revision-v1/', 0, 'revision', '', 0),
(10, 1, '2019-09-16 07:50:37', '2019-09-16 07:50:37', '', 'Slide 1', '', 'publish', 'closed', 'closed', '', 'slide1', '', '', '2019-09-16 08:58:38', '2019-09-16 08:58:38', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=slider&#038;p=10', 0, 'slider', '', 0),
(11, 1, '2019-09-16 08:01:26', '2019-09-16 08:01:26', '', 'slider1', '', 'publish', 'closed', 'closed', '', 'slider1', '', '', '2019-09-16 08:01:29', '2019-09-16 08:01:29', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=feature&#038;p=11', 0, 'feature', '', 0),
(12, 1, '2019-09-16 08:03:13', '2019-09-16 08:03:13', '', 'Slide 2', '', 'publish', 'closed', 'closed', '', 'slide-2', '', '', '2019-09-16 08:03:13', '2019-09-16 08:03:13', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=feature&#038;p=12', 0, 'feature', '', 0),
(13, 1, '2019-09-16 08:02:52', '2019-09-16 08:02:52', '', 'main_img02_no', '', 'inherit', 'open', 'closed', '', 'main_img02_no', '', '', '2019-09-16 08:02:52', '2019-09-16 08:02:52', '', 12, 'http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/main_img02_no.png', 0, 'attachment', 'image/png', 0),
(14, 1, '2019-09-16 08:02:53', '2019-09-16 08:02:53', '', 'main_img03_no', '', 'inherit', 'open', 'closed', '', 'main_img03_no', '', '', '2019-09-16 08:02:53', '2019-09-16 08:02:53', '', 12, 'http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/main_img03_no.png', 0, 'attachment', 'image/png', 0),
(15, 1, '2019-09-16 08:02:54', '2019-09-16 08:02:54', '', 'main_img04_no', '', 'inherit', 'open', 'closed', '', 'main_img04_no', '', '', '2019-09-19 01:17:54', '2019-09-19 01:17:54', '', 12, 'http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/main_img04_no.png', 0, 'attachment', 'image/png', 0),
(17, 1, '2019-09-16 08:04:44', '2019-09-16 08:04:44', '', 'Slide 3', '', 'publish', 'closed', 'closed', '', 'slide-3', '', '', '2019-09-16 08:04:44', '2019-09-16 08:04:44', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=feature&#038;p=17', 0, 'feature', '', 0),
(18, 1, '2019-09-16 08:05:00', '2019-09-16 08:05:00', '', 'Slide 4', '', 'publish', 'closed', 'closed', '', 'slide-4', '', '', '2019-09-16 08:05:00', '2019-09-16 08:05:00', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=feature&#038;p=18', 0, 'feature', '', 0),
(20, 1, '2019-09-16 08:14:35', '2019-09-16 08:14:35', '', 'Slide 2', '', 'publish', 'closed', 'closed', '', 'slide-2', '', '', '2019-09-16 08:14:35', '2019-09-16 08:14:35', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=slider&#038;p=20', 0, 'slider', '', 0),
(21, 1, '2019-09-16 08:14:52', '2019-09-16 08:14:52', '', 'Slide 3', '', 'publish', 'closed', 'closed', '', 'slide-3', '', '', '2019-09-16 08:14:52', '2019-09-16 08:14:52', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=slider&#038;p=21', 0, 'slider', '', 0),
(22, 1, '2019-09-16 08:15:10', '2019-09-16 08:15:10', '', 'Slide 4', '', 'publish', 'closed', 'closed', '', 'slide-4', '', '', '2019-09-16 08:15:10', '2019-09-16 08:15:10', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=slider&#038;p=22', 0, 'slider', '', 0),
(24, 1, '2018-08-20 09:43:12', '2018-08-20 09:43:12', '', '働き方改革”と管理者', '', 'trash', 'open', 'open', '', '%e5%83%8d%e3%81%8d%e6%96%b9%e6%94%b9%e9%9d%a9%e3%81%a8%e7%ae%a1%e7%90%86%e8%80%85__trashed', '', '', '2019-09-17 01:07:16', '2019-09-17 01:07:16', '', 0, 'http://localhost:8080/CamTu/Task007/?p=24', 0, 'post', '', 0),
(25, 1, '2019-09-16 09:43:12', '2019-09-16 09:43:12', '', '働き方改革”と管理者', '', 'inherit', 'closed', 'closed', '', '24-revision-v1', '', '', '2019-09-16 09:43:12', '2019-09-16 09:43:12', '', 24, 'http://localhost:8080/CamTu/Task007/2019/09/16/24-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2019-09-17 01:42:58', '2019-09-17 01:42:58', '', 'ljlhj', '', 'trash', 'open', 'open', '', 'ljlhj__trashed', '', '', '2019-09-17 03:55:25', '2019-09-17 03:55:25', '', 0, 'http://localhost:8080/CamTu/Task007/?p=26', 0, 'post', '', 0),
(27, 1, '2019-09-17 01:42:58', '2019-09-17 01:42:58', '', 'ljlhj', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-09-17 01:42:58', '2019-09-17 01:42:58', '', 26, 'http://localhost:8080/CamTu/Task007/2019/09/17/26-revision-v1/', 0, 'revision', '', 0),
(28, 1, '2019-09-17 01:43:28', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-17 01:43:28', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?p=28', 0, 'post', '', 0),
(29, 1, '2019-09-17 01:44:04', '2019-09-17 01:44:04', '', 'mdvnlko', '', 'trash', 'open', 'open', '', 'mdvnlko__trashed', '', '', '2019-09-17 02:32:29', '2019-09-17 02:32:29', '', 0, 'http://localhost:8080/CamTu/Task007/?p=29', 0, 'post', '', 0),
(30, 1, '2019-09-17 01:44:04', '2019-09-17 01:44:04', '', 'mdvnlko', '', 'inherit', 'closed', 'closed', '', '29-revision-v1', '', '', '2019-09-17 01:44:04', '2019-09-17 01:44:04', '', 29, 'http://localhost:8080/CamTu/Task007/2019/09/17/29-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2019-09-17 02:31:34', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-17 02:31:34', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?p=31', 0, 'post', '', 0),
(32, 1, '2019-09-17 03:54:49', '2019-09-17 03:54:49', '', 'ghjftyu', '', 'trash', 'open', 'open', '', 'ghjftyu__trashed', '', '', '2019-09-17 03:55:25', '2019-09-17 03:55:25', '', 0, 'http://localhost:8080/CamTu/Task007/?p=32', 0, 'post', '', 0),
(33, 1, '2019-09-17 03:54:49', '2019-09-17 03:54:49', '', 'ghjftyu', '', 'inherit', 'closed', 'closed', '', '32-revision-v1', '', '', '2019-09-17 03:54:49', '2019-09-17 03:54:49', '', 32, 'http://localhost:8080/CamTu/Task007/2019/09/17/32-revision-v1/', 0, 'revision', '', 0),
(34, 1, '2018-08-20 03:57:23', '2018-08-20 03:57:23', '<!-- wp:image {\"id\":61} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/dummyimg.png\" alt=\"\" class=\"wp-image-61\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 新しい法定された権利の創設 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 評価額と権利の性質 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 所得税への影響 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->', '働き方改革”と管理者', '', 'publish', 'open', 'open', '', '%e5%83%8d%e3%81%8d%e6%96%b9%e6%94%b9%e9%9d%a9%e3%81%a8%e7%ae%a1%e7%90%86%e8%80%85', '', '', '2019-09-17 10:20:46', '2019-09-17 10:20:46', '', 0, 'http://localhost:8080/CamTu/Task007/?p=34', 0, 'post', '', 0),
(35, 1, '2019-09-17 03:57:23', '2019-09-17 03:57:23', '', '働き方改革”と管理者', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2019-09-17 03:57:23', '2019-09-17 03:57:23', '', 34, 'http://localhost:8080/CamTu/Task007/2019/09/17/34-revision-v1/', 0, 'revision', '', 0),
(36, 1, '2018-08-20 03:58:29', '2018-08-20 03:58:29', '<!-- wp:image {\"id\":61} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/dummyimg.png\" alt=\"\" class=\"wp-image-61\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 新しい法定された権利の創設 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 評価額と権利の性質 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 所得税への影響 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->', '国税庁レポートから読み解く2018年度の重点事項', '', 'publish', 'open', 'open', '', '%e5%9b%bd%e7%a8%8e%e5%ba%81%e3%83%ac%e3%83%9d%e3%83%bc%e3%83%88%e3%81%8b%e3%82%89%e8%aa%ad%e3%81%bf%e8%a7%a3%e3%81%8f2018%e5%b9%b4%e5%ba%a6%e3%81%ae%e9%87%8d%e7%82%b9%e4%ba%8b%e9%a0%85', '', '', '2019-09-17 10:20:02', '2019-09-17 10:20:02', '', 0, 'http://localhost:8080/CamTu/Task007/?p=36', 0, 'post', '', 0),
(37, 1, '2019-09-17 03:58:29', '2019-09-17 03:58:29', '', '国税庁レポートから読み解く2018年度の重点事項', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2019-09-17 03:58:29', '2019-09-17 03:58:29', '', 36, 'http://localhost:8080/CamTu/Task007/2019/09/17/36-revision-v1/', 0, 'revision', '', 0),
(38, 1, '2018-08-21 03:59:37', '2018-08-21 03:59:37', '<!-- wp:image {\"id\":61} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/dummyimg.png\" alt=\"\" class=\"wp-image-61\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 新しい法定された権利の創設 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 評価額と権利の性質 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 所得税への影響 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->', '自然災害と中小企業支援策', '', 'publish', 'open', 'open', '', '%e8%87%aa%e7%84%b6%e7%81%bd%e5%ae%b3%e3%81%a8%e4%b8%ad%e5%b0%8f%e4%bc%81%e6%a5%ad%e6%94%af%e6%8f%b4%e7%ad%96', '', '', '2019-09-17 10:19:12', '2019-09-17 10:19:12', '', 0, 'http://localhost:8080/CamTu/Task007/?p=38', 0, 'post', '', 0),
(39, 1, '2019-09-17 03:59:37', '2019-09-17 03:59:37', '', '自然災害と中小企業支援策', '', 'inherit', 'closed', 'closed', '', '38-revision-v1', '', '', '2019-09-17 03:59:37', '2019-09-17 03:59:37', '', 38, 'http://localhost:8080/CamTu/Task007/2019/09/17/38-revision-v1/', 0, 'revision', '', 0),
(40, 1, '2018-08-22 04:00:16', '2018-08-22 04:00:16', '<!-- wp:image {\"id\":61} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/dummyimg.png\" alt=\"\" class=\"wp-image-61\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 新しい法定された権利の創設 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 評価額と権利の性質 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 所得税への影響 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->', '介護保険の被保険者', '', 'publish', 'open', 'open', '', '%e4%bb%8b%e8%ad%b7%e4%bf%9d%e9%99%ba%e3%81%ae%e8%a2%ab%e4%bf%9d%e9%99%ba%e8%80%85', '', '', '2019-09-17 10:18:43', '2019-09-17 10:18:43', '', 0, 'http://localhost:8080/CamTu/Task007/?p=40', 0, 'post', '', 0),
(41, 1, '2019-09-17 04:00:16', '2019-09-17 04:00:16', '', '介護保険の被保険者', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2019-09-17 04:00:16', '2019-09-17 04:00:16', '', 40, 'http://localhost:8080/CamTu/Task007/2019/09/17/40-revision-v1/', 0, 'revision', '', 0),
(42, 1, '2019-09-17 04:00:39', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-17 04:00:39', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?p=42', 0, 'post', '', 0),
(43, 1, '2018-08-24 04:01:02', '2018-08-24 04:01:02', '<!-- wp:image {\"id\":61} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/dummyimg.png\" alt=\"\" class=\"wp-image-61\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 新しい法定された権利の創設 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 評価額と権利の性質 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 所得税への影響 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->', '新しい権利　配偶者終身居住権', '', 'publish', 'open', 'open', '', '%e6%96%b0%e3%81%97%e3%81%84%e6%a8%a9%e5%88%a9%e3%80%80%e9%85%8d%e5%81%b6%e8%80%85%e7%b5%82%e8%ba%ab%e5%b1%85%e4%bd%8f%e6%a8%a9', '', '', '2019-09-17 10:17:46', '2019-09-17 10:17:46', '', 0, 'http://localhost:8080/CamTu/Task007/?p=43', 0, 'post', '', 0),
(44, 1, '2019-09-17 04:01:02', '2019-09-17 04:01:02', '', '新しい権利　配偶者終身居住権', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2019-09-17 04:01:02', '2019-09-17 04:01:02', '', 43, 'http://localhost:8080/CamTu/Task007/2019/09/17/43-revision-v1/', 0, 'revision', '', 0),
(45, 1, '2019-09-17 04:51:42', '2019-09-17 04:51:42', '<p class=\"address\">〒210-0005　川崎市川崎区東田町8 パレール三井ビル8階</p>\r\n<p class=\"time\">JR川崎駅東口より徒歩7分　京急川崎駅より徒歩5分</p>\r\n<span class=\"tel\">tel : 044-233-2811</span> <span class=\"fax\">fax : 044-233-0818</span>\r\n<span class=\"email\">mail : info@wms.or.jp</span>', '本店', '', 'publish', 'open', 'closed', '', '%e6%9c%ac%e5%ba%97', '', '', '2019-09-17 06:48:57', '2019-09-17 06:48:57', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=access&#038;p=45', 0, 'access', '', 0),
(46, 1, '2019-09-17 04:51:37', '2019-09-17 04:51:37', '', 'map_01', '', 'inherit', 'open', 'closed', '', 'map_01', '', '', '2019-09-17 04:51:37', '2019-09-17 04:51:37', '', 45, 'http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/map_01.png', 0, 'attachment', 'image/png', 0),
(48, 1, '2019-09-17 06:48:56', '2019-09-17 06:48:56', '<p class=\"address\">〒210-0005　川崎市川崎区東田町8 パレール三井ビル8階</p>\n<p class=\"time\">JR川崎駅東口より徒歩7分　京急川崎駅より徒歩5分</p>\n<span class=\"tel\">tel : 044-233-2811</span> <span class=\"fax\">fax : 044-233-0818</span>\n<span class=\"email\">mail : info@wms.or.jp</span>', '本店', '', 'inherit', 'closed', 'closed', '', '45-autosave-v1', '', '', '2019-09-17 06:48:56', '2019-09-17 06:48:56', '', 45, 'http://localhost:8080/CamTu/Task007/2019/09/17/45-autosave-v1/', 0, 'revision', '', 0),
(49, 1, '2019-09-17 06:50:23', '2019-09-17 06:50:23', '<p class=\"address\">〒252-0231　相模原市中央区相模原3-8-25 第3JSビル7階</p>\r\n					<p class=\"time\">JR横浜線相模原駅より徒歩2分</p>\r\n					<br/>\r\n					<p>\r\n						<span class=\"tel\">tel : 042-704-9581</span> <span class=\"fax\">fax : 042-704-9582</span>\r\n						<br/>\r\n						<span class=\"email\"></span>\r\n					</p>', '相模原支店', '', 'publish', 'open', 'closed', '', '%e7%9b%b8%e6%a8%a1%e5%8e%9f%e6%94%af%e5%ba%97', '', '', '2019-09-17 06:50:54', '2019-09-17 06:50:54', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=access&#038;p=49', 0, 'access', '', 0),
(50, 1, '2019-09-17 06:50:04', '2019-09-17 06:50:04', '', 'map_02', '', 'inherit', 'open', 'closed', '', 'map_02', '', '', '2019-09-17 06:50:04', '2019-09-17 06:50:04', '', 49, 'http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/map_02.png', 0, 'attachment', 'image/png', 0),
(51, 1, '2019-09-17 08:41:15', '2019-09-17 08:41:15', '', 'Topics', '', 'publish', 'closed', 'closed', '', 'topics', '', '', '2019-09-17 08:41:15', '2019-09-17 08:41:15', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=51', 0, 'page', '', 0),
(52, 1, '2019-09-17 08:41:15', '2019-09-17 08:41:15', '', 'Topics', '', 'inherit', 'closed', 'closed', '', '51-revision-v1', '', '', '2019-09-17 08:41:15', '2019-09-17 08:41:15', '', 51, 'http://localhost:8080/CamTu/Task007/2019/09/17/51-revision-v1/', 0, 'revision', '', 0),
(53, 1, '2019-09-17 08:41:52', '2019-09-17 08:41:52', '', 'Topics', '', 'inherit', 'closed', 'closed', '', '51-autosave-v1', '', '', '2019-09-17 08:41:52', '2019-09-17 08:41:52', '', 51, 'http://localhost:8080/CamTu/Task007/2019/09/17/51-autosave-v1/', 0, 'revision', '', 0),
(54, 1, '2019-09-17 08:53:03', '2019-09-17 08:53:03', '<!-- wp:html -->\n<div class=\"c-title c-title--page\">\n			<h1>サービス</h1>\n		</div>\n		<div class=\"p-service__top\">\n			<p class=\"c-label\">Smile Service</p>\n			<p class=\"label_content\">私たちのサービスは、笑顔をつくります。</p>\n\n			<p class=\"note\">私たち下平会計事務所のすべてのサービスは、個人事業主様や会社の経営者様を笑顔にするものです。<br/>\n			笑顔のあるところにはヒトやモノが集まり、組織が活性化します。<br/>\n			私たちはこれまで、笑顔のチカラでお客様の成長・発展をサポートしてきました。<br/>\n			これから先もすべてのお客様に寄りそい、信頼の笑顔を育んでまいります。</p>\n		</div>\n		\n		<div class=\"p-service__content\">\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 1</span></h2>\n				<p>自社の経理事務に時間とコストをかけすぎているとお感じなら、<br/>\n				経理業務をアウトソーシングしてコストを抑えませんか。<br/>\n				日々の記帳代行や給与計算から試算表や決算書の作成まで、<br/>\n				税務・会計のプロである私たちがトータルでサポートいたします。</p>\n			</div>\n\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 2</span></h2>\n				<p>経営を強化する近道は、数字に強くなること。<br/>\n				成長する会社、永続する会社になるために、決算書をもとに<br/>\n				現状を正確に把握しましょう。そのうえでリスクを予測し、自社の強みを<br/>\n				生かせる戦略アイデアを出して、利益の確保をいっしょに考えます。</p>\n			</div>\n\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 3</span></h2>\n				<p>人の活性化・定着化にお悩みの場合も、私たちにご相談ください。<br/>\n				人事労務の視点と、税務会計の視点とを組み合わせ、<br/>\n				人材や社内体制などの問題を解決。<br/>\n				組織改善や企業文化づくりをサポートしていきます。</p>\n			</div>\n\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 4</span></h2>\n				<p>専門家とのネットワークを駆使して相続や贈与を<br/>\n				スムーズにするお手伝いをします。大きな課税対象となる不動産は、<br/>\n				評価額によって納税額に大きな差が出るもの。相続税・贈与税申告の実績が<br/>\n				豊富な私たちなら、お客様の不安を安心に変えていきます。</p>\n			</div>\n\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 5</span></h2>\n				<p>専門家とのネットワークを駆使して相続や贈与を<br/>\n				スムーズにするお手伝いをします。大きな課税対象となる不動産は、<br/>\n				評価額によって納税額に大きな差が出るもの。相続税・贈与税申告の実績が<br/>\n				豊富な私たちなら、お客様の不安を安心に変えていきます。</p>\n			</div>\n		</div>\n<!-- /wp:html -->', 'サービス', '', 'publish', 'closed', 'closed', '', '%e3%82%b5%e3%83%bc%e3%83%93%e3%82%b9', '', '', '2019-09-23 02:16:44', '2019-09-23 02:16:44', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=54', 0, 'page', '', 0),
(55, 1, '2019-09-17 08:53:03', '2019-09-17 08:53:03', '<!-- wp:html -->\n<div class=\"c-title c-title--page\">\n			<h1>サービス</h1>\n		</div>\n		<div class=\"p-service__top\">\n			<p class=\"c-label\">Smile Service</p>\n			<p class=\"label_content\">私たちのサービスは、笑顔をつくります。</p>\n\n			<p class=\"note\">私たち下平会計事務所のすべてのサービスは、個人事業主様や会社の経営者様を笑顔にするものです。<br/>\n			笑顔のあるところにはヒトやモノが集まり、組織が活性化します。<br/>\n			私たちはこれまで、笑顔のチカラでお客様の成長・発展をサポートしてきました。<br/>\n			これから先もすべてのお客様に寄りそい、信頼の笑顔を育んでまいります。</p>\n		</div>\n		\n		<div class=\"p-service__content\">\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 1</span></h2>\n				<p>自社の経理事務に時間とコストをかけすぎているとお感じなら、<br/>\n				経理業務をアウトソーシングしてコストを抑えませんか。<br/>\n				日々の記帳代行や給与計算から試算表や決算書の作成まで、<br/>\n				税務・会計のプロである私たちがトータルでサポートいたします。</p>\n			</div>\n\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 2</span></h2>\n				<p>経営を強化する近道は、数字に強くなること。<br/>\n				成長する会社、永続する会社になるために、決算書をもとに<br/>\n				現状を正確に把握しましょう。そのうえでリスクを予測し、自社の強みを<br/>\n				生かせる戦略アイデアを出して、利益の確保をいっしょに考えます。</p>\n			</div>\n\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 3</span></h2>\n				<p>人の活性化・定着化にお悩みの場合も、私たちにご相談ください。<br/>\n				人事労務の視点と、税務会計の視点とを組み合わせ、<br/>\n				人材や社内体制などの問題を解決。<br/>\n				組織改善や企業文化づくりをサポートしていきます。</p>\n			</div>\n\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 4</span></h2>\n				<p>専門家とのネットワークを駆使して相続や贈与を<br/>\n				スムーズにするお手伝いをします。大きな課税対象となる不動産は、<br/>\n				評価額によって納税額に大きな差が出るもの。相続税・贈与税申告の実績が<br/>\n				豊富な私たちなら、お客様の不安を安心に変えていきます。</p>\n			</div>\n\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 5</span></h2>\n				<p>専門家とのネットワークを駆使して相続や贈与を<br/>\n				スムーズにするお手伝いをします。大きな課税対象となる不動産は、<br/>\n				評価額によって納税額に大きな差が出るもの。相続税・贈与税申告の実績が<br/>\n				豊富な私たちなら、お客様の不安を安心に変えていきます。</p>\n			</div>\n		</div>\n<!-- /wp:html -->', 'Service', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2019-09-17 08:53:03', '2019-09-17 08:53:03', '', 54, 'http://localhost:8080/CamTu/Task007/2019/09/17/54-revision-v1/', 0, 'revision', '', 0),
(56, 1, '2019-09-17 08:58:00', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-17 08:58:00', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?p=56', 0, 'post', '', 0),
(57, 1, '2019-09-17 10:07:44', '2019-09-17 10:07:44', '<!-- wp:html -->\n<ul class=\"p-toppics__list\">\n			<li>\n				<span class=\"datepost\">2018/08/27</span>\n				<a href=\"cat.html\" class=\"cat\">特集記事</a>\n				<a class=\"title\" href=\"post.html\">新しい権利　配偶者終身居住権</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/08/22</span>\n				<a href=\"cat.html\" class=\"cat\">デイリーニュース</a>\n				<a class=\"title\" href=\"post.html\">介護保険の被保険者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/07/07</span>\n				<a href=\"cat.html\" class=\"cat\">デイリーニュース</a>\n				<a class=\"title\" href=\"post.html\">自然災害と中小企業支援策</a>\n			</li>	\n			<li>\n				<span class=\"datepost\">2018/06/20</span>\n				<a href=\"cat.html\" class=\"cat\">特集記事</a>\n				<a class=\"title\" href=\"post.html\">国税庁レポートから読み解く2018年度の重点事項</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/05/20</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a class=\"title\" class=\"title\" href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/04/30</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a class=\"title\" href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/04/20</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a class=\"title\" href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/03/23</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a class=\"title\" href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/02/20</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a class=\"title\" href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/01/20</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a class=\"title\" href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n		</ul>\n\n		<div class=\"c-pnav\">\n			<a href=\"\" class=\"first\"></a>\n			<a href=\"\" class=\"active\">1</a>\n			<a href=\"\">2</a>\n			<a href=\"\">3</a>\n			<a href=\"\">4</a>\n			<a href=\"\" class=\"last\"></a>\n		</div>\n<!-- /wp:html -->', 'Search Page', '', 'trash', 'closed', 'closed', '', 'search-page__trashed', '', '', '2019-09-23 07:49:16', '2019-09-23 07:49:16', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=57', 0, 'page', '', 0),
(58, 1, '2019-09-17 10:07:44', '2019-09-17 10:07:44', '<!-- wp:html -->\n<ul class=\"p-toppics__list\">\n			<li>\n				<span class=\"datepost\">2018/08/27</span>\n				<a href=\"cat.html\" class=\"cat\">特集記事</a>\n				<a href=\"post.html\">新しい権利　配偶者終身居住権</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/08/22</span>\n				<a href=\"cat.html\" class=\"cat\">デイリーニュース</a>\n				<a href=\"post.html\">介護保険の被保険者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/07/07</span>\n				<a href=\"cat.html\" class=\"cat\">デイリーニュース</a>\n				<a href=\"post.html\">自然災害と中小企業支援策</a>\n			</li>	\n			<li>\n				<span class=\"datepost\">2018/06/20</span>\n				<a href=\"cat.html\" class=\"cat\">特集記事</a>\n				<a href=\"post.html\">国税庁レポートから読み解く2018年度の重点事項</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/05/20</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/04/30</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/04/20</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/03/23</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/02/20</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/01/20</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n		</ul>\n\n		<div class=\"c-pnav\">\n			<a href=\"\" class=\"first\"></a>\n			<a href=\"\" class=\"active\">1</a>\n			<a href=\"\">2</a>\n			<a href=\"\">3</a>\n			<a href=\"\">4</a>\n			<a href=\"\" class=\"last\"></a>\n		</div>\n<!-- /wp:html -->', 'Search Page', '', 'inherit', 'closed', 'closed', '', '57-revision-v1', '', '', '2019-09-17 10:07:44', '2019-09-17 10:07:44', '', 57, 'http://localhost:8080/CamTu/Task007/2019/09/17/57-revision-v1/', 0, 'revision', '', 0),
(59, 1, '2019-09-17 10:11:48', '2019-09-17 10:11:48', '<!-- wp:html -->\n<ul class=\"p-toppics__list\">\n			<li>\n				<span class=\"datepost\">2018/08/27</span>\n				<a href=\"cat.html\" class=\"cat\">特集記事</a>\n				<a class=\"title\" href=\"post.html\">新しい権利　配偶者終身居住権</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/08/22</span>\n				<a href=\"cat.html\" class=\"cat\">デイリーニュース</a>\n				<a class=\"title\" href=\"post.html\">介護保険の被保険者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/07/07</span>\n				<a href=\"cat.html\" class=\"cat\">デイリーニュース</a>\n				<a class=\"title\" href=\"post.html\">自然災害と中小企業支援策</a>\n			</li>	\n			<li>\n				<span class=\"datepost\">2018/06/20</span>\n				<a href=\"cat.html\" class=\"cat\">特集記事</a>\n				<a class=\"title\" href=\"post.html\">国税庁レポートから読み解く2018年度の重点事項</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/05/20</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a class=\"title\" class=\"title\" href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/04/30</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a class=\"title\" href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/04/20</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a class=\"title\" href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/03/23</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a class=\"title\" href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/02/20</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a class=\"title\" href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n			<li>\n				<span class=\"datepost\">2018/01/20</span>\n				<a href=\"cat.html\" class=\"cat\">事務所ニュース</a>\n				<a class=\"title\" href=\"post.html\">働き方改革”と管理者</a>\n			</li>\n		</ul>\n\n		<div class=\"c-pnav\">\n			<a href=\"\" class=\"first\"></a>\n			<a href=\"\" class=\"active\">1</a>\n			<a href=\"\">2</a>\n			<a href=\"\">3</a>\n			<a href=\"\">4</a>\n			<a href=\"\" class=\"last\"></a>\n		</div>\n<!-- /wp:html -->', 'Search Page', '', 'inherit', 'closed', 'closed', '', '57-revision-v1', '', '', '2019-09-17 10:11:48', '2019-09-17 10:11:48', '', 57, 'http://localhost:8080/CamTu/Task007/2019/09/17/57-revision-v1/', 0, 'revision', '', 0),
(60, 1, '2019-09-17 10:16:57', '2019-09-17 10:16:57', '<!-- wp:heading {\"level\":3} -->\n<h3> 新しい法定された権利の創設 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 評価額と権利の性質 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 所得税への影響 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->', '新しい権利　配偶者終身居住権', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2019-09-17 10:16:57', '2019-09-17 10:16:57', '', 43, 'http://localhost:8080/CamTu/Task007/2019/09/17/43-revision-v1/', 0, 'revision', '', 0),
(61, 1, '2019-09-17 10:17:42', '2019-09-17 10:17:42', '', 'dummyimg', '', 'inherit', 'open', 'closed', '', 'dummyimg', '', '', '2019-09-17 10:17:42', '2019-09-17 10:17:42', '', 43, 'http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/dummyimg.png', 0, 'attachment', 'image/png', 0),
(62, 1, '2019-09-17 10:17:46', '2019-09-17 10:17:46', '<!-- wp:image {\"id\":61} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/dummyimg.png\" alt=\"\" class=\"wp-image-61\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 新しい法定された権利の創設 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 評価額と権利の性質 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 所得税への影響 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->', '新しい権利　配偶者終身居住権', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2019-09-17 10:17:46', '2019-09-17 10:17:46', '', 43, 'http://localhost:8080/CamTu/Task007/2019/09/17/43-revision-v1/', 0, 'revision', '', 0),
(63, 1, '2019-09-17 10:18:43', '2019-09-17 10:18:43', '<!-- wp:image {\"id\":61} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/dummyimg.png\" alt=\"\" class=\"wp-image-61\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 新しい法定された権利の創設 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 評価額と権利の性質 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 所得税への影響 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->', '介護保険の被保険者', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2019-09-17 10:18:43', '2019-09-17 10:18:43', '', 40, 'http://localhost:8080/CamTu/Task007/2019/09/17/40-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(64, 1, '2019-09-17 10:19:12', '2019-09-17 10:19:12', '<!-- wp:image {\"id\":61} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/dummyimg.png\" alt=\"\" class=\"wp-image-61\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 新しい法定された権利の創設 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 評価額と権利の性質 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 所得税への影響 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->', '自然災害と中小企業支援策', '', 'inherit', 'closed', 'closed', '', '38-revision-v1', '', '', '2019-09-17 10:19:12', '2019-09-17 10:19:12', '', 38, 'http://localhost:8080/CamTu/Task007/2019/09/17/38-revision-v1/', 0, 'revision', '', 0),
(65, 1, '2019-09-17 10:20:02', '2019-09-17 10:20:02', '<!-- wp:image {\"id\":61} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/dummyimg.png\" alt=\"\" class=\"wp-image-61\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 新しい法定された権利の創設 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 評価額と権利の性質 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 所得税への影響 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->', '国税庁レポートから読み解く2018年度の重点事項', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2019-09-17 10:20:02', '2019-09-17 10:20:02', '', 36, 'http://localhost:8080/CamTu/Task007/2019/09/17/36-revision-v1/', 0, 'revision', '', 0),
(66, 1, '2019-09-17 10:20:46', '2019-09-17 10:20:46', '<!-- wp:image {\"id\":61} -->\n<figure class=\"wp-block-image\"><img src=\"http://localhost:8080/CamTu/Task007/wp-content/uploads/2019/09/dummyimg.png\" alt=\"\" class=\"wp-image-61\"/></figure>\n<!-- /wp:image -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 新しい法定された権利の創設 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 評価額と権利の性質 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> 所得税への影響 </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> 民法が改正され、配偶者終身居住権が創設されました。被相続人の配偶者が自宅に住み続けることができる権利で、高齢化が進む中、残された配偶者の住居や生活費を確保し易くする、というのが狙いです。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 子が自宅の所有権を相続し、被相続人の配偶者が終身居住権を相続する、というのが最も典型的な予想ケースとされています。 </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p> 所有権が第三者に渡っても、そのまま自宅に住み続けることができる、という排他的権利です。 </p>\n<!-- /wp:paragraph -->', '働き方改革”と管理者', '', 'inherit', 'closed', 'closed', '', '34-revision-v1', '', '', '2019-09-17 10:20:46', '2019-09-17 10:20:46', '', 34, 'http://localhost:8080/CamTu/Task007/2019/09/17/34-revision-v1/', 0, 'revision', '', 0),
(67, 1, '2019-09-18 01:02:47', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-18 01:02:47', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?p=67', 0, 'post', '', 0),
(68, 1, '2018-08-20 01:04:51', '2018-08-20 01:04:51', '', '働き方改革”と管理者', '', 'publish', 'open', 'open', '', '%e5%83%8d%e3%81%8d%e6%96%b9%e6%94%b9%e9%9d%a9%e3%81%a8%e7%ae%a1%e7%90%86%e8%80%85-2', '', '', '2019-09-18 01:05:20', '2019-09-18 01:05:20', '', 0, 'http://localhost:8080/CamTu/Task007/?p=68', 0, 'post', '', 0),
(69, 1, '2019-09-18 01:04:51', '2019-09-18 01:04:51', '', '働き方改革”と管理者', '', 'inherit', 'closed', 'closed', '', '68-revision-v1', '', '', '2019-09-18 01:04:51', '2019-09-18 01:04:51', '', 68, 'http://localhost:8080/CamTu/Task007/2019/09/18/68-revision-v1/', 0, 'revision', '', 0),
(70, 1, '2018-08-20 01:07:43', '2018-08-20 01:07:43', '', '働き方改革”と管理者', '', 'publish', 'open', 'open', '', '%e5%83%8d%e3%81%8d%e6%96%b9%e6%94%b9%e9%9d%a9%e3%81%a8%e7%ae%a1%e7%90%86%e8%80%85-3', '', '', '2019-09-18 01:08:05', '2019-09-18 01:08:05', '', 0, 'http://localhost:8080/CamTu/Task007/?p=70', 0, 'post', '', 0),
(71, 1, '2019-09-18 01:07:43', '2019-09-18 01:07:43', '', '働き方改革”と管理者', '', 'inherit', 'closed', 'closed', '', '70-revision-v1', '', '', '2019-09-18 01:07:43', '2019-09-18 01:07:43', '', 70, 'http://localhost:8080/CamTu/Task007/2019/09/18/70-revision-v1/', 0, 'revision', '', 0),
(72, 1, '2018-08-20 01:08:29', '2018-08-20 01:08:29', '', '働き方改革”と管理者', '', 'publish', 'open', 'open', '', '%e5%83%8d%e3%81%8d%e6%96%b9%e6%94%b9%e9%9d%a9%e3%81%a8%e7%ae%a1%e7%90%86%e8%80%85-4', '', '', '2019-09-18 01:19:39', '2019-09-18 01:19:39', '', 0, 'http://localhost:8080/CamTu/Task007/?p=72', 0, 'post', '', 0),
(73, 1, '2019-09-18 01:08:29', '2019-09-18 01:08:29', '', '働き方改革”と管理者', '', 'inherit', 'closed', 'closed', '', '72-revision-v1', '', '', '2019-09-18 01:08:29', '2019-09-18 01:08:29', '', 72, 'http://localhost:8080/CamTu/Task007/2019/09/18/72-revision-v1/', 0, 'revision', '', 0),
(74, 1, '2019-09-18 01:08:53', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-18 01:08:53', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?p=74', 0, 'post', '', 0),
(75, 1, '2018-08-20 01:09:02', '2018-08-20 01:09:02', '', '働き方改革”と管理者', '', 'publish', 'open', 'open', '', '%e5%83%8d%e3%81%8d%e6%96%b9%e6%94%b9%e9%9d%a9%e3%81%a8%e7%ae%a1%e7%90%86%e8%80%85-5', '', '', '2019-09-18 01:09:25', '2019-09-18 01:09:25', '', 0, 'http://localhost:8080/CamTu/Task007/?p=75', 0, 'post', '', 0),
(76, 1, '2019-09-18 01:09:02', '2019-09-18 01:09:02', '', '働き方改革”と管理者', '', 'inherit', 'closed', 'closed', '', '75-revision-v1', '', '', '2019-09-18 01:09:02', '2019-09-18 01:09:02', '', 75, 'http://localhost:8080/CamTu/Task007/2019/09/18/75-revision-v1/', 0, 'revision', '', 0),
(77, 1, '2018-08-20 01:09:53', '2018-08-20 01:09:53', '', '働き方改革”と管理者', '', 'publish', 'open', 'open', '', '%e5%83%8d%e3%81%8d%e6%96%b9%e6%94%b9%e9%9d%a9%e3%81%a8%e7%ae%a1%e7%90%86%e8%80%85-6', '', '', '2019-09-18 01:10:11', '2019-09-18 01:10:11', '', 0, 'http://localhost:8080/CamTu/Task007/?p=77', 0, 'post', '', 0),
(78, 1, '2019-09-18 01:09:53', '2019-09-18 01:09:53', '', '働き方改革”と管理者', '', 'inherit', 'closed', 'closed', '', '77-revision-v1', '', '', '2019-09-18 01:09:53', '2019-09-18 01:09:53', '', 77, 'http://localhost:8080/CamTu/Task007/2019/09/18/77-revision-v1/', 0, 'revision', '', 0),
(79, 1, '2018-08-20 01:10:29', '2018-08-20 01:10:29', '', '働き方改革”と管理者', '', 'publish', 'open', 'open', '', '%e5%83%8d%e3%81%8d%e6%96%b9%e6%94%b9%e9%9d%a9%e3%81%a8%e7%ae%a1%e7%90%86%e8%80%85-7', '', '', '2019-09-18 01:10:42', '2019-09-18 01:10:42', '', 0, 'http://localhost:8080/CamTu/Task007/?p=79', 0, 'post', '', 0),
(80, 1, '2019-09-18 01:10:29', '2019-09-18 01:10:29', '', '働き方改革”と管理者', '', 'inherit', 'closed', 'closed', '', '79-revision-v1', '', '', '2019-09-18 01:10:29', '2019-09-18 01:10:29', '', 79, 'http://localhost:8080/CamTu/Task007/2019/09/18/79-revision-v1/', 0, 'revision', '', 0),
(81, 1, '2019-09-18 01:10:46', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-18 01:10:46', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?p=81', 0, 'post', '', 0),
(82, 1, '2018-08-20 01:11:03', '2018-08-20 01:11:03', '', '働き方改革”と管理者', '', 'publish', 'open', 'open', '', '%e5%83%8d%e3%81%8d%e6%96%b9%e6%94%b9%e9%9d%a9%e3%81%a8%e7%ae%a1%e7%90%86%e8%80%85-8', '', '', '2019-09-18 01:11:25', '2019-09-18 01:11:25', '', 0, 'http://localhost:8080/CamTu/Task007/?p=82', 0, 'post', '', 0),
(83, 1, '2019-09-18 01:11:03', '2019-09-18 01:11:03', '', '働き方改革”と管理者', '', 'inherit', 'closed', 'closed', '', '82-revision-v1', '', '', '2019-09-18 01:11:03', '2019-09-18 01:11:03', '', 82, 'http://localhost:8080/CamTu/Task007/2019/09/18/82-revision-v1/', 0, 'revision', '', 0),
(84, 1, '2018-08-20 01:11:52', '2018-08-20 01:11:52', '', '働き方改革”と管理者', '', 'publish', 'open', 'open', '', '%e5%83%8d%e3%81%8d%e6%96%b9%e6%94%b9%e9%9d%a9%e3%81%a8%e7%ae%a1%e7%90%86%e8%80%85-9', '', '', '2019-09-18 01:12:08', '2019-09-18 01:12:08', '', 0, 'http://localhost:8080/CamTu/Task007/?p=84', 0, 'post', '', 0),
(85, 1, '2019-09-18 01:11:52', '2019-09-18 01:11:52', '', '働き方改革”と管理者', '', 'inherit', 'closed', 'closed', '', '84-revision-v1', '', '', '2019-09-18 01:11:52', '2019-09-18 01:11:52', '', 84, 'http://localhost:8080/CamTu/Task007/2019/09/18/84-revision-v1/', 0, 'revision', '', 0),
(86, 1, '2018-08-20 01:12:22', '2018-08-20 01:12:22', '', '働き方改革”と管理者', '', 'publish', 'open', 'open', '', '%e5%83%8d%e3%81%8d%e6%96%b9%e6%94%b9%e9%9d%a9%e3%81%a8%e7%ae%a1%e7%90%86%e8%80%85-10', '', '', '2019-09-18 01:12:37', '2019-09-18 01:12:37', '', 0, 'http://localhost:8080/CamTu/Task007/?p=86', 0, 'post', '', 0),
(87, 1, '2019-09-18 01:12:22', '2019-09-18 01:12:22', '', '働き方改革”と管理者', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2019-09-18 01:12:22', '2019-09-18 01:12:22', '', 86, 'http://localhost:8080/CamTu/Task007/2019/09/18/86-revision-v1/', 0, 'revision', '', 0),
(88, 1, '2018-08-20 01:41:00', '2018-08-20 01:41:00', '', '働き方改革”と管理者', '', 'publish', 'open', 'open', '', '%e5%83%8d%e3%81%8d%e6%96%b9%e6%94%b9%e9%9d%a9%e3%81%a8%e7%ae%a1%e7%90%86%e8%80%85-11', '', '', '2019-09-18 01:41:14', '2019-09-18 01:41:14', '', 0, 'http://localhost:8080/CamTu/Task007/?p=88', 0, 'post', '', 0),
(89, 1, '2019-09-18 01:41:00', '2019-09-18 01:41:00', '', '働き方改革”と管理者', '', 'inherit', 'closed', 'closed', '', '88-revision-v1', '', '', '2019-09-18 01:41:00', '2019-09-18 01:41:00', '', 88, 'http://localhost:8080/CamTu/Task007/2019/09/18/88-revision-v1/', 0, 'revision', '', 0),
(90, 1, '2019-09-18 08:56:41', '2019-09-18 08:56:41', '<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost:8080/CamTu/Task007/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->', 'Sample Page', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-09-18 08:56:41', '2019-09-18 08:56:41', '', 2, 'http://localhost:8080/CamTu/Task007/2019/09/18/2-revision-v1/', 0, 'revision', '', 0),
(91, 1, '2019-09-18 09:19:44', '2019-09-18 09:19:44', '', 'Slider', '', 'publish', 'closed', 'closed', '', 'slider', '', '', '2019-09-18 09:29:46', '2019-09-18 09:29:46', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=os_bxslider&#038;p=91', 0, 'os_bxslider', '', 0),
(92, 1, '2019-09-18 09:28:23', '2019-09-18 09:28:23', '', 'Slider', '', 'inherit', 'closed', 'closed', '', '91-autosave-v1', '', '', '2019-09-18 09:28:23', '2019-09-18 09:28:23', '', 91, 'http://localhost:8080/CamTu/Task007/2019/09/18/91-autosave-v1/', 0, 'revision', '', 0),
(93, 1, '2019-09-18 09:44:57', '2019-09-18 09:44:57', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"default\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Access Map', 'access-map', 'trash', 'closed', 'closed', '', 'group_5d81fa706d8e8__trashed', '', '', '2019-09-23 04:14:39', '2019-09-23 04:14:39', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field-group&#038;p=93', 0, 'acf-field-group', '', 0),
(94, 1, '2019-09-18 09:44:57', '2019-09-18 09:44:57', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:20:\"c-title c-title--sub\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title Map', 'title_map', 'trash', 'closed', 'closed', '', 'field_5d81fac6f28ed__trashed', '', '', '2019-09-23 04:14:39', '2019-09-23 04:14:39', '', 93, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=94', 0, 'acf-field', '', 0),
(95, 1, '2019-09-18 09:44:57', '2019-09-18 09:44:57', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:7:\"address\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Address', 'address', 'trash', 'closed', 'closed', '', 'field_5d81fb47f28ee__trashed', '', '', '2019-09-23 04:14:39', '2019-09-23 04:14:39', '', 93, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=95', 1, 'acf-field', '', 0),
(96, 1, '2019-09-18 09:44:57', '2019-09-18 09:44:57', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:4:\"time\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Time', 'time', 'trash', 'closed', 'closed', '', 'field_5d81fb68f28ef__trashed', '', '', '2019-09-23 04:14:39', '2019-09-23 04:14:39', '', 93, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=96', 2, 'acf-field', '', 0),
(97, 1, '2019-09-18 09:44:57', '2019-09-18 09:44:57', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:3:\"tel\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tel', 'tel', 'trash', 'closed', 'closed', '', 'field_5d81fbdcf28f0__trashed', '', '', '2019-09-23 04:14:39', '2019-09-23 04:14:39', '', 93, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=97', 3, 'acf-field', '', 0),
(98, 1, '2019-09-18 09:44:57', '2019-09-18 09:44:57', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:3:\"fax\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Fax', 'fax', 'trash', 'closed', 'closed', '', 'field_5d81fbf8f28f1__trashed', '', '', '2019-09-23 04:14:39', '2019-09-23 04:14:39', '', 93, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=98', 4, 'acf-field', '', 0),
(99, 1, '2019-09-18 09:44:57', '2019-09-18 09:44:57', 'a:9:{s:4:\"type\";s:5:\"email\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:5:\"email\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";}', 'Email', 'email', 'trash', 'closed', 'closed', '', 'field_5d81fc36f28f2__trashed', '', '', '2019-09-23 04:14:40', '2019-09-23 04:14:40', '', 93, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=99', 5, 'acf-field', '', 0),
(100, 1, '2019-09-18 09:44:57', '2019-09-18 09:44:57', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image Map', 'image_map', 'trash', 'closed', 'closed', '', 'field_5d81fc56f28f3__trashed', '', '', '2019-09-23 04:14:40', '2019-09-23 04:14:40', '', 93, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=100', 6, 'acf-field', '', 0),
(101, 1, '2018-01-18 09:47:28', '2018-01-18 09:47:28', '', '本店', '', 'trash', 'open', 'open', '', '%e6%9c%ac%e5%ba%97__trashed', '', '', '2019-09-23 06:43:49', '2019-09-23 06:43:49', '', 0, 'http://localhost:8080/CamTu/Task007/?p=101', 0, 'post', '', 0),
(102, 1, '2019-09-18 09:47:28', '2019-09-18 09:47:28', '', '本店', '', 'inherit', 'closed', 'closed', '', '101-revision-v1', '', '', '2019-09-18 09:47:28', '2019-09-18 09:47:28', '', 101, 'http://localhost:8080/CamTu/Task007/2019/09/18/101-revision-v1/', 0, 'revision', '', 0),
(103, 1, '2019-09-18 09:47:30', '2019-09-18 09:47:30', '', '本店', '', 'inherit', 'closed', 'closed', '', '101-revision-v1', '', '', '2019-09-18 09:47:30', '2019-09-18 09:47:30', '', 101, 'http://localhost:8080/CamTu/Task007/2019/09/18/101-revision-v1/', 0, 'revision', '', 0),
(104, 1, '2019-09-18 10:04:47', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-18 10:04:47', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field-group&p=104', 0, 'acf-field-group', '', 0),
(105, 1, '2019-09-19 01:31:30', '2019-09-19 01:31:30', '', 'Slider', '', 'publish', 'closed', 'closed', '', 'new-slideshow', '', '', '2019-09-19 01:44:06', '2019-09-19 01:44:06', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=ml-slider&#038;p=105', 0, 'ml-slider', '', 0),
(106, 1, '2019-09-19 01:31:42', '2019-09-19 01:31:42', '', 'Slider 105 - image', '', 'publish', 'closed', 'closed', '', 'slider-105-image', '', '', '2019-09-19 01:44:06', '2019-09-19 01:44:06', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=ml-slide&#038;p=106', 0, 'ml-slide', '', 0),
(107, 1, '2019-09-19 01:31:42', '2019-09-19 01:31:42', '', 'Slider 105 - image', '', 'publish', 'closed', 'closed', '', 'slider-105-image-2', '', '', '2019-09-19 01:44:06', '2019-09-19 01:44:06', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=ml-slide&#038;p=107', 1, 'ml-slide', '', 0),
(108, 1, '2019-09-19 01:31:43', '2019-09-19 01:31:43', '', 'Slider 105 - image', '', 'publish', 'closed', 'closed', '', 'slider-105-image-3', '', '', '2019-09-19 01:44:06', '2019-09-19 01:44:06', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=ml-slide&#038;p=108', 2, 'ml-slide', '', 0),
(109, 1, '2019-09-19 01:31:43', '2019-09-19 01:31:43', '', 'Slider 105 - image', '', 'publish', 'closed', 'closed', '', 'slider-105-image-4', '', '', '2019-09-19 01:44:06', '2019-09-19 01:44:06', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=ml-slide&#038;p=109', 3, 'ml-slide', '', 0),
(120, 1, '2018-01-19 03:14:24', '2018-01-19 03:14:24', '', '相模原支店', '', 'trash', 'open', 'open', '', '%e7%9b%b8%e6%a8%a1%e5%8e%9f%e6%94%af%e5%ba%97__trashed', '', '', '2019-09-23 06:44:16', '2019-09-23 06:44:16', '', 0, 'http://localhost:8080/CamTu/Task007/?p=120', 0, 'post', '', 0),
(121, 1, '2019-09-19 03:14:24', '2019-09-19 03:14:24', '', '相模原支店', '', 'inherit', 'closed', 'closed', '', '120-revision-v1', '', '', '2019-09-19 03:14:24', '2019-09-19 03:14:24', '', 120, 'http://localhost:8080/CamTu/Task007/2019/09/19/120-revision-v1/', 0, 'revision', '', 0),
(122, 1, '2019-09-19 03:14:26', '2019-09-19 03:14:26', '', '相模原支店', '', 'inherit', 'closed', 'closed', '', '120-revision-v1', '', '', '2019-09-19 03:14:26', '2019-09-19 03:14:26', '', 120, 'http://localhost:8080/CamTu/Task007/2019/09/19/120-revision-v1/', 0, 'revision', '', 0),
(123, 1, '2019-09-23 02:09:03', '2019-09-23 02:09:03', '', '私たちの想い', '', 'publish', 'closed', 'closed', '', '%e7%a7%81%e3%81%9f%e3%81%a1%e3%81%ae%e6%83%b3%e3%81%84', '', '', '2019-09-23 02:09:03', '2019-09-23 02:09:03', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=123', 0, 'page', '', 0),
(124, 1, '2019-09-23 02:09:03', '2019-09-23 02:09:03', '', '私たちの想い', '', 'inherit', 'closed', 'closed', '', '123-revision-v1', '', '', '2019-09-23 02:09:03', '2019-09-23 02:09:03', '', 123, 'http://localhost:8080/CamTu/Task007/2019/09/23/123-revision-v1/', 0, 'revision', '', 0),
(125, 1, '2019-09-23 02:09:29', '2019-09-23 02:09:29', '', '6つの強み', '', 'publish', 'closed', 'closed', '', '6%e3%81%a4%e3%81%ae%e5%bc%b7%e3%81%bf', '', '', '2019-09-23 02:09:29', '2019-09-23 02:09:29', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=125', 0, 'page', '', 0),
(126, 1, '2019-09-23 02:09:29', '2019-09-23 02:09:29', '', '6つの強み', '', 'inherit', 'closed', 'closed', '', '125-revision-v1', '', '', '2019-09-23 02:09:29', '2019-09-23 02:09:29', '', 125, 'http://localhost:8080/CamTu/Task007/2019/09/23/125-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2019-09-23 02:09:49', '2019-09-23 02:09:49', '', '所員の紹介', '', 'publish', 'closed', 'closed', '', '%e6%89%80%e5%93%a1%e3%81%ae%e7%b4%b9%e4%bb%8b', '', '', '2019-09-23 02:09:49', '2019-09-23 02:09:49', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=127', 0, 'page', '', 0),
(128, 1, '2019-09-23 02:09:49', '2019-09-23 02:09:49', '', '所員の紹介', '', 'inherit', 'closed', 'closed', '', '127-revision-v1', '', '', '2019-09-23 02:09:49', '2019-09-23 02:09:49', '', 127, 'http://localhost:8080/CamTu/Task007/2019/09/23/127-revision-v1/', 0, 'revision', '', 0),
(129, 1, '2019-09-23 02:10:11', '2019-09-23 02:10:11', '', '事務所案内', '', 'publish', 'closed', 'closed', '', '%e4%ba%8b%e5%8b%99%e6%89%80%e6%a1%88%e5%86%85', '', '', '2019-09-23 02:10:11', '2019-09-23 02:10:11', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=129', 0, 'page', '', 0),
(130, 1, '2019-09-23 02:10:11', '2019-09-23 02:10:11', '', '事務所案内', '', 'inherit', 'closed', 'closed', '', '129-revision-v1', '', '', '2019-09-23 02:10:11', '2019-09-23 02:10:11', '', 129, 'http://localhost:8080/CamTu/Task007/2019/09/23/129-revision-v1/', 0, 'revision', '', 0),
(131, 1, '2019-09-23 02:10:32', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-23 02:10:32', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?p=131', 0, 'post', '', 0),
(132, 1, '2019-09-23 02:11:01', '2019-09-23 02:11:01', '<!-- wp:html -->\n<div class=\"c-title c-title--page\">\n			<h1>サービス</h1>\n		</div>\n		<div class=\"p-service__top\">\n			<p class=\"c-label\">Smile Service</p>\n			<p class=\"label_content\">私たちのサービスは、笑顔をつくります。</p>\n\n			<p class=\"note\">私たち下平会計事務所のすべてのサービスは、個人事業主様や会社の経営者様を笑顔にするものです。<br/>\n			笑顔のあるところにはヒトやモノが集まり、組織が活性化します。<br/>\n			私たちはこれまで、笑顔のチカラでお客様の成長・発展をサポートしてきました。<br/>\n			これから先もすべてのお客様に寄りそい、信頼の笑顔を育んでまいります。</p>\n		</div>\n		\n		<div class=\"p-service__content\">\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 1</span></h2>\n				<p>自社の経理事務に時間とコストをかけすぎているとお感じなら、<br/>\n				経理業務をアウトソーシングしてコストを抑えませんか。<br/>\n				日々の記帳代行や給与計算から試算表や決算書の作成まで、<br/>\n				税務・会計のプロである私たちがトータルでサポートいたします。</p>\n			</div>\n\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 2</span></h2>\n				<p>経営を強化する近道は、数字に強くなること。<br/>\n				成長する会社、永続する会社になるために、決算書をもとに<br/>\n				現状を正確に把握しましょう。そのうえでリスクを予測し、自社の強みを<br/>\n				生かせる戦略アイデアを出して、利益の確保をいっしょに考えます。</p>\n			</div>\n\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 3</span></h2>\n				<p>人の活性化・定着化にお悩みの場合も、私たちにご相談ください。<br/>\n				人事労務の視点と、税務会計の視点とを組み合わせ、<br/>\n				人材や社内体制などの問題を解決。<br/>\n				組織改善や企業文化づくりをサポートしていきます。</p>\n			</div>\n\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 4</span></h2>\n				<p>専門家とのネットワークを駆使して相続や贈与を<br/>\n				スムーズにするお手伝いをします。大きな課税対象となる不動産は、<br/>\n				評価額によって納税額に大きな差が出るもの。相続税・贈与税申告の実績が<br/>\n				豊富な私たちなら、お客様の不安を安心に変えていきます。</p>\n			</div>\n\n			<div class=\"service_items\">\n				<h2 class=\"c-title--block\"><span class=\"c-label c-label--white\">Smile Service 5</span></h2>\n				<p>専門家とのネットワークを駆使して相続や贈与を<br/>\n				スムーズにするお手伝いをします。大きな課税対象となる不動産は、<br/>\n				評価額によって納税額に大きな差が出るもの。相続税・贈与税申告の実績が<br/>\n				豊富な私たちなら、お客様の不安を安心に変えていきます。</p>\n			</div>\n		</div>\n<!-- /wp:html -->', 'サービス', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2019-09-23 02:11:01', '2019-09-23 02:11:01', '', 54, 'http://localhost:8080/CamTu/Task007/2019/09/23/54-revision-v1/', 0, 'revision', '', 0),
(133, 1, '2019-09-23 02:12:40', '2019-09-23 02:12:40', ' ', '', '', 'publish', 'closed', 'closed', '', '133', '', '', '2019-09-23 02:12:40', '2019-09-23 02:12:40', '', 0, 'http://localhost:8080/CamTu/Task007/?p=133', 5, 'nav_menu_item', '', 0),
(134, 1, '2019-09-23 02:12:40', '2019-09-23 02:12:40', ' ', '', '', 'publish', 'closed', 'closed', '', '134', '', '', '2019-09-23 02:12:40', '2019-09-23 02:12:40', '', 0, 'http://localhost:8080/CamTu/Task007/?p=134', 4, 'nav_menu_item', '', 0),
(135, 1, '2019-09-23 02:12:39', '2019-09-23 02:12:39', ' ', '', '', 'publish', 'closed', 'closed', '', '135', '', '', '2019-09-23 02:12:39', '2019-09-23 02:12:39', '', 0, 'http://localhost:8080/CamTu/Task007/?p=135', 2, 'nav_menu_item', '', 0),
(136, 1, '2019-09-23 02:12:39', '2019-09-23 02:12:39', ' ', '', '', 'publish', 'closed', 'closed', '', '136', '', '', '2019-09-23 02:12:39', '2019-09-23 02:12:39', '', 0, 'http://localhost:8080/CamTu/Task007/?p=136', 1, 'nav_menu_item', '', 0),
(137, 1, '2019-09-23 02:12:40', '2019-09-23 02:12:40', ' ', '', '', 'publish', 'closed', 'closed', '', '137', '', '', '2019-09-23 02:12:40', '2019-09-23 02:12:40', '', 0, 'http://localhost:8080/CamTu/Task007/?p=137', 3, 'nav_menu_item', '', 0),
(138, 1, '2019-09-23 02:23:44', '2019-09-23 02:23:44', '', 'トップページ', '', 'publish', 'closed', 'closed', '', '%e3%83%88%e3%83%83%e3%83%97%e3%83%9a%e3%83%bc%e3%82%b8', '', '', '2019-09-23 02:23:44', '2019-09-23 02:23:44', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=138', 0, 'page', '', 0),
(139, 1, '2019-09-23 02:23:44', '2019-09-23 02:23:44', '', 'トップページ', '', 'inherit', 'closed', 'closed', '', '138-revision-v1', '', '', '2019-09-23 02:23:44', '2019-09-23 02:23:44', '', 138, 'http://localhost:8080/CamTu/Task007/2019/09/23/138-revision-v1/', 0, 'revision', '', 0),
(140, 1, '2019-09-23 02:23:56', '2019-09-23 02:23:56', '', 'お問合せ', '', 'publish', 'closed', 'closed', '', '%e3%81%8a%e5%95%8f%e5%90%88%e3%81%9b', '', '', '2019-09-23 02:23:56', '2019-09-23 02:23:56', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=140', 0, 'page', '', 0),
(141, 1, '2019-09-23 02:23:56', '2019-09-23 02:23:56', '', 'お問合せ', '', 'inherit', 'closed', 'closed', '', '140-revision-v1', '', '', '2019-09-23 02:23:56', '2019-09-23 02:23:56', '', 140, 'http://localhost:8080/CamTu/Task007/2019/09/23/140-revision-v1/', 0, 'revision', '', 0),
(142, 1, '2019-09-23 02:24:21', '2019-09-23 02:24:21', '', '個人情報保護方針', '', 'publish', 'closed', 'closed', '', '%e5%80%8b%e4%ba%ba%e6%83%85%e5%a0%b1%e4%bf%9d%e8%ad%b7%e6%96%b9%e9%87%9d', '', '', '2019-09-23 02:24:21', '2019-09-23 02:24:21', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=142', 0, 'page', '', 0),
(143, 1, '2019-09-23 02:24:21', '2019-09-23 02:24:21', '', '個人情報保護方針', '', 'inherit', 'closed', 'closed', '', '142-revision-v1', '', '', '2019-09-23 02:24:21', '2019-09-23 02:24:21', '', 142, 'http://localhost:8080/CamTu/Task007/2019/09/23/142-revision-v1/', 0, 'revision', '', 0),
(144, 1, '2019-09-23 02:26:08', '2019-09-23 02:26:08', ' ', '', '', 'publish', 'closed', 'closed', '', '144', '', '', '2019-09-23 02:26:08', '2019-09-23 02:26:08', '', 0, 'http://localhost:8080/CamTu/Task007/?p=144', 9, 'nav_menu_item', '', 0),
(145, 1, '2019-09-23 02:26:07', '2019-09-23 02:26:07', ' ', '', '', 'publish', 'closed', 'closed', '', '145', '', '', '2019-09-23 02:26:07', '2019-09-23 02:26:07', '', 0, 'http://localhost:8080/CamTu/Task007/?p=145', 8, 'nav_menu_item', '', 0),
(146, 1, '2019-09-23 02:26:06', '2019-09-23 02:26:06', ' ', '', '', 'publish', 'closed', 'closed', '', '146', '', '', '2019-09-23 02:26:06', '2019-09-23 02:26:06', '', 0, 'http://localhost:8080/CamTu/Task007/?p=146', 1, 'nav_menu_item', '', 0),
(147, 1, '2019-09-23 02:26:06', '2019-09-23 02:26:06', ' ', '', '', 'publish', 'closed', 'closed', '', '147', '', '', '2019-09-23 02:26:06', '2019-09-23 02:26:06', '', 0, 'http://localhost:8080/CamTu/Task007/?p=147', 2, 'nav_menu_item', '', 0),
(148, 1, '2019-09-23 02:26:07', '2019-09-23 02:26:07', ' ', '', '', 'publish', 'closed', 'closed', '', '148', '', '', '2019-09-23 02:26:07', '2019-09-23 02:26:07', '', 0, 'http://localhost:8080/CamTu/Task007/?p=148', 6, 'nav_menu_item', '', 0),
(149, 1, '2019-09-23 02:26:06', '2019-09-23 02:26:06', ' ', '', '', 'publish', 'closed', 'closed', '', '149', '', '', '2019-09-23 02:26:06', '2019-09-23 02:26:06', '', 0, 'http://localhost:8080/CamTu/Task007/?p=149', 4, 'nav_menu_item', '', 0),
(150, 1, '2019-09-23 02:26:06', '2019-09-23 02:26:06', ' ', '', '', 'publish', 'closed', 'closed', '', '150', '', '', '2019-09-23 02:26:06', '2019-09-23 02:26:06', '', 0, 'http://localhost:8080/CamTu/Task007/?p=150', 3, 'nav_menu_item', '', 0),
(151, 1, '2019-09-23 02:26:07', '2019-09-23 02:26:07', ' ', '', '', 'publish', 'closed', 'closed', '', '151', '', '', '2019-09-23 02:26:07', '2019-09-23 02:26:07', '', 0, 'http://localhost:8080/CamTu/Task007/?p=151', 5, 'nav_menu_item', '', 0),
(152, 1, '2019-09-23 02:26:07', '2019-09-23 02:26:07', ' ', '', '', 'publish', 'closed', 'closed', '', '152', '', '', '2019-09-23 02:26:07', '2019-09-23 02:26:07', '', 0, 'http://localhost:8080/CamTu/Task007/?p=152', 7, 'nav_menu_item', '', 0),
(153, 1, '2019-09-23 02:43:31', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-23 02:43:31', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field-group&p=153', 0, 'acf-field-group', '', 0),
(154, 1, '2019-09-23 03:21:28', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-23 03:21:28', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=154', 0, 'page', '', 0),
(155, 1, '2019-09-23 03:24:31', '2019-09-23 03:24:31', '', '本店', '', 'publish', 'closed', 'closed', '', '%e6%9c%ac%e5%ba%97', '', '', '2019-09-23 03:24:32', '2019-09-23 03:24:32', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=155', 0, 'page', '', 0),
(156, 1, '2019-09-23 03:24:31', '2019-09-23 03:24:31', '', '本店', '', 'inherit', 'closed', 'closed', '', '155-revision-v1', '', '', '2019-09-23 03:24:31', '2019-09-23 03:24:31', '', 155, 'http://localhost:8080/CamTu/Task007/2019/09/23/155-revision-v1/', 0, 'revision', '', 0),
(157, 1, '2019-09-23 03:24:32', '2019-09-23 03:24:32', '', '本店', '', 'inherit', 'closed', 'closed', '', '155-revision-v1', '', '', '2019-09-23 03:24:32', '2019-09-23 03:24:32', '', 155, 'http://localhost:8080/CamTu/Task007/2019/09/23/155-revision-v1/', 0, 'revision', '', 0),
(159, 1, '2019-09-23 03:51:59', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-23 03:51:59', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field-group&p=159', 0, 'acf-field-group', '', 0),
(160, 1, '2019-09-23 04:05:15', '2019-09-23 04:05:15', '', 'Home', '', 'trash', 'closed', 'closed', '', 'home__trashed', '', '', '2019-09-23 07:49:23', '2019-09-23 07:49:23', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=160', 0, 'page', '', 0),
(161, 1, '2019-09-23 04:05:15', '2019-09-23 04:05:15', '', 'Home', '', 'inherit', 'closed', 'closed', '', '160-revision-v1', '', '', '2019-09-23 04:05:15', '2019-09-23 04:05:15', '', 160, 'http://localhost:8080/CamTu/Task007/2019/09/23/160-revision-v1/', 0, 'revision', '', 0),
(162, 1, '2019-09-23 04:06:34', '2019-09-23 04:06:34', '', 'Home', '', 'inherit', 'closed', 'closed', '', '160-autosave-v1', '', '', '2019-09-23 04:06:34', '2019-09-23 04:06:34', '', 160, 'http://localhost:8080/CamTu/Task007/2019/09/23/160-autosave-v1/', 0, 'revision', '', 0),
(163, 1, '2019-09-23 04:14:25', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-23 04:14:25', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field-group&p=163', 0, 'acf-field-group', '', 0),
(164, 1, '2019-09-23 04:18:48', '2019-09-23 04:18:48', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:10:\"map-access\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Map', 'map', 'trash', 'closed', 'closed', '', 'group_5d8846b7902c7__trashed', '', '', '2019-09-23 04:24:00', '2019-09-23 04:24:00', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field-group&#038;p=164', 0, 'acf-field-group', '', 0),
(165, 1, '2019-09-23 04:18:48', '2019-09-23 04:18:48', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:0:\"\";}', 'Tile Map', 'tile_map', 'trash', 'closed', 'closed', '', 'field_5d8846bd77018__trashed', '', '', '2019-09-23 04:24:00', '2019-09-23 04:24:00', '', 164, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=165', 0, 'acf-field', '', 0),
(166, 1, '2019-09-23 04:18:48', '2019-09-23 04:18:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Adress', 'adress', 'trash', 'closed', 'closed', '', 'field_5d88470577019__trashed', '', '', '2019-09-23 04:24:01', '2019-09-23 04:24:01', '', 164, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=166', 1, 'acf-field', '', 0),
(167, 1, '2019-09-23 04:18:48', '2019-09-23 04:18:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Time', 'time', 'trash', 'closed', 'closed', '', 'field_5d8847377701a__trashed', '', '', '2019-09-23 04:24:01', '2019-09-23 04:24:01', '', 164, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=167', 2, 'acf-field', '', 0),
(168, 1, '2019-09-23 04:18:48', '2019-09-23 04:18:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tel', 'tel', 'trash', 'closed', 'closed', '', 'field_5d8847457701b__trashed', '', '', '2019-09-23 04:24:01', '2019-09-23 04:24:01', '', 164, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=168', 3, 'acf-field', '', 0),
(169, 1, '2019-09-23 04:18:48', '2019-09-23 04:18:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Fax', 'fax', 'trash', 'closed', 'closed', '', 'field_5d8847547701c__trashed', '', '', '2019-09-23 04:24:01', '2019-09-23 04:24:01', '', 164, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=169', 4, 'acf-field', '', 0),
(170, 1, '2019-09-23 04:18:48', '2019-09-23 04:18:48', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Email', 'email', 'trash', 'closed', 'closed', '', 'field_5d88475f7701d__trashed', '', '', '2019-09-23 04:24:01', '2019-09-23 04:24:01', '', 164, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=170', 5, 'acf-field', '', 0),
(171, 1, '2019-09-23 04:18:48', '2019-09-23 04:18:48', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image Map', 'image_map', 'trash', 'closed', 'closed', '', 'field_5d88476a7701e__trashed', '', '', '2019-09-23 04:24:02', '2019-09-23 04:24:02', '', 164, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=171', 6, 'acf-field', '', 0),
(172, 1, '2019-09-23 04:18:53', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-23 04:18:53', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=172', 0, 'page', '', 0),
(173, 1, '2019-09-23 04:19:08', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-09-23 04:19:08', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?p=173', 0, 'post', '', 0),
(174, 1, '2019-09-23 04:19:24', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-23 04:19:24', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=174', 0, 'page', '', 0),
(175, 1, '2019-09-23 04:19:41', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-23 04:19:41', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=175', 0, 'page', '', 0),
(176, 1, '2019-09-23 04:20:50', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-23 04:20:50', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?page_id=176', 0, 'page', '', 0),
(177, 1, '2019-09-23 04:25:39', '2019-09-23 04:25:39', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:10:\"map-access\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Map', 'map', 'trash', 'closed', 'closed', '', 'group_5d8848e666da9__trashed', '', '', '2019-09-23 04:26:55', '2019-09-23 04:26:55', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field-group&#038;p=177', 0, 'acf-field-group', '', 0),
(178, 1, '2019-09-23 04:25:39', '2019-09-23 04:25:39', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Title Map', 'title_map', 'trash', 'closed', 'closed', '', 'field_5d8848f5acb7d__trashed', '', '', '2019-09-23 04:26:56', '2019-09-23 04:26:56', '', 177, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=178', 0, 'acf-field', '', 0),
(179, 1, '2019-09-23 04:25:39', '2019-09-23 04:25:39', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image Map', 'image_map', 'publish', 'closed', 'closed', '', 'field_5d884911acb7e', '', '', '2019-09-23 04:25:39', '2019-09-23 04:25:39', '', 178, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&p=179', 0, 'acf-field', '', 0),
(180, 1, '2019-09-23 04:30:05', '2019-09-23 04:30:05', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:10:\"map-access\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Map Access', 'map-access', 'publish', 'closed', 'closed', '', 'group_5d8849951ce03', '', '', '2019-09-23 04:45:11', '2019-09-23 04:45:11', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field-group&#038;p=180', 0, 'acf-field-group', '', 0),
(181, 1, '2019-09-23 04:30:05', '2019-09-23 04:30:05', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:0:\"\";}', 'Map', 'map', 'publish', 'closed', 'closed', '', 'field_5d8849a47fdf1', '', '', '2019-09-23 04:45:11', '2019-09-23 04:45:11', '', 180, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&#038;p=181', 0, 'acf-field', '', 0),
(182, 1, '2019-09-23 04:30:05', '2019-09-23 04:30:05', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title Map', 'title_map', 'publish', 'closed', 'closed', '', 'field_5d8849ba7fdf2', '', '', '2019-09-23 04:30:05', '2019-09-23 04:30:05', '', 181, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&p=182', 0, 'acf-field', '', 0),
(183, 1, '2019-09-23 04:30:05', '2019-09-23 04:30:05', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Adresss', 'adresss', 'publish', 'closed', 'closed', '', 'field_5d8849d67fdf3', '', '', '2019-09-23 04:30:05', '2019-09-23 04:30:05', '', 181, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&p=183', 1, 'acf-field', '', 0),
(184, 1, '2019-09-23 04:30:05', '2019-09-23 04:30:05', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Time', 'time', 'publish', 'closed', 'closed', '', 'field_5d8849e87fdf4', '', '', '2019-09-23 04:30:05', '2019-09-23 04:30:05', '', 181, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&p=184', 2, 'acf-field', '', 0),
(185, 1, '2019-09-23 04:30:05', '2019-09-23 04:30:05', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Tel', 'tel', 'publish', 'closed', 'closed', '', 'field_5d884a037fdf5', '', '', '2019-09-23 04:30:05', '2019-09-23 04:30:05', '', 181, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&p=185', 3, 'acf-field', '', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(186, 1, '2019-09-23 04:30:06', '2019-09-23 04:30:06', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Fax', 'fax', 'publish', 'closed', 'closed', '', 'field_5d884a0e7fdf6', '', '', '2019-09-23 04:30:06', '2019-09-23 04:30:06', '', 181, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&p=186', 4, 'acf-field', '', 0),
(187, 1, '2019-09-23 04:30:06', '2019-09-23 04:30:06', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Email', 'email', 'publish', 'closed', 'closed', '', 'field_5d884a1a7fdf7', '', '', '2019-09-23 04:30:06', '2019-09-23 04:30:06', '', 181, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&p=187', 5, 'acf-field', '', 0),
(188, 1, '2019-09-23 04:30:06', '2019-09-23 04:30:06', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image Map', 'image_map', 'publish', 'closed', 'closed', '', 'field_5d884a257fdf8', '', '', '2019-09-23 04:30:06', '2019-09-23 04:30:06', '', 181, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&p=188', 6, 'acf-field', '', 0),
(189, 1, '2019-09-23 05:01:42', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-09-23 05:01:42', '0000-00-00 00:00:00', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field-group&p=189', 0, 'acf-field-group', '', 0),
(190, 1, '2019-09-23 06:07:41', '2019-09-23 06:07:41', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:6:\"slider\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Slider', 'slider', 'publish', 'closed', 'closed', '', 'group_5d8860bcdf2d4', '', '', '2019-09-23 06:08:08', '2019-09-23 06:08:08', '', 0, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field-group&#038;p=190', 0, 'acf-field-group', '', 0),
(191, 1, '2019-09-23 06:07:41', '2019-09-23 06:07:41', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'BxSlider', 'bxslider', 'publish', 'closed', 'closed', '', 'field_5d8860c8dc8f5', '', '', '2019-09-23 06:07:41', '2019-09-23 06:07:41', '', 190, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&p=191', 0, 'acf-field', '', 0),
(192, 1, '2019-09-23 06:07:41', '2019-09-23 06:07:41', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image Slide', 'image_slide', 'publish', 'closed', 'closed', '', 'field_5d8860e1dc8f6', '', '', '2019-09-23 06:07:41', '2019-09-23 06:07:41', '', 191, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&p=192', 0, 'acf-field', '', 0),
(193, 1, '2019-09-23 06:07:41', '2019-09-23 06:07:41', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Link Imge Slide', 'link_imge_slide', 'publish', 'closed', 'closed', '', 'field_5d886112dc8f7', '', '', '2019-09-23 06:07:41', '2019-09-23 06:07:41', '', 191, 'http://localhost:8080/CamTu/Task007/?post_type=acf-field&p=193', 1, 'acf-field', '', 0),
(194, 1, '2019-09-23 07:49:19', '2019-09-23 07:49:19', '<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost:8080/CamTu/Task007.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you visit our login page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->', 'Privacy Policy', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2019-09-23 07:49:19', '2019-09-23 07:49:19', '', 3, 'http://localhost:8080/CamTu/Task007/2019/09/23/3-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_rs_photos`
--

CREATE TABLE `wp_rs_photos` (
  `id` int(11) NOT NULL,
  `folder_id` int(11) NOT NULL DEFAULT 0,
  `album_id` int(11) NOT NULL,
  `attachment_id` int(11) NOT NULL,
  `position` int(11) NOT NULL DEFAULT 9000,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wp_rs_photos`
--

INSERT INTO `wp_rs_photos` (`id`, `folder_id`, `album_id`, `attachment_id`, `position`, `timestamp`) VALUES
(1, 0, 0, 14, 9000, '2019-09-18 10:29:39'),
(2, 0, 0, 13, 9000, '2019-09-18 10:29:39'),
(3, 0, 0, 6, 9000, '2019-09-18 10:29:39'),
(4, 0, 0, 15, 9000, '2019-09-18 10:29:39');

-- --------------------------------------------------------

--
-- Table structure for table `wp_rs_resources`
--

CREATE TABLE `wp_rs_resources` (
  `id` int(11) NOT NULL,
  `resource_type` enum('folder','image','map','video') COLLATE utf8_unicode_ci NOT NULL,
  `resource_id` int(11) NOT NULL,
  `slider_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wp_rs_resources`
--

INSERT INTO `wp_rs_resources` (`id`, `resource_type`, `resource_id`, `slider_id`) VALUES
(1, 'image', 1, 1),
(2, 'image', 3, 1),
(3, 'image', 4, 1),
(4, 'image', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_rs_settings_sets`
--

CREATE TABLE `wp_rs_settings_sets` (
  `id` int(11) NOT NULL,
  `data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_rs_settings_sets`
--

INSERT INTO `wp_rs_settings_sets` (`id`, `data`) VALUES
(1, 'a:7:{s:10:\"properties\";a:4:{s:5:\"width\";s:4:\"1000\";s:9:\"widthType\";s:2:\"px\";s:6:\"height\";s:3:\"400\";s:13:\"videoAutoplay\";s:5:\"false\";}s:7:\"general\";a:11:{s:4:\"mode\";s:10:\"horizontal\";s:10:\"moveSlides\";s:1:\"0\";s:4:\"auto\";s:6:\"enable\";s:17:\"slideshowControls\";s:6:\"enable\";s:10:\"navigation\";s:1:\"0\";s:4:\"rows\";s:1:\"0\";s:8:\"captions\";s:5:\"false\";s:19:\"captionsByMouseover\";s:4:\"true\";s:5:\"speed\";s:3:\"500\";s:11:\"randomStart\";s:1:\"1\";s:6:\"easing\";s:6:\"linear\";}s:7:\"caption\";a:9:{s:5:\"color\";s:4:\"#fff\";s:9:\"font-size\";s:2:\"14\";s:10:\"text-align\";s:4:\"auto\";s:11:\"font-family\";s:9:\"Open Sans\";s:9:\"font-type\";s:8:\"standard\";s:20:\"background-color-hex\";s:4:\"#000\";s:16:\"background-color\";s:16:\"rgba(0, 0, 0, 1)\";s:18:\"background-opacity\";s:3:\"0.5\";s:5:\"inTop\";s:5:\"false\";}s:5:\"touch\";a:3:{s:12:\"touchEnabled\";s:4:\"true\";s:8:\"oneToOne\";s:4:\"true\";s:14:\"swipeThreshold\";s:2:\"55\";}s:5:\"pager\";a:2:{s:12:\"pagerEnabled\";s:4:\"true\";s:9:\"pagerType\";s:4:\"full\";}s:13:\"socialSharing\";a:1:{s:6:\"status\";s:7:\"disable\";}s:11:\"__veditor__\";s:0:\"\";}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_rs_sliders`
--

CREATE TABLE `wp_rs_sliders` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `settings_id` int(11) NOT NULL,
  `plugin` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `wp_rs_sliders`
--

INSERT INTO `wp_rs_sliders` (`id`, `title`, `settings_id`, `plugin`) VALUES
(1, 'Slider', 1, 'bx');

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'お知らせ', '%e3%81%8a%e7%9f%a5%e3%82%89%e3%81%9b', 0),
(2, 'slider', 'slider', 0),
(3, '特集記事', '%e7%89%b9%e9%9b%86%e8%a8%98%e4%ba%8b', 0),
(4, '事務所ニュース', '%e4%ba%8b%e5%8b%99%e6%89%80%e3%83%8b%e3%83%a5%e3%83%bc%e3%82%b9', 0),
(5, 'デイリーニュース', '%e3%83%87%e3%82%a4%e3%83%aa%e3%83%bc%e3%83%8b%e3%83%a5%e3%83%bc%e3%82%b9', 0),
(6, '105', '105', 0),
(7, 'Main menu', 'main-menu', 0),
(8, 'Footer menu', 'footer-menu', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(5, 1, 0),
(24, 1, 0),
(26, 3, 0),
(29, 4, 0),
(32, 4, 0),
(34, 4, 0),
(36, 3, 0),
(38, 5, 0),
(40, 5, 0),
(43, 3, 0),
(68, 4, 0),
(70, 4, 0),
(72, 4, 0),
(75, 4, 0),
(77, 4, 0),
(79, 4, 0),
(82, 4, 0),
(84, 4, 0),
(86, 4, 0),
(88, 4, 0),
(101, 1, 0),
(106, 6, 0),
(107, 6, 0),
(108, 6, 0),
(109, 6, 0),
(120, 1, 0),
(133, 7, 0),
(134, 7, 0),
(135, 7, 0),
(136, 7, 0),
(137, 7, 0),
(144, 8, 0),
(145, 8, 0),
(146, 8, 0),
(147, 8, 0),
(148, 8, 0),
(149, 8, 0),
(150, 8, 0),
(151, 8, 0),
(152, 8, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'slider_cat', '', 0, 0),
(3, 3, 'category', '', 0, 2),
(4, 4, 'category', '', 0, 11),
(5, 5, 'category', '', 0, 2),
(6, 6, 'ml-slider', '', 0, 4),
(7, 7, 'nav_menu', '', 0, 5),
(8, 8, 'nav_menu', '', 0, 9);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'plugin_editor_notice'),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"b8076f83b484bf36ee44323ca88befee370cc9f542c951155a0fdd098e57297e\";a:4:{s:10:\"expiration\";i:1569377259;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36\";s:5:\"login\";i:1569204459;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'wp_user-settings', 'libraryContent=browse&editor=html&post_dfw=off&mfold=o'),
(19, 1, 'wp_user-settings-time', '1568857876'),
(20, 1, 'closedpostboxes_feature', 'a:0:{}'),
(21, 1, 'metaboxhidden_feature', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(22, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(23, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),
(24, 1, 'closedpostboxes_post', 'a:0:{}'),
(25, 1, 'metaboxhidden_post', 'a:0:{}'),
(26, 1, 'wp_metaslider_user_saw_callout_toolbar', '1'),
(27, 1, 'nav_menu_recently_edited', '8'),
(28, 1, 'closedpostboxes_page', 'a:0:{}'),
(29, 1, 'metaboxhidden_page', 'a:0:{}'),
(30, 1, 'meta-box-order_page', 'a:4:{s:6:\"normal\";s:23:\"acf-group_5d81fa706d8e8\";s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:0:\"\";s:8:\"advanced\";s:0:\"\";}'),
(31, 1, 'enable_custom_fields', '1'),
(32, 1, 'closedpostboxes_toplevel_page_map-access', 'a:0:{}'),
(33, 1, 'metaboxhidden_toplevel_page_map-access', 'a:0:{}'),
(34, 1, 'meta-box-order_toplevel_page_map-access', 'a:2:{s:4:\"side\";s:9:\"submitdiv\";s:6:\"normal\";s:23:\"acf-group_5d8846b7902c7\";}'),
(35, 1, 'screen_layout_toplevel_page_map-access', '2');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$Ba2/U9Yx.yhOAmZuz9yAqeVM./Ad2Z0', 'admin', 'camtun80@gmail.com', '', '2019-09-16 04:16:00', '', 0, 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_masterslider_options`
--
ALTER TABLE `wp_masterslider_options`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_masterslider_sliders`
--
ALTER TABLE `wp_masterslider_sliders`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `wp_nextend2_image_storage`
--
ALTER TABLE `wp_nextend2_image_storage`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hash` (`hash`);

--
-- Indexes for table `wp_nextend2_section_storage`
--
ALTER TABLE `wp_nextend2_section_storage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application` (`application`,`section`,`referencekey`),
  ADD KEY `application_2` (`application`,`section`);

--
-- Indexes for table `wp_nextend2_smartslider3_generators`
--
ALTER TABLE `wp_nextend2_smartslider3_generators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_nextend2_smartslider3_sliders`
--
ALTER TABLE `wp_nextend2_smartslider3_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_nextend2_smartslider3_sliders_xref`
--
ALTER TABLE `wp_nextend2_smartslider3_sliders_xref`
  ADD PRIMARY KEY (`group_id`,`slider_id`);

--
-- Indexes for table `wp_nextend2_smartslider3_slides`
--
ALTER TABLE `wp_nextend2_smartslider3_slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_rs_photos`
--
ALTER TABLE `wp_rs_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_rs_resources`
--
ALTER TABLE `wp_rs_resources`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `wp_rs_settings_sets`
--
ALTER TABLE `wp_rs_settings_sets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_rs_sliders`
--
ALTER TABLE `wp_rs_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_masterslider_options`
--
ALTER TABLE `wp_masterslider_options`
  MODIFY `ID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_masterslider_sliders`
--
ALTER TABLE `wp_masterslider_sliders`
  MODIFY `ID` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_nextend2_image_storage`
--
ALTER TABLE `wp_nextend2_image_storage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wp_nextend2_section_storage`
--
ALTER TABLE `wp_nextend2_section_storage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10117;

--
-- AUTO_INCREMENT for table `wp_nextend2_smartslider3_generators`
--
ALTER TABLE `wp_nextend2_smartslider3_generators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_nextend2_smartslider3_sliders`
--
ALTER TABLE `wp_nextend2_smartslider3_sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wp_nextend2_smartslider3_slides`
--
ALTER TABLE `wp_nextend2_smartslider3_slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=543;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=637;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=195;

--
-- AUTO_INCREMENT for table `wp_rs_photos`
--
ALTER TABLE `wp_rs_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wp_rs_resources`
--
ALTER TABLE `wp_rs_resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wp_rs_settings_sets`
--
ALTER TABLE `wp_rs_settings_sets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_rs_sliders`
--
ALTER TABLE `wp_rs_sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
